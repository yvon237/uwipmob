/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Mmoperator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import uWIPWS.ParseJsonObject;

/**
 *
 * @author Yvon ZOBO
 */
@Stateless
@Path("entities.mmoperator")
public class MmoperatorFacadeREST extends AbstractFacade<Mmoperator> {
    @PersistenceContext(unitName = "uWIPWSPU")
    private EntityManager em;

    public MmoperatorFacadeREST() {
        super(Mmoperator.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Mmoperator entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Long id, Mmoperator entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Mmoperator find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Mmoperator> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Mmoperator> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    
    @GET
    @Path("GetAllmmOperator")
    @Produces("application/json")
    public List<Mmoperator> GetAllmmOperator(){
        Query query = em.createNamedQuery("Mmoperator.findAll");
        return query.getResultList();
    }
    @GET
    @Path("addmmOperator")
    @Consumes("application/json")
    public String addmmOperator(@QueryParam("idoperator")Long idoperator,
            @QueryParam("code")String code){
        
        
        String response;
        Mmoperator m = new Mmoperator();
        m.setIdoperator(idoperator);
        m.setCode(code);
        
        try {
            super.create(m);
             response = ParseJsonObject.constructJSON("addmmoperator", true);
             return response;
            
        } catch (Exception e) {
            e.getMessage();
             response = ParseJsonObject.constructJSON("updateclient", false,"error lors de l'ajout d'un operateur!");
             return response;
        }
        
   }
        
        
   

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
