/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Uuser;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.codehaus.jettison.json.JSONObject;
import uWIPWS.ParseJsonObject;
import uWIPWS.ShaHash;

/**
 *
 * @author Yvon ZOBO
 */
@Stateless
@Path("entities.uuser")
public class UuserFacadeREST extends AbstractFacade<Uuser> {
    @PersistenceContext(unitName = "uWIPWSPU")
    private EntityManager em;

    public UuserFacadeREST() {
        super(Uuser.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Uuser entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Long id, Uuser entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Uuser find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Uuser> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Uuser> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    
    
    /**
     * methode to do test
     * @return 
     */
    @GET
    @Path("getName")
    @Produces("application/json")
    public String getName(){
        String response;
        Query query = em.createNamedQuery("Uuser.findLoginAndPassword");
        query.setParameter("login", "yvon").setParameter("mdp", "Fe30zFrMj87Lw+TNLTgGCVHSk/c=");
       List<Uuser>  user;
       
        
        user = query.getResultList();
        if(!user.isEmpty())
            response = ParseJsonObject.constructJSON("login", true);
        else
            response = ParseJsonObject.constructJSON("login", false, "Incorrect login or password");
        return response;
    }
    
    
    /**
     * method to authenficate user
     * @param login
     * @param mdp
     * @return 
     */
    
    @GET
    @Path("/doLogin")
    @Produces("application/json")
    public String doLogin(@QueryParam("login") String login, @QueryParam("mdp") String mdp){
        String response;
        Query query = em.createNamedQuery("Uuser.findLoginAndPassword");
        query.setParameter("login", login).setParameter("mdp", mdp);
      
        List<Uuser>  user;
       
        
        user = query.getResultList();
        if(!user.isEmpty())
            response = ParseJsonObject.constructJSON("login", true);
           
        else
            response = ParseJsonObject.constructJSON("login", false, "Incorrect login or password");
        return response;
    }
    
    /**
     * retourne la liste de tous les utilisateurs
     * @return 
     */
    @GET
    @Path("getAllUsers")
    @Produces("application/json")
    public List<Uuser> getAllUsers(){
        Query query = em.createNamedQuery("Uuser.findAll");
        List<Uuser> userList = query.getResultList();
        
        return userList;
        
       
       /* try {
             JSONObject obj = new JSONObject();
             response =  obj.put("listuser", userList).toString();
        } catch (Exception e) {
        }
      
       
        return  response;*/
        
    }
    

     
/**
 * retourne l'utilisateur connecter
 * @param login
 * @param mdp
 * @return 
 */
    @GET
    @Path("/getUserByloginpassword")
    @Produces(value = "application/json")
    public Uuser getUserByloginpassword(@QueryParam("login") String login,@QueryParam("mdp")String mdp) {
       Uuser uuserResult= new Uuser();
       // List<Uuser> uuserResult = new ArrayList<>(); 
        
        try {
             Query query = em.createNamedQuery("Uuser.findLoginAndPassword");
            query.setParameter("login", login).setParameter("mdp", mdp);
           uuserResult =(Uuser) query.getSingleResult();
           // uuserResult = query.getResultList();
        } catch (Exception e) {
        }
      return uuserResult;
   }
    
    @GET
    @Path(value = "getUserByloginTest")
    @Produces(value = "application/json")
    public Uuser userByloginTest() {
        Query query = em.createNamedQuery("Uuser.findLoginAndPassword");
        query.setParameter("login", "yvon").setParameter("mdp", "Fe30zFrMj87Lw+TNLTgGCVHSk/c=");
        Uuser user =(Uuser) query.getSingleResult();
        String response=null;
        if(user==null)
            return null;
        else
            return user;
        
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
