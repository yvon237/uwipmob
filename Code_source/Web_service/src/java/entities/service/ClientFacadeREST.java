/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Client;
import entities.Mmoperator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import uWIPWS.ParseJsonObject;

/**
 *
 * @author Yvon ZOBO
 */
@Stateless
@Path("entities.client")
public class ClientFacadeREST extends AbstractFacade<Client> {
    @PersistenceContext(unitName = "uWIPWSPU")
    private EntityManager em;

    public ClientFacadeREST() {
        super(Client.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Client entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Long id, Client entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Client find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces("application/json")
    public List<Client> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Client> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    
    @GET
    @Path("/getAllCustomers")
    @Produces("apllication/json")
    public List<Client> getAllCustomers(){
        Query query = em.createNamedQuery("Client.findAll");
        
        List<Client> list;
        list = query.getResultList();
        
        
        return list;
    }
    
    
    
    @GET
    @Path("upDateClient")
    @Consumes("application/json")
    public String upDateClient(@QueryParam("idclient")Long idclient,@QueryParam("idoperator")Long idoperator,@QueryParam("firstname")String firstname,
            @QueryParam("lastname")String lastname,@QueryParam("nationality")String nationality,@QueryParam("preferredphone") String preferredphone,
            @QueryParam("phoneorange")String phoneorange,@QueryParam("phonemtn")String phonemtn,@QueryParam("phonenexttel")String phonenexttel,
            @QueryParam("signedistinctif")String signedistinctif,@QueryParam("firstcontact")String firstcontact,@QueryParam("mobilemoney")String mobilemoney,@QueryParam("municipality")String municipality,
            @QueryParam("village")String village){
        
        String response;
        
         Client client = new Client();
        Mmoperator operator;
        
         Query query = em.createNamedQuery("Mmoperator.findByIdoperator");
             query.setParameter("idoperator", idoperator);
        
                operator = (Mmoperator) query.getSingleResult();
                
        client.setIdoperator(operator);
        client.setIdclient(idclient);
        client.setLastname(lastname);
        client.setFirstname(firstname);
        client.setNationality(nationality);
        client.setPreferredphone(preferredphone);
        client.setPhoneorange(phoneorange);
        client.setPhonemtn(phonemtn);
        client.setPhonenexttel(phonenexttel);
        client.setSignedistinctif(signedistinctif);
        client.setFirstcontact(firstcontact);
        client.setMobilemoney(mobilemoney);
        client.setMunicipality(municipality);
        client.setVillage(village);
        
        try {
            super.edit(client);
            response = ParseJsonObject.constructJSON("updateclient", true);
             return response;
        } catch (Exception e) {
            e.getMessage();
            response = ParseJsonObject.constructJSON("updateclient", false,"erreur de mise à jour du  client!!");
             return response;
        }
        
        
    }
    @POST
    @Path("addclient")
    @Consumes("application/json")
    public void addclient(@QueryParam("idclient")Long idclient,@QueryParam("idoperator")Long idoperator,@QueryParam("firstname")String firstname,
            @QueryParam("lastname")String lastname,@QueryParam("nationality")String nationality,@QueryParam("preferredphone") String preferredphone,
            @QueryParam("phoneorange")String phoneorange,@QueryParam("phonemtn")String phonemtn,@QueryParam("phonenexttel")String phonenexttel,
            @QueryParam("signedistinctif")String signedistinctif,@QueryParam("firstcontact")String firstcontact,@QueryParam("mobilemoney")String mobilemoney,@QueryParam("municipality")String municipality,
            @QueryParam("village")String village){
        
        
         Client client = new Client();
        Mmoperator operator;
        
         Query query = em.createNamedQuery("Mmoperator.findByIdoperator");
             query.setParameter("idoperator", idoperator);
        
                operator = (Mmoperator) query.getSingleResult();
                
        client.setIdoperator(operator);
        client.setIdclient(idclient);
        client.setLastname(lastname);
        client.setFirstname(firstname);
        client.setNationality(nationality);
        client.setPreferredphone(preferredphone);
        client.setPhoneorange(phoneorange);
        client.setPhonemtn(phonemtn);
        client.setPhonenexttel(phonenexttel);
        client.setSignedistinctif(signedistinctif);
        client.setFirstcontact(firstcontact);
        client.setMobilemoney(mobilemoney);
        client.setMunicipality(municipality);
        client.setVillage(village);
        
        
        super.create(client);
        
    }
    
    
    

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
