/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uWIPWS;

import entities.Client;
import entities.Uuser;
import java.util.List;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Yvon ZOBO
 */
public class ParseJsonObject {
    
    
    
     public static boolean isNotNull(String txt) {
        // System.out.println("Inside isNotNull");
        return txt != null && txt.trim().length() >= 0;
    }
 
    public static String constructJSON(String tag, boolean status) {
       
        JSONObject obj = new JSONObject();
        try {
            obj.put("tag", tag);
            obj.put("status", new Boolean(status));
        } catch (JSONException e) {
        }
        return obj.toString();
    }
    
      public static String constructJSON(String tag, boolean status,String err_msg) {
       JSONObject obj = new JSONObject();
        try {
            obj.put("tag", tag);
            obj.put("status", new Boolean(status));
            obj.put("err_msg", err_msg);
        } catch (JSONException e) {
        }
        return obj.toString();
    }
      
      
      public static String constructUserJSON(String tag,boolean status,List<Uuser> user){
           JSONObject userObj = new JSONObject();
           
           try{
               userObj.put("tag", tag);
               userObj.put("status", status);
              userObj.put("user", user);
              /* userObj.put("iduuser", user.getIduuser());
               userObj.put("idprofile", user.getIdprofile());
               userObj.put("idorganization", user.getIdorganization());
               userObj.put("firstname", user.getFirstname());
               userObj.put("lastname", user.getLastname());
               userObj.put("birth", user.getBirth());
               userObj.put("login", user.getLogin());
               userObj.put("mdp", user.getMdp());*/
               
               
           }catch(JSONException e){
               
           }
           return userObj.toString();
      }
      
      
      
      
      
      
      
      public static JSONObject parseListToJson(List<Client>clients){
          
          JSONObject object = new JSONObject();
          JSONArray array = new JSONArray();
          try {
              
                
                object.put("listuser", clients);
                
               
             
          } catch (JSONException e) {
          }
       
        return object;
      }
    
}
