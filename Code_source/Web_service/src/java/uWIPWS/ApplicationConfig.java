/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uWIPWS;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Yvon ZOBO
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(entities.service.BoxFacadeREST.class);
        resources.add(entities.service.ClientFacadeREST.class);
        resources.add(entities.service.CodeFacadeREST.class);
        resources.add(entities.service.ContractFacadeREST.class);
        resources.add(entities.service.CountrylayerFacadeREST.class);
        resources.add(entities.service.CreditsFacadeREST.class);
        resources.add(entities.service.DistributorFacadeREST.class);
        resources.add(entities.service.HardversFacadeREST.class);
        resources.add(entities.service.MmoperatorFacadeREST.class);
        resources.add(entities.service.MmtransactionFacadeREST.class);
        resources.add(entities.service.NoteFacadeREST.class);
        resources.add(entities.service.OemproductitemsFacadeREST.class);
        resources.add(entities.service.OrganizationFacadeREST.class);
        resources.add(entities.service.PaygcodesFacadeREST.class);
        resources.add(entities.service.PaygotpgeneratorFacadeREST.class);
        resources.add(entities.service.PayplandetailsFacadeREST.class);
        resources.add(entities.service.PrivilegeFacadeREST.class);
        resources.add(entities.service.ProduitsFacadeREST.class);
        resources.add(entities.service.ProfileFacadeREST.class);
        resources.add(entities.service.SoftversFacadeREST.class);
        resources.add(entities.service.TermesFacadeREST.class);
        resources.add(entities.service.TownlayerFacadeREST.class);
        resources.add(entities.service.UuserFacadeREST.class);
        
    }
    
}
