package com.example.yvonzobo.uwipmob.views.activities;

import android.app.AlertDialog;

import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.TextView;
import android.widget.Toast;

import com.example.yvonzobo.uwipmob.R;

import com.example.yvonzobo.uwipmob.views.fragments.createupdateprospect.CoordonneesProspectFragment;
import com.example.yvonzobo.uwipmob.views.fragments.createupdateprospect.IdentityProspectFragment;
import com.example.yvonzobo.uwipmob.views.fragments.createupdateprospect.RessourcesProspectFragment;

public class ProspectActivity extends AppCompatActivity implements IdentityProspectFragment.OnIdentityProspectInteractionListener,CoordonneesProspectFragment.CoordonnesProspectFragmentInteractionListener,
        RessourcesProspectFragment.OnResourcesFragmentInteractionListener{
    Fragment mFragment; // concerve l'instance du dernier fragment affiché
    Fragment parentFragment;
    private String mTitle = "Menu prospect"; // contient le titre de la page dernièrement affichée
    private TextView title;
    private Toolbar toolbar;
    private boolean closeactivity =false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prospect);
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackground(getResources().getDrawable(R.drawable.black_color));
        toolbar.setTitle("Plate-forme mobile uwip");
        setSupportActionBar(toolbar);

           // AccueilActivity.getInstance().finish();
      title = (TextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.nouveau));


        setFragment(IdentityProspectFragment.newInstance());
        parentFragment = IdentityProspectFragment.newInstance();





    }
    public void setFragment(final Fragment fragment){
        if(fragment==null) return;

        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right).replace(R.id.container,fragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.changecolor) {
            return true;
        } else if (id == R.id.changepass) {
            return true;
        } else if (id == R.id.aide) {
            return true;
        } else if (id == R.id.apropos) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    public void showSuccessAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS");

        // Setting Dialog Message
        alertDialog.setMessage("êtes-vous d'annuler l'enregistrement \ndu nouveau client?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Oui", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog,int which) {

               closeactivity = true;
                Toast.makeText(getApplicationContext(),"Appuyer de nouveau pour quitter",Toast.LENGTH_LONG).show();

                dialog.dismiss();



            }
        });

        alertDialog.setNegativeButton("Non", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {

        if (parentFragment != null) {
            showSuccessAlert();
            parentFragment=null;
        }

        if (closeactivity) {
            Intent accueil = new Intent(ProspectActivity.this,AccueilActivity.class);
            accueil.putExtra("login",ProspectActivity.this.getIntent().getStringExtra("login"));
            accueil.putExtra("mdp",ProspectActivity.this.getIntent().getStringExtra("mdp"));
            accueil.putExtra("typeagent",ProspectActivity.this.getIntent().getStringExtra("typeagent"));
            startActivity(accueil);
            this.finish();
        }





    }




    /**
     * methode interface inteface identite fragment
     * passage du Bundle identite au fragment coordonnness
     * @param identite
     */
    @Override
    public void identiteProspectInteraction(Bundle identite) {
        setFragment(CoordonneesProspectFragment.newInstance(identite));
        title.setText("Nouveau Client");
        toolbar.setTitle("Gestion Clients");
        mFragment = CoordonneesProspectFragment.newInstance(identite);

    }

    /**
     * methode interface coordonnees fragment
     * passage des Bundle identite et coordonnees au fragment Ressources
     * @param identite
     * @param coordonnees
     */
    @Override
    public void onCommunicateDataInteraction(Bundle identite, Bundle coordonnees) {
            setFragment(RessourcesProspectFragment.newInstance(identite,coordonnees));
            mFragment = RessourcesProspectFragment.newInstance(identite,coordonnees);
    }

    /**
     * methode de l'interface coordonnees fragment
     * rappel du fragment identite et passage des informations sur l'identite et l'objet de l'appel
     * @param identiteprospect
     * @param precedent
     */
    @Override
    public void onPassDataCallBackIdentiteProspectInteraction(Bundle identiteprospect, String precedent) {
        setFragment(IdentityProspectFragment.newInstance(identiteprospect,precedent));
        mFragment = IdentityProspectFragment.newInstance(identiteprospect,precedent);
    }


    @Override
    public void onCommunicateDataInteraction(Bundle datas) {

    }

    @Override
    public void onPassDataCallBackRessourcesProspectInteraction(Bundle identiteprospect, String precedent) {

    }
}
