package com.example.yvonzobo.uwipmob.controllers;

import com.example.yvonzobo.uwipmob.models.database.ContentDb;

/**
 * Created by yvon on 20/10/16.
 */
public class ClientController {

    public static final String CREATE_TABLE_CLIENT= "CREATE TABLE "+
            ContentDb.TABLE_CLIENT+" ("+
            ContentDb.COLUMM_ID_CLIENT_LOCALE+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            ContentDb.COLUMM_ID_CLIENT+" INTEGER, "+
            ContentDb.COLUMM_ID_OPERATOR+" INTEGER,"+
            ContentDb.COLUMM_FIRSTNAME+" TEXT, "+
            ContentDb.COLUMM_LASTNAME+" TEXT, "+
            ContentDb.COLUMM_CLIENTUNIQNUMBER+" INTEGER,"+
            ContentDb.COLUMM_NATIONALITY+" TEXT, "+
            ContentDb.COLUMM_TOTAL_BOX+" INTEGER, "+
            ContentDb.COLUMM_PREFERRED_PHONE+" TEXT,"+
            ContentDb.COLUMM_PHONE_ORANGE+" TEXT,"+
            ContentDb.COLUMM_PHONE_MTN+" TEXT,"+
            ContentDb.COLUMM_PHONE_NEXTTEL+" TEXT,"+
            ContentDb.COLUMM_CNIPIC+" TEXT,"+
            ContentDb.COLUMM_ACQUISITDATE+" TEXT,"+
            ContentDb.COLUMM_SIGNEDISTINCTIF+" TEXT,"+
            ContentDb.COLUMM_FIRST_CONTACT+" TEXT,"+
            ContentDb.COLUMM_MOBILE_MONEY+" TEXT,"+
            ContentDb.COLUMM_VILLAGE+" TEXT,"+
            "FOREIGN KEY("+ContentDb.COLUMM_ID_OPERATOR+") REFERENCES "+ContentDb.TABLE_OPERATOR+"("+ContentDb.COLUMM_ID_OPERATOR+"))";

}
