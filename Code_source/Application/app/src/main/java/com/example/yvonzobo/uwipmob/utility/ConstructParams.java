package com.example.yvonzobo.uwipmob.utility;

import com.example.yvonzobo.uwipmob.models.entities.AgentEntity;
import com.example.yvonzobo.uwipmob.models.entities.OperatorEntity;
import com.example.yvonzobo.uwipmob.models.entities.ProspectEntity;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Yvon ZOBO on 03/08/2016.
 */
public class ConstructParams {

   public static RequestParams constructParamsCustomer(ProspectEntity customer, boolean addid){
        RequestParams params = new RequestParams();

        if (addid) {
            params.put("idclient",customer.getIdclient());
        }


        params.put("idoperator",customer.getIdoperator());
        params.put("firstname",customer.getFirstname());
        params.put("lastname",customer.getLastname());
        params.put("nationality",customer.getNationality());
        params.put("preferredphone",customer.getPreferredphone());
        params.put("phoneorange",customer.getPhoneorange());
        params.put("phonemtn",customer.getPhonemtn());
        params.put("phonenexttel",customer.getPhonenexttel());
        params.put("cnipic",customer.getCnipic());
        params.put("acquisitdate", customer.getAcquisitdate());
        params.put("signedistinctif",customer.getSignedistinctif());
        params.put("firstcontact",customer.getFirstcontact());
        params.put("mobilemoney",customer.getMobilemoney());
        params.put("municipality",customer.getMunicipality());
        params.put("village",customer.getVillage());
        return params;
    }
    public static RequestParams contructParamsOperator(OperatorEntity operatorEntity){
        RequestParams params = new RequestParams();

        params.put("idoperator",operatorEntity.getIdoperator());
        params.put("code",operatorEntity.getCode());

        return params;
    }


    public static RequestParams constructParamsCustomerCount(Long idclient) {
        RequestParams params = new RequestParams();
        params.put("idclient",idclient);

        return params;
    }

    /**
     * instance peut avoir comme valeur update ou create
     * @param entityList
     * @param instance
     * @return
     */

    public static RequestParams constructListcustomerParams(List<ProspectEntity> entityList,String instance) {
        RequestParams params = new RequestParams();

        params.put(instance,entityList);
        return params;


    }
    public static RequestParams constructProspectParams(String object) {
        RequestParams params = new RequestParams();
        params.put("client",object);

        return params;
    }
}
