package com.example.yvonzobo.uwipmob.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.yvonzobo.uwipmob.R;

import com.example.yvonzobo.uwipmob.models.menu.MenuClient;


import java.util.List;

/**
 * Created by Yvon ZOBO on 01/10/2016.
 */
public class MenuClientAdapter  extends BaseAdapter{

    public class ViewHolder {
        TextView titremenu;
    }
    public List<MenuClient> menuClientList;
    public Context context;



    public MenuClientAdapter(List<MenuClient> list, Context context) {
        menuClientList = list;
        context = context;


    }


    @Override
    public int getCount() {
        return menuClientList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, final View convertView, ViewGroup parent) {

        View rowView = convertView;

        final ViewHolder viewHolder;

        if (rowView == null) {
            rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_menu_client, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.titremenu = (TextView) rowView.findViewById(R.id.title);

            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        try {
            viewHolder.titremenu.setText(menuClientList.get(i).getTitre());


        } catch (Exception e) {
            e.getCause();
            e.getMessage();
        }

        return rowView;
    }
}
