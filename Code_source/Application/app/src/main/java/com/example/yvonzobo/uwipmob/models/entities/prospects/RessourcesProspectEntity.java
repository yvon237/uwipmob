package com.example.yvonzobo.uwipmob.models.entities.prospects;

/**
 * Created by Yvon ZOBO on 11/10/2016.
 */
public class RessourcesProspectEntity {

    private String rse_satisfaiteneo; // si le client est satisfait par le réseau électrique eneo
    private int rse_facturemensuelleeneo; // montant moyen d'une facture eneo
    private String rse_generator; // si le client utilise un générateur (si oui affichée le champ qtitegasoil et prixunitgasoil
    private int rse_qtitehebdogasoil; // quantité de gasoil utilisé par le client par semaine
    private int rse_prixunitgasoil; // prix d'un litre de gasoil
    private  String rse_extrautilsgenerator; // si le generateur est utilisé à d'autre fins
    private String rse_lampepetrole; // si utlisation de la lampe à pétrole affichée les champs rse_qtitehebdopetrole et rse_prixunitpetrole
    private int rse_qtitehebdopetrole; // nombre litres par semaine
    private int rse_prixunitpetrole; // le prix d'un litre de pétrole
    private String rse_bougie; // si utilisation bougie affichée rse_qtitehebdobougie et rse_prixunitbougie
    private int rse_qtitehebdobougie;
    private int rse_prixunitbougie;
    private String rse_pile;
    private int rse_qtitehebdopile;
    private int rse_prixunitpile;
    private int rse_qtitephonedispo; // nombre de telephone du client
    private String moderecharge; // lieu de recharge (a domicile ou a l'exterieur
    private int rse_coutrechargephone; // montant d'une recharge
    private int rse_totalhebdorecharge; // nombre de recharge par semaine
    private int rse_transport; // depenses moyennes en terme de deplacement


    public RessourcesProspectEntity() {

    }


    public String getRse_satisfaiteneo() {
        return rse_satisfaiteneo;
    }

    public void setRse_satisfaiteneo(String rse_satisfaiteneo) {
        this.rse_satisfaiteneo = rse_satisfaiteneo;
    }

    public int getRse_facturemensuelleeneo() {
        return rse_facturemensuelleeneo;
    }

    public void setRse_facturemensuelleeneo(int rse_facturemensuelleeneo) {
        this.rse_facturemensuelleeneo = rse_facturemensuelleeneo;
    }

    public String getRse_generator() {
        return rse_generator;
    }

    public void setRse_generator(String rse_generator) {
        this.rse_generator = rse_generator;
    }

    public int getRse_qtitehebdogasoil() {
        return rse_qtitehebdogasoil;
    }

    public void setRse_qtitehebdogasoil(int rse_qtitehebdogasoil) {
        this.rse_qtitehebdogasoil = rse_qtitehebdogasoil;
    }

    public int getRse_prixunitgasoil() {
        return rse_prixunitgasoil;
    }

    public void setRse_prixunitgasoil(int rse_prixunitgasoil) {
        this.rse_prixunitgasoil = rse_prixunitgasoil;
    }

    public String getRse_extrautilsgenerator() {
        return rse_extrautilsgenerator;
    }

    public void setRse_extrautilsgenerator(String rse_extrautilsgenerator) {
        this.rse_extrautilsgenerator = rse_extrautilsgenerator;
    }

    public int getRse_qtitehebdopetrole() {
        return rse_qtitehebdopetrole;
    }

    public void setRse_qtitehebdopetrole(int rse_qtitehebdopetrole) {
        this.rse_qtitehebdopetrole = rse_qtitehebdopetrole;
    }

    public String getRse_lampepetrole() {
        return rse_lampepetrole;
    }

    public void setRse_lampepetrole(String rse_lampepetrole) {
        this.rse_lampepetrole = rse_lampepetrole;
    }

    public int getRse_prixunitpetrole() {
        return rse_prixunitpetrole;
    }

    public void setRse_prixunitpetrole(int rse_prixunitpetrole) {
        this.rse_prixunitpetrole = rse_prixunitpetrole;
    }

    public String getRse_bougie() {
        return rse_bougie;
    }

    public void setRse_bougie(String rse_bougie) {
        this.rse_bougie = rse_bougie;
    }

    public int getRse_qtitehebdobougie() {
        return rse_qtitehebdobougie;
    }

    public void setRse_qtitehebdobougie(int rse_qtitehebdobougie) {
        this.rse_qtitehebdobougie = rse_qtitehebdobougie;
    }

    public int getRse_prixunitbougie() {
        return rse_prixunitbougie;
    }

    public void setRse_prixunitbougie(int rse_prixunitbougie) {
        this.rse_prixunitbougie = rse_prixunitbougie;
    }

    public String getRse_pile() {
        return rse_pile;
    }

    public void setRse_pile(String rse_pile) {
        this.rse_pile = rse_pile;
    }

    public int getRse_qtitehebdopile() {
        return rse_qtitehebdopile;
    }

    public void setRse_qtitehebdopile(int rse_qtitehebdopile) {
        this.rse_qtitehebdopile = rse_qtitehebdopile;
    }

    public int getRse_qtitephonedispo() {
        return rse_qtitephonedispo;
    }

    public void setRse_qtitephonedispo(int rse_qtitephonedispo) {
        this.rse_qtitephonedispo = rse_qtitephonedispo;
    }

    public int getRse_prixunitpile() {
        return rse_prixunitpile;
    }

    public void setRse_prixunitpile(int rse_prixunitpile) {
        this.rse_prixunitpile = rse_prixunitpile;
    }

    public String getModerecharge() {
        return moderecharge;
    }

    public void setModerecharge(String moderecharge) {
        this.moderecharge = moderecharge;
    }

    public int getRse_coutrechargephone() {
        return rse_coutrechargephone;
    }

    public void setRse_coutrechargephone(int rse_coutrechargephone) {
        this.rse_coutrechargephone = rse_coutrechargephone;
    }

    public int getRse_totalhebdorecharge() {
        return rse_totalhebdorecharge;
    }

    public void setRse_totalhebdorecharge(int rse_totalhebdorecharge) {
        this.rse_totalhebdorecharge = rse_totalhebdorecharge;
    }

    public int getRse_transport() {
        return rse_transport;
    }

    public void setRse_transport(int rse_transport) {
        this.rse_transport = rse_transport;
    }
}
