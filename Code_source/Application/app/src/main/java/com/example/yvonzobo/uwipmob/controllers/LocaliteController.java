package com.example.yvonzobo.uwipmob.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.yvonzobo.uwipmob.models.database.ContentDb;
import com.example.yvonzobo.uwipmob.models.database.DbHelper;
import com.example.yvonzobo.uwipmob.models.entities.VillageEntity;

/**
 * Created by Yvon ZOBO on 12/10/2016.
 */
public class LocaliteController {



    public static final String CREATE_TABLE_VILLAGE = "CREATE TABLE "+
            ContentDb.TABLE_VILLAGE+" ("+
            ContentDb.COLUMM_ID_VILLAGE+" INTEGER PRIMARY KEY, "+
            ContentDb.COLUMM_ID_ARRONDISSEMENT+" INTEGER, "+
            ContentDb.COLUMM_VILLAGE+" TEXT),"+
            "FOREIGN KEY("+ContentDb.COLUMM_ID_ARRONDISSEMENT+") REFERENCES "+ContentDb.TABLE_ARRONDISSEMENT+"("+ContentDb.COLUMM_ID_ARRONDISSEMENT+"));";


    public static final String CREATE_TABLE_ARRONDISSEMENT = "CREATE TABLE "+
            ContentDb.TABLE_ARRONDISSEMENT+" ("+
            ContentDb.COLUMM_ID_ARRONDISSEMENT+" INTEGER PRIMARY KEY, "+
            ContentDb.COLUMM_ID_DEPARTEMENT+" INTEGER,"+
            ContentDb.COLUMM_ARRONDISSEMENT+" TEXT, "+
            "FOREIGN KEY("+ContentDb.COLUMM_ID_DEPARTEMENT+") REFERENCES "+ContentDb.TABLE_DEPARTEMENT+"("+ContentDb.COLUMM_ID_DEPARTEMENT+"));";

    public static final String CREATE_TABLE_DEPARTEMENT = "CREATE TABLE "+
            ContentDb.TABLE_DEPARTEMENT+" ("+
            ContentDb.COLUMM_ID_DEPARTEMENT+" INTEGER PRIMARY KEY, "+
            ContentDb.COLUMM_ID_REGION+" INTEGER,"+
            ContentDb.COLUMM_DEPARTEMENT+" TEXT, "+
            "FOREIGN KEY("+ContentDb.COLUMM_ID_REGION+") REFERENCES "+ContentDb.TABLE_REGION+"("+ContentDb.COLUMM_ID_REGION+"));";

    public static final String CREATE_TABLE_REGION = "CREATE TABLE "+
            ContentDb.TABLE_REGION+" ("+
            ContentDb.COLUMM_ID_REGION+" INTEGER PRIMARY KEY, "+
            ContentDb.COLUMM_REGION+" TEXT);";


    private DbHelper db;
    private SQLiteDatabase mydb;

    public LocaliteController(Context context){
        db = new DbHelper(context);
    }
    /**
     * ouvre la base de données en écriture
     * @return
     */
    public SQLiteDatabase open(){

        mydb = db.getWritableDatabase();
        return mydb;
    }

    /**
     * ferme la base de données
     */
    public void close(){
        mydb.close();
    }

    public SQLiteDatabase getMydb(){
        return mydb;
    }


    public int getIdvillage(String village){
        Cursor cursor = open().rawQuery("select idvillage from village where nomvillage = ?",new String[]{village});

        if (cursor.getCount() == 0) {
            return 0;
        } else {
            cursor.moveToFirst();
            return  cursor.getInt(0);
        }

    }
    public int getIdArrondissement(String arrondissement){
        Cursor cursor = open().rawQuery("select idarrondissement from arrondissement where nomarrondissement = ?",new String[]{arrondissement});

        if (cursor.getCount() == 0) {
            return 0;
        } else {
            cursor.moveToFirst();
            return  cursor.getInt(0);
        }

    }

    /**
     * verifie si le village entré existe déjà dans la base de données
     * @param village
     * @return
     */
    public boolean getVillageifExist(String village) {
        Cursor cursor = open().rawQuery("SELECT idvillage FROM village WHERE nomvillage= ?",new String[]{village});

        if (cursor.getCount()==0)
            return false;
        else
            return true;
    }

    public boolean insertVillage(VillageEntity villageEntity) {
        ContentValues values = new ContentValues();

        values.put(ContentDb.COLUMM_VILLAGE,villageEntity.getLibelle_village());
        long result = open().insert(ContentDb.TABLE_VILLAGE,null,values);

        if (result == -1) {
            return false;
        }else
            return true;
    }

}
