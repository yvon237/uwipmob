package com.example.yvonzobo.uwipmob.utility;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;

/**
 * Created by Yvon ZOBO on 21/07/2016.
 */
public class Utility {

    //public static final String ADRESS_SERVER ="http://46.101.211.161:8080/uWIPWS1.1/webresources/";
    public static final String ADRESS_SERVER ="http://192.168.1.9:9999/uWIPWS1.1/webresources/";
    public static final String ADRESS_SERVER_UPLOAD_IMAGE ="http://46.101.211.161:8080/uWIPWS1.0/webContent/uploadimg.jsp";

    public static boolean isNotNull(String txt){
        if (txt!=null && txt.trim().length()>=3 || !txt.equals("valeur") || !txt.equals(" "))
            return true;
        else
        return false;

    }
    public static boolean controleFormatNumber(String number) {
        if (number.length()!= 9) {
            return true;
        }else return false;
    }

    public static Bitmap getRoundedShape(Bitmap bitmap) {



        Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

        Paint paint = new Paint();

        //smooth out the edges

        paint.setAntiAlias(true);

        paint.setFilterBitmap(true);

        paint.setDither(true);

        paint.setColor(Color.parseColor("#F4661B"));

        paint.setStrokeWidth(10);



        Canvas c = new Canvas(circleBitmap);

        c.drawARGB(0, 0, 0, 0);

        //This draw a circle of Gerycolor which will be the border of image.

        c.drawCircle(bitmap.getWidth()/2, bitmap.getHeight()/2, bitmap.getWidth()/2, paint);

        BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        paint.setAntiAlias(true);

        paint.setShader(shader);

        // This will draw the image.

        c.drawCircle(bitmap.getWidth()/2, bitmap.getHeight()/2, bitmap.getWidth()/2-6, paint);

        return circleBitmap;

    }


    public static boolean compareToObect(Object object1, Object object){
        if (object1.equals(object))
            return true;
        else return false;
    }



}
