package com.example.yvonzobo.uwipmob.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.example.yvonzobo.uwipmob.models.database.ContentDb;
import com.example.yvonzobo.uwipmob.models.database.DbHelper;
import com.example.yvonzobo.uwipmob.models.entities.UserEntity;

import java.util.List;

/**
 * Created by Yvon ZOBO on 20/07/2016.
 */
public class UserController {



    public static final String CREATE_TABLE_USER = "CREATE TABLE "+
            ContentDb.TABLE_USER+" ("+
            ContentDb.COLUMM_ID_USER+" INTEGER PRIMARY KEY, "+
            ContentDb.COLUMM_ID_PROFILE+" INTEGER,"+
            ContentDb.COLUMM_ID_ORGANISATION+" INTEGER,"+
            ContentDb.COLUMM_FIRSTNAME+" TEXT, "+
            ContentDb.COLUMM_LASTNAME+" TEXT, "+
            ContentDb.COLUMM_BIRTH+" TEXT, "+
            ContentDb.COLUMM_LOGIN+" TEXT, "+
            ContentDb.COLUMM_PASSWORD+" TEXT,"+
            "FOREIGN KEY("+ContentDb.COLUMM_ID_PROFILE+") REFERENCES "+ContentDb.TABLE_PROFILE+"("+ContentDb.COLUMM_ID_PROFILE+"),"+
            "FOREIGN KEY("+ContentDb.COLUMM_ID_ORGANISATION+") REFERENCES "+ContentDb.TABLE_ORGANIZATION+"("+ContentDb.COLUMM_ID_ORGANISATION+"));";


    private DbHelper db;
    private SQLiteDatabase mydb;
    private List<UserEntity>userEntityList;
    private String[] allcolumms={
        ContentDb.COLUMM_ID_USER,
                ContentDb.COLUMM_ID_PROFILE,
                ContentDb.COLUMM_ID_ORGANISATION,
                ContentDb.COLUMM_LASTNAME,
                ContentDb.COLUMM_FIRSTNAME,
                ContentDb.COLUMM_BIRTH,
                ContentDb.COLUMM_LOGIN,
                ContentDb.COLUMM_PASSWORD
    };




    public UserController(Context context){
        db = new DbHelper(context);
    }
    /**
     * ouvre la base de données en écriture
     * @return
     */
    public SQLiteDatabase open(){

        mydb = db.getWritableDatabase();
        return mydb;
    }

    /**
     * ferme la base de données
     */
    public void close(){
        mydb.close();
    }

    public SQLiteDatabase getMydb(){
        return mydb;
    }

    public List<UserEntity> getUserEntityList() {
        return userEntityList;
    }

    public void setUserEntityList(List<UserEntity> userEntityList) {
        this.userEntityList = userEntityList;
    }

    /**
     * insert un utilisateur dans la table user
     * @param user
     * @return
     */
    public boolean insertUser(UserEntity user){
        ContentValues values = new ContentValues();
        values.put(ContentDb.COLUMM_ID_USER,user.getIduser());
        values.put(ContentDb.COLUMM_ID_PROFILE,user.getIdprofile());
        values.put(ContentDb.COLUMM_ID_ORGANISATION,user.getIdorganisation());
        values.put(ContentDb.COLUMM_FIRSTNAME,user.getFirstname());
        values.put(ContentDb.COLUMM_LASTNAME,user.getLastname());
        values.put(ContentDb.COLUMM_BIRTH,user.getDatenaiss());
        values.put(ContentDb.COLUMM_LOGIN,user.getLogin());
        values.put(ContentDb.COLUMM_PASSWORD,user.getPassword());


        long result = open().insert(ContentDb.TABLE_USER,null,values);
        if (result==-1){
            return false;
        }else
            return true;
    }

    public boolean addListUser(List<UserEntity> list){
        boolean bool=false;
        if(list.isEmpty())
            bool = false;
        for (int i=0;i<list.size();i++) {
            if (insertUser(list.get(i)))
                bool= true;
            else
                bool= false;
        }
        return bool;
    }

    public boolean checkUserBylogin_Password(String login,String mdp){
        Cursor user = null;
       try {
           user = open().rawQuery("SELECT * FROM uuser WHERE login =? AND mdp = ? ",new String[]{login,mdp});

          // cursorToUser(user);

       }catch (Exception sqle){
            sqle.printStackTrace();
       }
       if ( user.getCount()==0)
                return false;
        else return true;


    }
    public boolean getLoginFromdb(){
        Cursor cursor = null;
       try {

           cursor = open().rawQuery("select login from uuser",null);

       }catch (Exception e){
           e.printStackTrace();
       }
        if(cursor.getCount()==0){
            return false;
        }else
        return true;

    }
    public Long getIdProfile(){
        Cursor cursor;
        cursor = open().rawQuery("select idprofile from uuser",null);
        Long id = cursor.getLong(1);
        return id;
    }
    public Long getIdOrganization(){
        Cursor cursor;
        cursor = open().rawQuery("select idorganization from uuser",null);
        Long id = cursor.getLong(1);
        return id;
    }
    public boolean compareTwoString(String l1,String l2){
        if (l1.equals(l2))
            return true;
        else return false;
    }
    public boolean deleteUser(Long id){

            int result= open().delete(ContentDb.TABLE_USER, ContentDb.COLUMM_ID_PROFILE+ "= " +
                ""+id,null);

        if (result!=0){
            return true;
        }else
            return false;


    }
    /**
     * reccupere un utilisateur en fonction de son id
     * @param id
     * @return
     */
    public UserEntity getUser(int id){
        Cursor user = open().query(ContentDb.TABLE_USER, new String[]{
                ContentDb.COLUMM_ID_USER,
                ContentDb.COLUMM_ID_PROFILE,
                ContentDb.COLUMM_ID_ORGANISATION,
                ContentDb.COLUMM_FIRSTNAME,
                ContentDb.COLUMM_LASTNAME,
                ContentDb.COLUMM_BIRTH,
                ContentDb.COLUMM_LOGIN,
                ContentDb.COLUMM_PASSWORD,
                },null,null,null,ContentDb.COLUMM_ID_USER+" = "+id, null);
        return cursorToUser(user);
    }


    /**
     * retourne une instance de utilisateur avec les valeurs du curseur
     * @param cursor
     * @return
     */

    private UserEntity cursorToUser(Cursor cursor){
        if (cursor.getCount()==0)
            return null;

        UserEntity user = new UserEntity();
        user.setIduser(cursor.getLong(0));
        user.setIdprofile(cursor.getLong(1));
        user.setIdorganisation(cursor.getLong(2));
        user.setFirstname(cursor.getString(3));
        user.setLastname(cursor.getString(4));
        user.setDatenaiss(cursor.getString(5));
        user.setLogin(cursor.getString(6));
        user.setPassword(cursor.getString(7));


        cursor.close();

        return user;
    }

}
