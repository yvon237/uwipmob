package com.example.yvonzobo.uwipmob.models.menu;



/**
 * Created by Yvon ZOBO on 01/10/2016.
 */
public class MenuClient {

    private String titre;

    public MenuClient(String title) {
        titre = title;

    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }
}
