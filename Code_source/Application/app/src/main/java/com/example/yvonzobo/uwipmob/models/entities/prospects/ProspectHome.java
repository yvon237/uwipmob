package com.example.yvonzobo.uwipmob.models.entities.prospects;

/**
 * Created by Yvon ZOBO on 01/10/2016.
 */
public class ProspectHome {

    private Long idclienthome;
    private String techmatieretoit;
    private String techetattoit;
    private String techdistluminaire;
    private String techhauteurplafond;
    private String techmatiereplafond;
    private String techmatieremurs;
    private String  techdisposuppbox;
    private String techmatieresuppbox;
    private String techdispoechelle;
    private String techdispoescabeau;
    private String techitinerairetransport;


    public ProspectHome() {
    }

    public Long getIdclienthome() {
        return idclienthome;
    }

    public void setIdclienthome(Long idclienthome) {
        this.idclienthome = idclienthome;
    }

    public String getTechmatieretoit() {
        return techmatieretoit;
    }

    public void setTechmatieretoit(String techmatieretoit) {
        this.techmatieretoit = techmatieretoit;
    }

    public String getTechetattoit() {
        return techetattoit;
    }

    public void setTechetattoit(String techetattoit) {
        this.techetattoit = techetattoit;
    }

    public String getTechdistluminaire() {
        return techdistluminaire;
    }

    public void setTechdistluminaire(String techdistluminaire) {
        this.techdistluminaire = techdistluminaire;
    }

    public String getTechhauteurplafond() {
        return techhauteurplafond;
    }

    public void setTechhauteurplafond(String techhauteurplafond) {
        this.techhauteurplafond = techhauteurplafond;
    }

    public String getTechmatiereplafond() {
        return techmatiereplafond;
    }

    public void setTechmatiereplafond(String techmatiereplafond) {
        this.techmatiereplafond = techmatiereplafond;
    }

    public String getTechmatieremurs() {
        return techmatieremurs;
    }

    public void setTechmatieremurs(String techmatieremurs) {
        this.techmatieremurs = techmatieremurs;
    }

    public String getTechdisposuppbox() {
        return techdisposuppbox;
    }

    public void setTechdisposuppbox(String techdisposuppbox) {
        this.techdisposuppbox = techdisposuppbox;
    }

    public String getTechmatieresuppbox() {
        return techmatieresuppbox;
    }

    public void setTechmatieresuppbox(String techmatieresuppbox) {
        this.techmatieresuppbox = techmatieresuppbox;
    }

    public String getTechdispoechelle() {
        return techdispoechelle;
    }

    public void setTechdispoechelle(String techdispoechelle) {
        this.techdispoechelle = techdispoechelle;
    }

    public String getTechdispoescabeau() {
        return techdispoescabeau;
    }

    public void setTechdispoescabeau(String techdispoescabeau) {
        this.techdispoescabeau = techdispoescabeau;
    }

    public String getTechitinerairetransport() {
        return techitinerairetransport;
    }

    public void setTechitinerairetransport(String techitinerairetransport) {
        this.techitinerairetransport = techitinerairetransport;
    }
}
