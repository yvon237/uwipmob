package com.example.yvonzobo.uwipmob.views.fragments.createupdateprospect;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yvonzobo.uwipmob.R;
import com.example.yvonzobo.uwipmob.bundle.ConstructBundleProspect;
import com.example.yvonzobo.uwipmob.controllers.LocaliteController;
import com.example.yvonzobo.uwipmob.controllers.OperatorController;
import com.example.yvonzobo.uwipmob.models.entities.Arrondissement;
import com.example.yvonzobo.uwipmob.models.entities.Departement;
import com.example.yvonzobo.uwipmob.models.entities.Region;
import com.example.yvonzobo.uwipmob.models.entities.VillageEntity;
import com.example.yvonzobo.uwipmob.models.entities.prospects.CoordonnesProspectEntity;
import com.example.yvonzobo.uwipmob.utility.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CoordonneesProspectFragment.CoordonnesProspectFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CoordonneesProspectFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CoordonneesProspectFragment extends Fragment implements AdapterView.OnItemSelectedListener{



    private CoordonnesProspectFragmentInteractionListener mListener;


    // textview pour les valeurs des differents champs de types text des dialog
    private TextView t_sourcing, t_mobilemoney,t_localite,t_phoneprefer,t_phoneorange,t_phonemtn,t_phonenexttel,t_operator,
            t_departement,t_village,t_region, t_arrondissement, t_zone;
    // les textview des differents libelle
    private TextView l_phonenumber,
            l_sourcing,l_mobilemoney,l_localite,l_operator,l_zone,l_sourceeclairage;
    // les layout de chaque bloc on affecte l'evenement onclicklistener
    private LinearLayout layoutsourceeclairage,layoutphonenumber,
            layoutsourcing,layoutoperator,layoutmobilemoney,layoutlocalite,layoutzone;
    //la boite de dialog
    private  AlertDialog.Builder builder;
    // les editText se trouvant dans les differents boites de dialog
    private EditText editText,editText2,editText3,editText4;
    //textInputLayout utilisé pour le design
    private TextInputLayout textInputLayout,textInputLayout1,textInputLayout2,textInputLayout3;
    //button enregister au serveur et en local et button choix de l'image dans la gallery
    private ImageView suivant, precedent;
    // spinner pour les selections des pays
    private Spinner spinnerzone,spinnersourcing;

    private String[] zone ={"Urbain","Péri-urbain","Rural"};
    private String[] sourcing = {"Démarche commercial","Boutique","Spot radio","Bouche à oreille","Bouche à oreille client","Site internet","Facebook","Leader d'opinion"};

    // utiliser pour connaitre le numero preferer du client
    private RadioGroup radioGroup;
    private RadioButton radioyes,radiono,radioOrange,radioMtn,radioNexttel;
    // le bean client
    private CoordonnesProspectEntity coordonnesProspectEntity = new CoordonnesProspectEntity();
    private Region region = new Region();
    private Departement departement = new Departement();
    private Arrondissement arrondissement = new Arrondissement();
    private VillageEntity village = new VillageEntity();
    private OperatorController operatorController;
    private LocaliteController localiteController;
    private int contraleSpinner =0;

    public CoordonneesProspectFragment() {
        // Required empty public constructor
    }

    /**
     * reccuperer le bundle de l'identité du prospect
     * @param identity
     * @return
     */
    public static CoordonneesProspectFragment newInstance(Bundle identity) {
        CoordonneesProspectFragment fragment = new CoordonneesProspectFragment();
        Bundle args = new Bundle();
        args.putBundle("identite",identity);

        fragment.setArguments(args);

        return fragment;
    }

    public static CoordonneesProspectFragment newInstance(Bundle coordonnees, String whatcall) {
        CoordonneesProspectFragment fragment = new CoordonneesProspectFragment();
        Bundle args = new Bundle();
        args.putBundle("coordonnes",coordonnees);
        args.putString("whatcall",whatcall); // puie sur le bouton retour du fragment suivant ou appel pour modifier


        fragment.setArguments(args);

        return fragment;
    }

    /**
     * extraction du bundle identé
     * @return
     */

    public Bundle getBundleIdentite() {
        return getArguments().getBundle("identite");
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_coordonnees_prospect, container, false);


            Toast.makeText(getActivity(),getBundleIdentite().getString("firstname"),Toast.LENGTH_LONG).show();


        t_sourcing = (TextView)view.findViewById(R.id.t_sourcing);
        t_mobilemoney = (TextView)view.findViewById(R.id.t_mobilemoney);

        t_phoneprefer  = (TextView)view.findViewById(R.id.t_phoneprefer);
        t_phoneorange = (TextView)view.findViewById(R.id.t_phoneorange);
        t_phonemtn = (TextView)view.findViewById(R.id.t_phonemtn);
        t_phonenexttel = (TextView)view.findViewById(R.id.t_phonenexttel);
        t_departement = (TextView)view.findViewById(R.id.t_municipality);
        t_village= (TextView)view.findViewById(R.id.t_village);
        t_operator = (TextView)view.findViewById(R.id.t_operator);
        t_region = (TextView)view.findViewById(R.id.t_region);
        t_zone = (TextView)view.findViewById(R.id.t_zone);
        t_arrondissement = (TextView)view.findViewById(R.id.t_arrondissement);





        l_phonenumber = (TextView)view.findViewById(R.id.l_phonenumber);
        l_sourcing = (TextView)view.findViewById(R.id.l_sourcing);

        l_mobilemoney = (TextView)view.findViewById(R.id.l_mobilemoney);
        l_localite = (TextView)view.findViewById(R.id.l_localite);
        l_operator = (TextView)view.findViewById(R.id.l_operator);
        l_zone = (TextView)view.findViewById(R.id.l_zone);



        layoutphonenumber = (LinearLayout)view.findViewById(R.id.layoutphonenumberr);


        layoutsourcing = (LinearLayout)view.findViewById(R.id.layoutsourcing);
        layoutmobilemoney = (LinearLayout)view.findViewById(R.id.layoutmobilemoney);
        layoutlocalite = (LinearLayout)view.findViewById(R.id.layoutlocalite);
        layoutoperator =(LinearLayout)view.findViewById(R.id.layoutoperator);
        layoutzone =(LinearLayout)view.findViewById(R.id.layoutzone);


        suivant = (ImageView)view.findViewById(R.id.nextpage);
        precedent = (ImageView)view.findViewById(R.id.previouspage);

        operatorController = new OperatorController(getActivity());
        localiteController = new LocaliteController(getActivity());

        onClickLayoutLocalite();
        onClickLayoutMobileMoney();
       // onClickLayoutOperator();
        onClickLayoutPhoneNumber();
        onClickLayoutsourcing();
        onClickLayoutZone();


        onClickBSuivant();
        onClickPrecedent();

        return view;
    }


    public void onClickBSuivant() {
        suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controleCoordonnesProspect();
                      }


        });
    }

    private void onClickPrecedent() {
        precedent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onPassDataCallBackIdentiteProspectInteraction(getBundleIdentite(), "precedent");
            }
        });
    }



    private  void onClickLayoutPhoneNumber(){
        layoutphonenumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogPhoneNumebers(l_phonenumber);
            }
        });
    }
    private void onClickLayoutOperator(){
        layoutoperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogOperateurMobile(l_operator);
            }
        });
    }

    private  void onClickLayoutsourcing(){
        layoutsourcing.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onCreateDialogSourcing(l_sourcing);

            }
        });
    }

    /**
     * appel de la boite de dialogue mobile money
     * si le prospect dispose d'un compte mobile money
     * oui
     * non
     */
    private  void onClickLayoutMobileMoney(){
        layoutmobilemoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogMobileMoney(l_mobilemoney);
            }
        });
    }

    /**
     * appel de la boite de dialogue localite
     * à l'interiue on
     * la region du prospect
     * le departement ou municipality
     * le village
     */
    private  void onClickLayoutLocalite(){
        layoutlocalite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogLocalite(l_localite);
            }
        });
    }

    /**
     * appel de la boite de dialogue zone
     * les types de zone
     * urbain
     * peri-urbain
     * rural
     */

    private void onClickLayoutZone() {
        layoutzone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                l_zone.setText("Zone du prospect");
                onCreateDialogZone(l_zone);
            }
        });
    }

    /**
     * passage des bundle identite et coordonnees au fragment suivant
     * @param identite
     * @param coordonnees
     */
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Bundle identite,Bundle coordonnees) {
        if (mListener != null) {
            mListener.onCommunicateDataInteraction(identite,coordonnees);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CoordonnesProspectFragmentInteractionListener) {
            mListener = (CoordonnesProspectFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    private Dialog onCreateDialogZone(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        spinnerzone = new Spinner(getActivity());
        spinnerzone.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,zone);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerzone.setAdapter(adapterType);

        contraleSpinner =2;

        builder.setView(spinnerzone);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonZoneClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativezoneButtonClickListener());

        return builder.show();
    }



    private class OnPositiveButtonZoneClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativezoneButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }


    private Dialog onCreateDialogSourcing(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        spinnersourcing = new Spinner(getActivity());
        spinnersourcing.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,sourcing);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnersourcing.setAdapter(adapterType);

        contraleSpinner=3;



        builder.setView(spinnersourcing);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonSourcingClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeSourcingButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonSourcingClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeSourcingButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }






    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (contraleSpinner == 2) {
                t_zone.setText(zone[i]);
        } else if (contraleSpinner == 3) {
            t_sourcing.setText(sourcing[i]);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private Dialog onCreateDialogPhoneNumebers(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        textInputLayout = new TextInputLayout(getActivity());
        textInputLayout1 = new TextInputLayout(getActivity());
        textInputLayout2 = new TextInputLayout(getActivity());
        textInputLayout3 = new TextInputLayout(getActivity());
        LinearLayout layout = new LinearLayout(getActivity());
        LinearLayout layoutRadioGroup = new LinearLayout(getActivity());
        ScrollView scrollView = new ScrollView(getActivity());

        TextView favorite = new TextView(getActivity());
        favorite.setText("Numéro preféré");
        favorite.setGravity(Gravity.CENTER);
        favorite.setTextSize(20);




        layoutRadioGroup.setOrientation(LinearLayout.HORIZONTAL);
        layout.setOrientation(LinearLayout.VERTICAL);

        radioGroup = new RadioGroup(getActivity());
        radioOrange = new RadioButton(getActivity());
        radioOrange.setId(RadioButton.generateViewId());
        radioOrange.setText("Orange");
        radioMtn = new RadioButton(getActivity());
        radioMtn.setId(RadioButton.generateViewId());
        radioMtn.setText("Mtn");

        radioNexttel = new RadioButton(getActivity());
        radioNexttel.setId(RadioButton.generateViewId());
        radioNexttel.setText("Nexttel");



        radioGroup.addView(radioOrange);
        radioGroup.addView(radioMtn);
        radioGroup.addView(radioNexttel);
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        layoutRadioGroup.addView(radioGroup);



        editText2 = new EditText(getActivity());
        editText2.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText2.setHint("Numéro Orange");

        editText3 = new EditText(getActivity());
        editText3.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText3.setHint("Numéro Mtn");


        editText4 = new EditText(getActivity());
        editText4.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText4.setHint("Numéro Nexttel");



        textInputLayout1.addView(editText2);
        textInputLayout2.addView(editText3);
        textInputLayout3.addView(editText4);



        layout.addView(textInputLayout1);
        layout.addView(textInputLayout2);
        layout.addView(textInputLayout3);
        layout.addView(favorite);
        layout.addView(layoutRadioGroup);


        scrollView.addView(layout);

        builder.setView(scrollView);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonPhoneNumberClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativePhoneNumberButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonPhoneNumberClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            String orange = editText2.getText().toString();
            String mtn = editText3.getText().toString();
            String nexttel = editText4.getText().toString();
            if (radioGroup.getCheckedRadioButtonId() == radioOrange.getId()){
                coordonnesProspectEntity.setPhoneorange(orange);
                coordonnesProspectEntity.setPreferredphone(orange);
                if (mtn.equals("")){
                    coordonnesProspectEntity.setPhonemtn("non");
                }else
                    coordonnesProspectEntity.setPhonemtn(mtn);
                if (nexttel.equals(""))
                    coordonnesProspectEntity.setPhonenexttel("non");
                else
                    coordonnesProspectEntity.setPhonenexttel(nexttel);

                t_phoneprefer.setText(coordonnesProspectEntity.getPreferredphone());
                t_phoneorange.setText(coordonnesProspectEntity.getPhoneorange());
                t_phonemtn.setText(coordonnesProspectEntity.getPhonemtn());
                t_phonenexttel.setText(coordonnesProspectEntity.getPhonenexttel());
            }else if (radioGroup.getCheckedRadioButtonId()== radioMtn.getId()){
                coordonnesProspectEntity.setPreferredphone(mtn);
                coordonnesProspectEntity.setPhonemtn(mtn);

                if (orange.equals("")){
                    coordonnesProspectEntity.setPhoneorange("non");
                }else
                    coordonnesProspectEntity.setPhoneorange(orange);
                if (nexttel.equals(""))
                    coordonnesProspectEntity.setPhonenexttel("non");
                else
                    coordonnesProspectEntity.setPhonenexttel(nexttel);

                t_phoneprefer.setText(coordonnesProspectEntity.getPreferredphone());
                t_phoneorange.setText(coordonnesProspectEntity.getPhoneorange());
                t_phonemtn.setText(coordonnesProspectEntity.getPhonemtn());
                t_phonenexttel.setText(coordonnesProspectEntity.getPhonenexttel());

            }else if (radioGroup.getCheckedRadioButtonId()== radioNexttel.getId()){
                coordonnesProspectEntity.setPreferredphone(nexttel);
                coordonnesProspectEntity.setPhonenexttel(nexttel);

                if (orange.equals("")){
                    coordonnesProspectEntity.setPhoneorange("non");
                }else
                    coordonnesProspectEntity.setPhoneorange(orange);
                if (mtn.equals(""))
                    coordonnesProspectEntity.setPhonemtn("non");
                else
                    coordonnesProspectEntity.setPhonemtn(nexttel);


                t_phoneprefer.setText(coordonnesProspectEntity.getPreferredphone());
                t_phoneorange.setText(coordonnesProspectEntity.getPhoneorange());
                t_phonemtn.setText(coordonnesProspectEntity.getPhonemtn());
                t_phonenexttel.setText(coordonnesProspectEntity.getPhonenexttel());

            }






            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativePhoneNumberButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }


    private Dialog onCreateDialogOperateurMobile(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        LinearLayout layoutRadioGroup = new LinearLayout(getActivity());
        layoutRadioGroup.setOrientation(LinearLayout.HORIZONTAL);

        radioGroup = new RadioGroup(getActivity());
        radioOrange = new RadioButton(getActivity());
        radioOrange.setId(RadioButton.generateViewId());
        radioOrange.setText("Orange");
        radioMtn = new RadioButton(getActivity());
        radioMtn.setId(RadioButton.generateViewId());
        radioMtn.setText("Mtn");

        radioNexttel = new RadioButton(getActivity());
        radioNexttel.setId(RadioButton.generateViewId());
        radioNexttel.setText("Nexttel");



        radioGroup.addView(radioOrange);
        radioGroup.addView(radioMtn);
        radioGroup.addView(radioNexttel);
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        layoutRadioGroup.addView(radioGroup);


        builder.setView(layoutRadioGroup);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonOperatorMobileClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeOperatorButtonMobileClickListener());

        return builder.show();
    }

    private class OnPositiveButtonOperatorMobileClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            if (radioGroup.getCheckedRadioButtonId() == radioOrange.getId()){
                t_operator.setText("ORANGE-CM");
            } else if (radioGroup.getCheckedRadioButtonId() == radioMtn.getId()) {
                t_operator.setText("MTN-CM");
            } else if (radioGroup.getCheckedRadioButtonId() == radioNexttel.getId()) {
                t_operator.setText("NEXTTEL-CM");
            }



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeOperatorButtonMobileClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    private Dialog onCreateDialogMobileMoney(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());

        radioGroup = new RadioGroup(getActivity());
        radioGroup.setId(RadioGroup.generateViewId());
        radioyes = new RadioButton(getActivity());
        radioyes.setId(RadioButton.generateViewId());
        radioyes.setText("oui");
        radiono = new RadioButton(getActivity());
        radiono.setText("Non");
        radiono.setId(RadioButton.generateViewId());
        LinearLayout layout = new LinearLayout(getActivity());

        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        radioGroup.addView(radioyes);
        radioGroup.addView(radiono);
        layout.addView(radioGroup);
        layout.setOrientation(LinearLayout.HORIZONTAL);

        builder.setView(layout);



        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonMobilemoneyClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeMobileMoneyButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonMobilemoneyClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            if (radioGroup.getCheckedRadioButtonId()== radioyes.getId())
                t_mobilemoney.setText("Oui");
            else if (radioGroup.getCheckedRadioButtonId()== radiono.getId())
                t_mobilemoney.setText("Non");


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeMobileMoneyButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    private Dialog onCreateDialogLocalite(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText2 = new EditText(getActivity());
        editText3 = new EditText(getActivity());
        editText4 = new EditText(getActivity());
        editText3.setInputType(InputType.TYPE_CLASS_TEXT);
        editText3.setHint("Région");
        editText2.setInputType(InputType.TYPE_CLASS_TEXT);
        editText2.setHint("Village");
        textInputLayout = new TextInputLayout(getActivity());
        textInputLayout1 = new TextInputLayout(getActivity());
        textInputLayout3 = new TextInputLayout(getActivity());
        textInputLayout2 = new TextInputLayout(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setHint("Département");
        editText4.setInputType(InputType.TYPE_CLASS_TEXT);
        editText4.setHint("Arrondissement");





        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        textInputLayout3.addView(editText3);
        textInputLayout.addView(editText);
        textInputLayout1.addView(editText2);
        textInputLayout2.addView(editText4);

       // layout.addView(textInputLayout3);
       // layout.addView(textInputLayout);
       // layout.addView(textInputLayout2);
        layout.addView(textInputLayout1);


        builder.setView(layout);

        builder.setCancelable(false);




        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonLocaliteClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeLocaliteClickListener());

        return builder.show();
    }
    private class OnPositiveButtonLocaliteClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

           // t_region.setText(editText3.getText().toString());
           // t_departement.setText(editText.getText().toString());
           // t_arrondissement.setText(editText4.getText().toString());
            t_village.setText(editText2.getText().toString());





            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeLocaliteClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }

    /**
     * verification des champs entres
     * envoi des informations via des Bundle
     * Bundle identite
     * Bundle coordonnees
     */

    public void controleCoordonnesProspect() {
         if  (t_phoneprefer.getText().equals("valeur") || t_phoneprefer.getText().equals(" ") || !Utility.isNotNull(t_phoneprefer.getText().toString()) || t_phoneprefer.getText().toString().equals("non")) {
            Toast.makeText(getActivity(), "numéro permanent est obligatoire!!", Toast.LENGTH_LONG).show();
        }  else if (t_mobilemoney.getText().equals("valeur")) {
            Toast.makeText(getActivity(),"mobile money invalide!",Toast.LENGTH_LONG).show();
        }   /*else if (t_region.getText().equals("valeur") || !Utility.isNotNull(t_region.getText().toString())) {
            Toast.makeText(getActivity(),"préciser la région du prospect!",Toast.LENGTH_LONG).show();
        }  else if (t_departement.getText().equals("valeur") || !Utility.isNotNull(t_departement.getText().toString())) {
             Toast.makeText(getActivity(),"préciser le départemment du prospect!",Toast.LENGTH_LONG).show();
         }else if (t_arrondissement.getText().equals("valeur")) {
            Toast.makeText(getActivity(),"arrondissement prospect invalide!",Toast.LENGTH_LONG).show();
        }*/ else if (t_village.getText().equals("valeur") || !Utility.isNotNull(t_village.getText().toString())) {
            Toast.makeText(getActivity(),"préciser le village du prospect!",Toast.LENGTH_LONG).show();
        }else if (t_zone.getText().equals("valeur")) {
            Toast.makeText(getActivity(),"zone du prospect invalide!",Toast.LENGTH_LONG).show();
        }else if (t_sourcing.getText().equals("valeur")) {
            Toast.makeText(getActivity(),"préciser la source d'informations du prospect",Toast.LENGTH_LONG).show();
        }else if (!t_phoneorange.getText().equals("non") && Utility.controleFormatNumber(t_phoneorange.getText().toString())) {
            Toast.makeText(getActivity(),"numéro orange invalide, 9 chiffres!",Toast.LENGTH_LONG).show();
        }else if (!t_phonemtn.getText().equals("non") && Utility.controleFormatNumber(t_phonemtn.getText().toString())) {
            Toast.makeText(getActivity(),"numéro mtn invalide, 9 chiffres!",Toast.LENGTH_LONG).show();
        }else if (!t_phonenexttel.getText().equals("non") && Utility.controleFormatNumber(t_phoneorange.getText().toString())) {
            Toast.makeText(getActivity(),"numéro nexttel invalide, 9 chiffres!",Toast.LENGTH_LONG).show();
        }else {
            coordonnesProspectEntity.setPreferredphone(t_phoneprefer.getText().toString());
            coordonnesProspectEntity.setPhoneorange(t_phoneorange.getText().toString());
            coordonnesProspectEntity.setPhonemtn(t_phonemtn.getText().toString());
            coordonnesProspectEntity.setPhonenexttel(t_phonenexttel.getText().toString());
            coordonnesProspectEntity.setMobilemoney(t_mobilemoney.getText().toString());
            coordonnesProspectEntity.setSourcing(t_sourcing.getText().toString());
           // coordonnesProspectEntity.setIdoperator(Long.valueOf(operatorController.getIdoperator(t_operator.getText().toString())));

                // verification de l'existence du village et ajout si non existant
            /* if (!localiteController.getVillageifExist(t_village.getText().toString())) {
                 village.setLibelle_village(t_village.getText().toString());
                 if (localiteController.insertVillage(village))
                     Toast.makeText(getActivity(),"village inserer avec succès!",Toast.LENGTH_LONG).show();
             }
             //recuperation id village correspondant
             coordonnesProspectEntity.setIdvillage(Long.valueOf(localiteController.getIdvillage(t_village.getText().toString())));

*/
            // passage des infos par des Bundle au fragment suivant
            onButtonPressed(getBundleIdentite(), ConstructBundleProspect.constructBundleCoordonnees(coordonnesProspectEntity));



        }


    }

    public void onRestoreInstanceState() {
        try {
            if (getArguments().getBundle("coordonnes") != null) {
                if (getArguments().getString("whatcall") == "precedent") {
                    t_phoneprefer.setText(getArguments().getBundle("coordonnees").getString("preferredphone"));
                    t_phoneorange.setText(getArguments().getBundle("coordonnees").getString("phoneorange"));
                    t_phonemtn.setText(getArguments().getBundle("coordonnees").getString("phonemtn"));
                    t_phonenexttel.setText(getArguments().getBundle("coordonnees").getString("phonenexttel"));
                    t_sourcing.setText(getArguments().getBundle("coordonnees").getString("sourcing"));
                    t_mobilemoney.setText(getArguments().getBundle("coordonnees").getString("mobilemoney"));
                    t_zone.setText(getArguments().getBundle("coordonnees").getString("typezone"));
                }
            }

        } catch (Exception e) {

        }
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface CoordonnesProspectFragmentInteractionListener {
        // TODO: Update argument type and name
        void onCommunicateDataInteraction(Bundle identite,Bundle coordonnees);
        void onPassDataCallBackIdentiteProspectInteraction(Bundle identiteprospect, String precedent);
    }
}
