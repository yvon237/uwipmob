package com.example.yvonzobo.uwipmob.models.entities.prospects;

/**
 * Created by Yvon ZOBO on 12/10/2016.
 */
public class SuiviProspectEntity {

    private String suivi_lieurencontre;
    private int suivi_totalrelanceprospection;
    private int suivi_totalrelancepaiement;
    private int suivi_alerte;
    private String suivi_dateacompteprevue; // date paiement acompte


    public String getSuivi_lieurencontre() {
        return suivi_lieurencontre;
    }

    public void setSuivi_lieurencontre(String suivi_lieurencontre) {
        this.suivi_lieurencontre = suivi_lieurencontre;
    }

    public int getSuivi_totalrelanceprospection() {
        return suivi_totalrelanceprospection;
    }

    public void setSuivi_totalrelanceprospection(int suivi_totalrelanceprospection) {
        this.suivi_totalrelanceprospection = suivi_totalrelanceprospection;
    }

    public int getSuivi_totalrelancepaiement() {
        return suivi_totalrelancepaiement;
    }

    public void setSuivi_totalrelancepaiement(int suivi_totalrelancepaiement) {
        this.suivi_totalrelancepaiement = suivi_totalrelancepaiement;
    }

    public int getSuivi_alerte() {
        return suivi_alerte;
    }

    public void setSuivi_alerte(int suivi_alerte) {
        this.suivi_alerte = suivi_alerte;
    }

    public String getSuivi_dateacompteprevue() {
        return suivi_dateacompteprevue;
    }

    public void setSuivi_dateacompteprevue(String suivi_dateacompteprevue) {
        this.suivi_dateacompteprevue = suivi_dateacompteprevue;
    }
}
