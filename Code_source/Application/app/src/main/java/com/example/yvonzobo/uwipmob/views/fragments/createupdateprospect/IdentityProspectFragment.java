package com.example.yvonzobo.uwipmob.views.fragments.createupdateprospect;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yvonzobo.uwipmob.R;
import com.example.yvonzobo.uwipmob.bundle.ConstructBundleProspect;
import com.example.yvonzobo.uwipmob.models.entities.prospects.IdentiteProspectEntity;
import com.example.yvonzobo.uwipmob.utility.ConstructJsonObject;
import com.example.yvonzobo.uwipmob.utility.Utility;


public class IdentityProspectFragment extends Fragment implements AdapterView.OnItemSelectedListener{



    private OnIdentityProspectInteractionListener mListener;

    // textview pour les valeurs des differents champs de types text des dialog
    private TextView t_nationality,t_lastname,t_firstname,t_age,t_langue, t_sexe,t_email;
    // les textview des differents libelle
    private TextView l_nationality,l_lastname,l_firstname, l_age,l_langue,l_sexe,l_email;
    // les layout de chaque bloc on affecte l'evenement onclicklistener
    private LinearLayout layoutnationality,layoutlastname,layoutfirstname,layoutage,layoutsexe,layoutlangue, layoutemail;
    //la boite de dialog
    private  AlertDialog.Builder builder;
    // les editText se trouvant dans les differents boites de dialog
    private EditText editText;
    // spinner pour les selections des pays
    private Spinner spinnerlangue, spinner;
    private String[] country ={"Cameroun","Congo","Gabon","Guinnée Equatoriale","Mali"};

    private String[] langue ={"Français","Anglais","Bilingue","Autres"};
    private String[] zone ={"Urbain","Péri-urbain","Rural"};

    private int controleSpinner=0; //controle quel menu deroulant l'utilisateur a cliquer

    private IdentiteProspectEntity identiteProspectEntity = new IdentiteProspectEntity();

    // utiliser pour connaitre le numero preferer du client
    private RadioGroup radioGroup;
    private RadioButton radiohomme, radiofemme;
    private ImageView b_suivant;

    public IdentityProspectFragment() {
        // Required empty public constructor
    }


    public static IdentityProspectFragment newInstance() {
        IdentityProspectFragment fragment = new IdentityProspectFragment();
        return fragment;
    }

    /**
     * instance permettant de faire retour arriere ou modifier selon la valeur
     * de la variable whatcall (qui appel)
     * @param identite
     * @param whatcall
     * @return
     */
    public static IdentityProspectFragment newInstance(Bundle identite, String whatcall) {
        IdentityProspectFragment fragment = new IdentityProspectFragment();
        Bundle args = new Bundle();
        args.putBundle("identite",identite);
        args.putString("whatcall",whatcall); // onbackpressed ou appel pour modifier


        fragment.setArguments(args);

        return fragment;
    }

    /**
     * retauration de l'instance
     * losqu'on clique sur le bouton retour du fragment coordonnees prospect
     */
    public void onRestoreInstanceState() {

        try {
            if (getArguments().getBundle("identite") != null) {
                String whatcall= getArguments().getString("whatcall");
                if (whatcall == "precedent") {
                    t_firstname.setText(getArguments().getBundle("identite").getString("firstname"));
                    t_lastname.setText(getArguments().getBundle("identite").getString("lastname"));
                    t_langue.setText(getArguments().getBundle("identite").getString("langue"));
                    t_age.setText(getArguments().getBundle("identite").getString("age"));
                    t_email.setText(getArguments().getBundle("identite").getString("email"));
                    t_sexe.setText(getArguments().getBundle("identite").getString("sexe"));
                    t_nationality.setText(getArguments().getBundle("identite").getString("nationality"));

                }

            }
        } catch (Exception e) {
            e.getCause();
            e.getMessage();

        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_identity_prospect, container, false);

        t_lastname = (TextView)view.findViewById(R.id.t_lastname);
        t_firstname = (TextView)view.findViewById(R.id.t_firstname);
        t_age = (TextView)view.findViewById(R.id.t_age);
        t_langue = (TextView)view.findViewById(R.id.t_langue);
        t_sexe = (TextView)view.findViewById(R.id.t_sexe);
        t_nationality = (TextView)view.findViewById(R.id.t_nationality);
        t_email = (TextView)view.findViewById(R.id.t_email);




        l_lastname = (TextView)view.findViewById(R.id.l_lastname);
        l_firstname = (TextView)view.findViewById(R.id.l_firstname);
        l_age = (TextView)view.findViewById(R.id.l_age);
        l_langue = (TextView)view.findViewById(R.id.l_langue);
        l_sexe = (TextView)view.findViewById(R.id.l_sexe);
        l_nationality = (TextView)view.findViewById(R.id.l_nationality);
        l_email = (TextView)view.findViewById(R.id.l_email);



        layoutlastname = (LinearLayout)view.findViewById(R.id.layoutnom);
        layoutfirstname = (LinearLayout)view.findViewById(R.id.layoutprenom);
        layoutage =(LinearLayout)view.findViewById(R.id.layoutage);
        layoutsexe =(LinearLayout)view.findViewById(R.id.layoutsexe);
        layoutlangue =(LinearLayout)view.findViewById(R.id.layoutlangue);
        layoutnationality = (LinearLayout)view.findViewById(R.id.layoutnationality);
        layoutemail = (LinearLayout)view.findViewById(R.id.layoutemail);


        b_suivant = (ImageView)view.findViewById(R.id.b_suivant);

        onClickLayoutFirstname();
        onClickLayoutLastname();
        onClickLayoutAge();
        onClickLayoutSexe();
        onClickLayoutLangue();
        onClickLayoutNationality();
        onClickLayoutEmail();
        onClickBSuivant();


        onRestoreInstanceState();




        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Bundle identite) {
        if (mListener != null) {
            mListener.identiteProspectInteraction(identite);
        }
    }

    public void onClickBSuivant() {
        b_suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifIdentity();
                Log.d("identite", ConstructJsonObject.convertGSON(identiteProspectEntity));
                Log.d("extraitIdentite", String.valueOf(ConstructJsonObject.extratGSON(ConstructJsonObject.convertGSON(identiteProspectEntity))));

            }
        });
    }


    private  void onClickLayoutNationality(){
        layoutnationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogNationality(l_nationality);
            }
        });
    }
    private  void onClickLayoutLastname(){
        layoutlastname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogLastname(l_lastname);
            }
        });
    }

    private  void onClickLayoutFirstname(){
        layoutfirstname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogFirstname(l_firstname);
            }
        });
    }

    private void onClickLayoutAge() {
        layoutage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogAge(l_age);

            }
        });
    }



    private void onClickLayoutLangue() {
        layoutlangue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogLangue(l_langue);
            }
        });
    }

    private void onClickLayoutSexe() {
        layoutsexe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogSexe(l_sexe);
            }
        });
    }

    private void onClickLayoutEmail() {
        layoutemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogEmail(l_email);
            }
        });
    }



    private Dialog onCreateDialogLastname(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);

        builder.setView(editText);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonLastnameClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeLastnameButtonClickListener());

        return builder.show();
    }




    private class OnPositiveButtonLastnameClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            String valeur = editText.getText().toString();
            t_lastname.setText(valeur);
            identiteProspectEntity.setLastname(valeur);


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeLastnameButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }







    private Dialog onCreateDialogFirstname(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);

        builder.setView(editText);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonFirstnameClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeFirstnameButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonFirstnameClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            final String valeur = editText.getText().toString();
            t_firstname.setText(valeur);
            identiteProspectEntity.setFirstname(valeur);


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeFirstnameButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }





    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


        if (controleSpinner == 1) {
            t_nationality.setText(country[i]);
        } else if (controleSpinner==2)
            t_langue.setText(langue[i]);

    }
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private Dialog onCreateDialogLangue(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        spinnerlangue = new Spinner(getActivity());
        spinnerlangue.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,langue);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerlangue.setAdapter(adapterType);

        controleSpinner = 2;

        builder.setView(spinnerlangue);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonLangueClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeLangueButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonLangueClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeLangueButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }

    public Dialog onCreateDialogSexe(TextView labelle) {
        builder= new AlertDialog.Builder(getActivity());


        LinearLayout layoutRadioGroup = new LinearLayout(getActivity());
        layoutRadioGroup.setOrientation(LinearLayout.HORIZONTAL);

        radioGroup = new RadioGroup(getActivity());
        radiohomme = new RadioButton(getActivity());
        radiohomme.setId(RadioButton.generateViewId());
        radiohomme.setText("Masculin");
        radiofemme = new RadioButton(getActivity());
        radiofemme.setId(RadioButton.generateViewId());
        radiofemme.setText("Feminin");





        radioGroup.addView(radiohomme);
        radioGroup.addView(radiofemme);

        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        layoutRadioGroup.addView(radioGroup);


        builder.setView(layoutRadioGroup);

        builder.setTitle(labelle.getText());

        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonSexeClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeSexeButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonSexeClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            if (radioGroup.getCheckedRadioButtonId()== radiofemme.getId())
                t_sexe.setText("Feminin");
            else if (radioGroup.getCheckedRadioButtonId()== radiohomme.getId())
                t_sexe.setText("Masculin");


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeSexeButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }

    private Dialog onCreateDialogNationality(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        spinner = new Spinner(getActivity());
        spinner.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,country);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterType);

        controleSpinner=1;



        builder.setView(spinner);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonNationalityClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeNationalityButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonNationalityClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeNationalityButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }


    private Dialog onCreateDialogAge(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);

        builder.setView(editText);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonAgeClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeAgeButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonAgeClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            final String valeur = editText.getText().toString();
            t_age.setText(valeur);

            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeAgeButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }

    public void onCreateDialogEmail(TextView libelle){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle(libelle.getText().toString());

        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setHint(getResources().getString(R.string.email));

        alertDialog.setCancelable(false);
        alertDialog.setView(editText);


        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {



            public void onClick(DialogInterface dialog,int which) {

                t_email.setText(editText.getText().toString());

                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });

            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnIdentityProspectInteractionListener) {
            mListener = (OnIdentityProspectInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }



    /**
     * verification des informations entrees
     * et envoi au fragment coordonnes par un Bundle
     *
     */
    public void verifIdentity() {
        if (t_nationality.getText().equals("valeur") || !Utility.isNotNull(t_nationality.getText().toString())) {
            Toast.makeText(getActivity(),"choisir une nationalité,obligatoire!",Toast.LENGTH_LONG).show();

        }else if (!Utility.isNotNull(t_lastname.getText().toString()) || t_lastname.getText().toString().equals("valeur")) {
            Toast.makeText(getActivity(),"Nom client invalide!",Toast.LENGTH_LONG).show();
        } else if (!Utility.isNotNull(t_firstname.getText().toString()) || t_firstname.getText().toString().equals("valeur")) {
            Toast.makeText(getActivity(),"Prénom client invalide!",Toast.LENGTH_LONG).show();
        } else if (t_age.getText().toString().equals("valeur") || t_age.getText().toString().equals(" ")) {
            Toast.makeText(getActivity(), "Age invalide!",Toast.LENGTH_LONG).show();
        }else if (!Utility.isNotNull(t_langue.getText().toString()) || t_langue.getText().toString().equals("valeur")) {
            Toast.makeText(getActivity(), "Langue invalide!",Toast.LENGTH_LONG).show();
        }else if (!Utility.isNotNull(t_sexe.getText().toString()) || t_sexe.getText().toString().equals("valeur")) {
            Toast.makeText(getActivity(), "Sexe invalide!",Toast.LENGTH_LONG).show();
        }else if (t_email.getText().toString()!= "valeur" && !Patterns.EMAIL_ADDRESS.matcher(t_email.getText().toString()).matches()) {
            Toast.makeText(getActivity(),"Email client invalide!",Toast.LENGTH_LONG).show();
        }else{
            identiteProspectEntity.setLastname(t_lastname.getText().toString());
            identiteProspectEntity.setFirstname(t_firstname.getText().toString());
            identiteProspectEntity.setAge(Integer.parseInt(t_age.getText().toString()));
            identiteProspectEntity.setLangue(t_langue.getText().toString());
            identiteProspectEntity.setSexe(t_sexe.getText().toString());
            identiteProspectEntity.setNationality(t_nationality.getText().toString());

            if (t_email.getText().toString() != "valleur") {
                identiteProspectEntity.setEmail(t_email.getText().toString());
            }

            // passage des informations de l'identite du prospect au fragment coordonnees par un Bundle
            onButtonPressed(ConstructBundleProspect.constructBundleIdentity(identiteProspectEntity));

        }


    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }





    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnIdentityProspectInteractionListener {
        // TODO: Update argument type and name
        void identiteProspectInteraction(Bundle identite);
    }
}
