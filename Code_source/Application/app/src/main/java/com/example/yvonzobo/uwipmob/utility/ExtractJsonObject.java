package com.example.yvonzobo.uwipmob.utility;

import com.example.yvonzobo.uwipmob.models.entities.AgentEntity;
import com.example.yvonzobo.uwipmob.models.entities.ContratEntity;
import com.example.yvonzobo.uwipmob.models.entities.OperatorEntity;
import com.example.yvonzobo.uwipmob.models.entities.ProspectEntity;
import com.example.yvonzobo.uwipmob.models.entities.UserEntity;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yvon ZOBO on 27/07/2016.
 */
public class ExtractJsonObject {



    public static UserEntity extractUserJsonObject(JSONObject object){



        UserEntity userEntity = new UserEntity();


        try {


            userEntity.setIduser(object.getLong("iduuser"));
            userEntity.setIdprofile(object.getJSONObject("idprofile").getLong("idprofile"));
            userEntity.setIdorganisation(object.getJSONObject("idorganization").getLong("idorganization"));
            userEntity.setLastname(object.getString("lastname"));
            userEntity.setFirstname(object.getString("firstname"));
            userEntity.setDatenaiss(object.getString("birth"));
            userEntity.setLogin(object.getString("login"));
            userEntity.setPassword(object.getString("mdp"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userEntity;
    }




    public static AgentEntity extractAgentJsonObject(JSONObject object) {
        AgentEntity agentEntity = new AgentEntity();

        try {
             if (object.has("idagent")) {

                agentEntity.setIdagent(object.getLong("idagent"));
            }
            if (object.getJSONObject("iduuser").has("lastname")) {
                agentEntity.setNom(object.getJSONObject("iduuser").getString("lastname"));
            }
            if (object.getJSONObject("iduuser").has("firstname")) {
                agentEntity.setNom(object.getJSONObject("iduuser").getString("firstname"));
            }

            if (object.has("typeagent")) {
                agentEntity.setTypeagent(object.getString("typeagent"));
            }
            if (object.has("iduuser")) {
                agentEntity.setIduuser(object.getJSONObject("iduuser").getLong("iduuser"));
            }
            if (object.has("iduuser")) {
                agentEntity.setPrenom(object.getJSONObject("iduuser").getString("firstname"));
            }
            if (object.has("iduuser")) {
                agentEntity.setNom(object.getJSONObject("iduuser").getString("lastname"));
            }
            if (object.has("iduuser")) {
                agentEntity.setLogin(object.getJSONObject("iduuser").getString("login"));
            }
            if (object.has("iduuser")) {
                agentEntity.setMdp(object.getJSONObject("iduuser").getString("mdp"));
            }




            return agentEntity;

        }catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public  static List<ProspectEntity> extractProspectListJsonObject(JSONObject content){



        List<ProspectEntity> customerEntityList = new ArrayList<>();

        int taille;

        try {
                JSONArray array = content.getJSONArray("client");

            for ( taille=0;taille<array.length();taille++){
                ProspectEntity customerEntity = new ProspectEntity();

                JSONObject object = array.getJSONObject(taille);

                    if(object.has("idclient")){
                        customerEntity.setIdclient(object.getLong("idclient"));
                    }
                     if (object.has("idoperator")){
                    customerEntity.setIdoperator(object.getJSONObject("idoperator").getLong("idoperator"));
                     }
                    if (object.has("firstname"))
                        customerEntity.setFirstname(object.getString("firstname"));

                    if (object.has("lastname"))
                        customerEntity.setLastname(object.getString("lastname"));
                    if (object.has("clientuniqnumber"))
                        customerEntity.setClientuniqnumber(object.getInt("clientuniqnumber"));
                    if (object.has("nationality")){
                        customerEntity.setNationality(object.getString("nationality"));
                    }
                    if (object.has("totalbox"))
                        customerEntity.setTotalbox(object.getInt("totalbox"));

                    if (object.has("preferredphone"))
                        customerEntity.setPreferredphone(object.getString("preferredphone"));

                    if (object.has("phoneorange"))
                        customerEntity.setPhoneorange(object.getString("phoneorange"));

                    if (object.has("phonemtn"))
                        customerEntity.setPhonemtn(object.getString("phonemtn"));

                    if (object.has("phonenexttel"))
                        customerEntity.setPhonenexttel(object.getString("phonenexttel"));
                    if (object.has("cnipic"))
                        customerEntity.setCnipic(object.getString("cnipic"));
                    if (object.has("acquisitdate"))
                        customerEntity.setAcquisitdate(object.getString("acquisitdate"));

                    if (object.has("signedistinctif"))
                        customerEntity.setSignedistinctif(object.getString("signedistinctif"));
                    if (object.has("firstcontact"))
                        customerEntity.setFirstcontact(object.getString("firstcontact"));

                    if (object.has("mobilemoney"))
                         customerEntity.setMobilemoney(object.getString("mobilemoney"));
                    if (object.has("municipality"))
                        customerEntity.setMunicipality(object.getString("municipality"));
                    if (object.has("village"))
                        customerEntity.setVillage(object.getString("village"));


                customerEntityList.add(customerEntity);

                //  Toast.makeText(getActivity(),jsonArray.getJSONObject(taille).getString("lastname"),Toast.LENGTH_LONG).show();

            }
            return customerEntityList;


        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }


    }



    public static int getCountCustomer(JSONObject response) {
        Long id = null;
            try {
                if (response.has("tag")) {
                    id =  response.getLong("tag");
                }
                return id.intValue();

            } catch (JSONException e) {
                e.getMessage();
                e.getCause();
                return 0;
            }


    }

    public static List<OperatorEntity> extractOperatorListJsonEntity(JSONObject content){


        List<OperatorEntity> operatorEntityList = new ArrayList<>();

        try {
             JSONArray array = content.getJSONArray("mmoperator");
            for (int t=0;t<array.length();t++){
                OperatorEntity operatorEntity = new OperatorEntity();
                JSONObject object = array.getJSONObject(t);
                operatorEntity.setIdoperator(object.getLong("idoperator"));
                operatorEntity.setCode(object.getString("code"));

                operatorEntityList.add(operatorEntity);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return operatorEntityList;
    }

    public static  List<ContratEntity> extractContractJsonEntity(JSONArray array) {

        List<ContratEntity> list = new ArrayList<>();
        try {

        for (int t=0; t<array.length();t++){
            ContratEntity entity = new ContratEntity();

                JSONObject object = array.getJSONObject(t);
                if (object.has("idcontract"))
                    entity.setIdcontract(object.getLong("idcontract"));
                if (object.has("datecontra"))
                    entity.setDatecontra((object.getString("datecontra")));
                if (object.has("totalmoneypaid"))
                    entity.setTotalmoneypaid(object.getDouble("totalmoneypaid"));
                if (object.has("totalmoneydue"))
                    entity.setTotlmoneydue(object.getDouble("totalmoneydue"));
                if (object.has("moneybalance"))
                    entity.setMoneybalance(object.getDouble("moneybalance"));
                if (object.has("contractpic"))
                    entity.setContractpic(object.getString("contractpic"));
                if (object.has("homepic"))
                    entity.setHomepic(object.getString("homepic"));
                if (object.has("village"))
                    entity.setVillage(object.getString("village"));
                if (object.has("municipality"))
                    entity.setMunicipality(object.getString("municipality"));
                if (object.has("estimnexttransact"))
                    entity.setEstimnextrtansact((object.getString("estimnexttransact")));
                if (object.has("typeinstall"))
                    entity.setTypeinstall(object.getString("typeinstall"));
                if (object.has("natconnect"))
                    entity.setNatconnect(object.getString("natconnect"));
                if (object.has("installstatus"))
                    entity.setInstallstatus(object.getString("installstatus"));
                if (object.has("userofsystem"))
                    entity.setUserofsystem(object.getString("userofsystem"));
                if (object.has("installdate"))
                    entity.setInstalldate((object.getString("installdate")));
                if (object.has("desinstalldate"))
                    entity.setDesinstalldate((object.getString("desinstalldate")));
                if (object.has("phoneofusersystem"))
                    entity.setPhoneofusersystem(object.getInt("phoneofusersystem"));
                if (object.has("numusersystem"))
                    entity.setNumusersystem(object.getInt("numusersystem"));
                if (object.has("typecontrat"))
                    entity.setTypecontrat(object.getString("typecontrat"));
                if (object.has("idboxe"))
                    entity.setIdboxe(object.getJSONObject("idboxe").getLong("idboxe"));
                if (object.has("idclient"))
                    entity.setIdclient(object.getJSONObject("idclient").getLong("idclient"));


                list.add(entity);
        }
            return list;
        } catch (JSONException e) {
            e.getMessage();
            return null;
        }
    }




}
