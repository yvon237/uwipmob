package com.example.yvonzobo.uwipmob.models.entities.prospects;

/**
 * Created by Yvon ZOBO on 30/09/2016.
 */
public class CoordonnesProspectEntity {
    private String nationality;
    private Long idoperator;
    private Long idregion;
    private Long iddepartement;
    private Long idarrondissement;
    private Long idvillage;
    private String preferredphone;
    private String phoneorange;
    private String phonemtn;
    private String phonenexttel;
    private String statutindividu; // auto (prospect, client, en attente installation ...)
    private String sourcing; // source d'information comment avez vous connu upowa
    private String mobilemoney; // champ de la bd mmdispo disponibilité mobile money
    private String acquisitdate; // date premier contact avec le prospect (enregistrement automatique)
    private int clientuniqnumber; // numéro unique du client (random)
    private String typezone; // urbain, péri-urbain, rural



    public CoordonnesProspectEntity() {

    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Long getIdoperator() {
        return idoperator;
    }

    public void setIdoperator(Long idoperator) {
        this.idoperator = idoperator;
    }

    public String getPreferredphone() {
        return preferredphone;
    }

    public void setPreferredphone(String preferredphone) {
        this.preferredphone = preferredphone;
    }

    public String getPhoneorange() {
        return phoneorange;
    }

    public void setPhoneorange(String phoneorange) {
        this.phoneorange = phoneorange;
    }

    public String getPhonemtn() {
        return phonemtn;
    }

    public void setPhonemtn(String phonemtn) {
        this.phonemtn = phonemtn;
    }

    public String getPhonenexttel() {
        return phonenexttel;
    }

    public void setPhonenexttel(String phonenexttel) {
        this.phonenexttel = phonenexttel;
    }

    public String getSourcing() {
        return sourcing;
    }

    public void setSourcing(String sourcing) {
        this.sourcing = sourcing;
    }

    public String getMobilemoney() {
        return mobilemoney;
    }

    public void setMobilemoney(String mobilemoney) {
        this.mobilemoney = mobilemoney;
    }

    public String getTypezone() {
        return typezone;
    }

    public void setTypezone(String typezone) {
        this.typezone = typezone;
    }

    public Long getIdregion() {
        return idregion;
    }

    public void setIdregion(Long idregion) {
        this.idregion = idregion;
    }

    public Long getIddepartement() {
        return iddepartement;
    }

    public void setIddepartement(Long iddepartement) {
        this.iddepartement = iddepartement;
    }

    public Long getIdvillage() {
        return idvillage;
    }

    public void setIdvillage(Long idvillage) {
        this.idvillage = idvillage;
    }

    public Long getIdarrondissement() {
        return idarrondissement;
    }

    public void setIdarrondissement(Long idarrondissement) {
        this.idarrondissement = idarrondissement;
    }

    public String getStatutindividu() {
        return statutindividu;
    }

    public void setStatutindividu(String statutindividu) {
        this.statutindividu = statutindividu;
    }

    public int getClientuniqnumber() {
        return clientuniqnumber;
    }

    public void setClientuniqnumber(int clientuniqnumber) {
        this.clientuniqnumber = clientuniqnumber;
    }

    public String getAcquisitdate() {
        return acquisitdate;
    }

    public void setAcquisitdate(String acquisitdate) {
        this.acquisitdate = acquisitdate;
    }
}
