package com.example.yvonzobo.uwipmob.models.entities;

/**
 * Created by Yvon ZOBO on 11/10/2016.
 */
public class Region {

    private Long idregion;
    private String libelle_region;


    public Region() {

    }

    public void setIdregion(Long idregion) {
        this.idregion = idregion;
    }

    public Long getIdregion() {
        return idregion;

    }

    public void setLibelle_region(String libelle_region) {
        this.libelle_region = libelle_region;
    }

    public String getLibelle_region() {
        return  libelle_region;
    }
}
