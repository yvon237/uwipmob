package com.example.yvonzobo.uwipmob.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.yvonzobo.uwipmob.models.database.ContentDb;
import com.example.yvonzobo.uwipmob.models.database.DbHelper;
import com.example.yvonzobo.uwipmob.models.entities.AgentEntity;

/**
 * Created by Yvon ZOBO on 09/10/2016.
 */
public class AgentController {


    public static final String CREATE_TABLE_AGENT = "CREATE TABLE "+
            ContentDb.TABLE_AGENT+" ("+
            ContentDb.COLUMM_ID_AGENT+" INTEGER PRIMARY KEY, "+
            ContentDb.COLUMM_ID_USER+" INTEGER,"+
            ContentDb.COLUMM_NOM+" TEXT, "+
            ContentDb.COLUMM_PRENOM+" TEXT, "+
            ContentDb.COLUMM_typeagent+" TEXT, "+
            ContentDb.COLUMM_LOGIN+" TEXT, "+
            ContentDb.COLUMM_PASSWORD+" TEXT);";


    private DbHelper db;
    private SQLiteDatabase mydb;

    public AgentController(Context context){
        db = new DbHelper(context);
    }
    /**
     * ouvre la base de données en écriture
     * @return
     */
    public SQLiteDatabase open(){

        mydb = db.getWritableDatabase();
        return mydb;
    }

    /**
     * ferme la base de données
     */
    public void close(){
        mydb.close();
    }

    public SQLiteDatabase getMydb(){
        return mydb;
    }

    public String getTypeAgent(String login, String password) {
        Cursor cursor=null;
        try {
            cursor = open().rawQuery("SELECT typeagent FROM agent WHERE login =? AND mdp = ? ", new String[]{login,password});

        }catch (Exception sqle){
            sqle.printStackTrace();

        }
        cursor.moveToFirst();

        String typeagent = cursor.getString(0);
        cursor.close();


        return typeagent;




    }
    public boolean checkAgentBylogin_Password(String login,String mdp){
        Cursor agent = null;
        try {
            agent = open().rawQuery("SELECT * FROM agent WHERE login =? AND mdp = ? ",new String[]{login,mdp});


        }catch (Exception sqle){
            sqle.printStackTrace();
        }
        if ( agent.getCount()==0)
            return false;
        else return true;


    }

    public boolean getLoginAgentFromdb(){
        Cursor cursor = null;
        try {

            cursor = open().rawQuery("select login from agent",null);

        }catch (Exception e){
            e.printStackTrace();
        }
        if(cursor.getCount()==0){
            return false;
        }else
            return true;

    }
    public boolean deleteAgent(Long id){

        int result= open().delete(ContentDb.TABLE_AGENT, ContentDb.COLUMM_ID_USER+ "= " +
                ""+id,null);

        if (result!=0){
            return true;
        }else
            return false;


    }
    public Long getIdUuser(){
        Cursor cursor;
        cursor = open().rawQuery("select iduuser from agent",null);
        Long id = cursor.getLong(1);
        return id;
    }



    /**
     * insert un utilisateur dans la table user
     * @param agentEntity
     * @return
     */
    public boolean insertAgent(AgentEntity agentEntity){
        ContentValues values = new ContentValues();
        values.put(ContentDb.COLUMM_ID_USER,agentEntity.getIdagent());
        values.put(ContentDb.COLUMM_ID_USER,agentEntity.getIduuser());
        values.put(ContentDb.COLUMM_NOM,agentEntity.getNom());
        values.put(ContentDb.COLUMM_PRENOM,agentEntity.getPrenom());
        values.put(ContentDb.COLUMM_typeagent,agentEntity.getTypeagent());
        values.put(ContentDb.COLUMM_LOGIN,agentEntity.getLogin());
        values.put(ContentDb.COLUMM_PASSWORD,agentEntity.getMdp());


        long result = open().insert(ContentDb.TABLE_AGENT,null,values);
        if (result==-1){
            return false;
        }else
            return true;
    }

    public boolean updatePassword(String login, String password,String ancienpassword) {
        ContentValues values = new ContentValues();


        values.put(ContentDb.COLUMM_LOGIN,login);
        values.put(ContentDb.COLUMM_PASSWORD,password);

    return   open().update(ContentDb.TABLE_AGENT,values,ContentDb.COLUMM_PASSWORD,new String[]{ancienpassword})>0;
    }


    public Long getIdAgent() {
        Cursor cursor = open().rawQuery("SELECT idagent from agent",null);
        cursor.moveToFirst();
       Long idagent= cursor.getLong(0);
        cursor.close();
        return idagent;

    }



}
