package com.example.yvonzobo.uwipmob.views.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.yvonzobo.uwipmob.R;
import com.example.yvonzobo.uwipmob.adapters.PostProspectAdapter;
import com.example.yvonzobo.uwipmob.controllers.ProspectController;
import com.example.yvonzobo.uwipmob.models.entities.PostProspectList;

import java.util.ArrayList;
import java.util.List;


public class ListeProspectFragment extends Fragment {

    private ListeProspectInteractionListener mListener;
    private ListView mListView;
    private ProspectController mProspectController;
    private PostProspectAdapter mPostProspectAdapter;
    private List<PostProspectList> mPostProspectList;

    public ListeProspectFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ListeProspectFragment newInstance() {
        ListeProspectFragment fragment = new ListeProspectFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_liste_prospect, container, false);

        setHasOptionsMenu(true);


        mProspectController = new ProspectController(getActivity());
        mListView = (ListView)view.findViewById(R.id.listview);


        mPostProspectList = new ArrayList<>();
        mPostProspectList = mProspectController.getPostCustomer();

        mPostProspectAdapter = new PostProspectAdapter(mPostProspectList,getActivity());


        mListView.setAdapter(mPostProspectAdapter);

        mListView.setOnItemClickListener(new ItemListener());


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ListeProspectInteractionListener) {
            mListener = (ListeProspectInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public class ItemListener implements AdapterView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            if (mListener!=null)
                 mListener.passProspectToView(mPostProspectList.get(i).getNom(),mPostProspectList.get(i).getPrenom());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface ListeProspectInteractionListener {

        void onFragmentInteraction(Uri uri);
        void passProspectToView(String nom,String prenom);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {

        menuInflater.inflate(R.menu.options_search,menu);

     SearchView searchView = (SearchView)menu.findItem(R.id.search).getActionView();

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mPostProspectAdapter.filter(newText.toString().trim());
                mListView.invalidate();


                return true;
            }
        });


    }
}
