package com.example.yvonzobo.uwipmob.views.fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yvonzobo.uwipmob.R;
import com.example.yvonzobo.uwipmob.controllers.OperatorController;
import com.example.yvonzobo.uwipmob.controllers.ProspectController;
import com.example.yvonzobo.uwipmob.models.entities.OperatorEntity;
import com.example.yvonzobo.uwipmob.models.entities.PostProspectList;
import com.example.yvonzobo.uwipmob.models.entities.ProspectEntity;
import com.example.yvonzobo.uwipmob.utility.CheckNetworkConnection;
import com.example.yvonzobo.uwipmob.utility.ExtractJsonObject;
import com.example.yvonzobo.uwipmob.utility.Urls;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class DetailsProspectFragment extends Fragment implements  AdapterView.OnItemSelectedListener{

    private DetailsProspectInteractionListener mDetailListener;

    private List<ProspectEntity> customerEntities;


    // textview pour les valeurs des differents champs de types text des dialog
    private TextView dt_lastname,dt_firstname,dt_nationality,dt_profession,
            dt_firstcontact,dt_mobilemoney,dt_municipality,dt_village,dt_phoneprefer,dt_phoneorange,
            dt_phonemtn, dt_phonenexttel,dt_operator;
    // les textview des differents libelle
    private TextView dl_lastname,dl_firstname,dl_nationality,dl_phonenumber,dl_profession,
            dl_firstcontact,dl_mobilemoney,dl_localite,dl_operator;
    // les layout de chaque bloc on affecte l'evenement onclicklistener
    private LinearLayout layoutlastname,layoutfirstname,layoutnationality,layoutphonenumber,
            layoutprofession,layoutfirstcontact,layoutmobilemoney,layoutlocalite,layoutoperator;
    //la boite de dialog
    private  AlertDialog.Builder builder;
    // les editText se trouvant dans les differents boites de dialog
    private EditText editText,editText2,editText3,editText4;
    //textInputLayout utilisé pour le design
    private TextInputLayout textInputLayout,textInputLayout1,textInputLayout2,textInputLayout3;
    //button enregister au serveur et en local et button choix de l'image dans la gallery
    private Button register;
    // spinner pour les selections des pays
    private Spinner spinner;
    private String[] country ={"Cameroun","Congo","Gabon","Guinnée Equatoriale","Mali"};
    // utiliser pour connaitre le numero preferer du client
    private RadioGroup radioGroup;
    private RadioButton radioyes,radiono,radioOrange,radioMtn,radioNexttel;
    // le bean client
    private ProspectEntity customerEntity = new ProspectEntity();
    private OperatorEntity operatorEntity = new OperatorEntity();
    // pour l'image de la cni
    private ImageView cnipic,cniview;
    private Button b_modif;
    private ProspectController customerController;
    private ProgressDialog dialog;
    private OperatorController operatorController;
    private List<PostProspectList>customerEntityForLists = new ArrayList<>();


    public DetailsProspectFragment() {
        // Required empty public constructor
    }

    public static DetailsProspectFragment newInstance(String nom, String prenom) {
        DetailsProspectFragment fragment = new DetailsProspectFragment();
        Bundle args = new Bundle();
        args.putCharSequence("firstname",prenom);
        args.putCharSequence("lastname",nom);

        fragment.setArguments(args);

        return fragment;
    }
    public String[] getPropect() {
        String[] name;

        name = new String[]{getArguments().getCharSequence("firstname").toString(), getArguments().getCharSequence("lastname").toString()};
        return name;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for getActivity() fragment
        View view= inflater.inflate(R.layout.fragment_details_prospect, container, false);


        customerController = new ProspectController(getActivity());
        operatorController = new OperatorController(getActivity());




        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Extraction en cours...");
        dialog.setCancelable(false);


        dt_lastname = (TextView)view.findViewById(R.id.dt_lastname);
        dt_firstname = (TextView)view.findViewById(R.id.dt_firstname);
        dt_nationality = (TextView)view.findViewById(R.id.dt_nationality);


        dt_profession = (TextView)view.findViewById(R.id.dt_profession);
        dt_firstcontact = (TextView)view.findViewById(R.id.dt_firstcontact);
        dt_mobilemoney = (TextView)view.findViewById(R.id.dt_mobilemoney);

        dt_phoneprefer  = (TextView)view.findViewById(R.id.dt_phoneprefer);
        dt_phoneorange = (TextView)view.findViewById(R.id.dt_phoneorange);
        dt_phonemtn = (TextView)view.findViewById(R.id.dt_phonemtn);
        dt_phonenexttel = (TextView)view.findViewById(R.id.dt_phonenexttel);
        dt_municipality = (TextView)view.findViewById(R.id.dt_municipality);
        dt_village = (TextView)view.findViewById(R.id.dt_village);
        dt_operator = (TextView)view.findViewById(R.id.dt_operator);



        dl_lastname = (TextView)view.findViewById(R.id.dl_lastname);
        dl_firstname = (TextView)view.findViewById(R.id.dl_firstname);
        dl_nationality = (TextView)view.findViewById(R.id.dl_nationality);
        dl_phonenumber = (TextView)view.findViewById(R.id.dl_phonenumber);

        dl_profession = (TextView)view.findViewById(R.id.dl_profession);
        dl_firstcontact = (TextView)view.findViewById(R.id.dl_firstcontact);
        dl_mobilemoney = (TextView)view.findViewById(R.id.dl_mobilemoney);
        dl_localite = (TextView)view.findViewById(R.id.dl_localite);

        dl_operator = (TextView)view.findViewById(R.id.dl_operator);






        layoutlastname = (LinearLayout)view.findViewById(R.id.dlayoutnom);
        layoutfirstname = (LinearLayout)view.findViewById(R.id.dlayoutprenom);
        layoutnationality = (LinearLayout)view.findViewById(R.id.dlayoutnationality);
        layoutphonenumber = (LinearLayout)view.findViewById(R.id.dlayoutphonenumberr);

        layoutprofession = (LinearLayout)view.findViewById(R.id.dlayoutprofession);
        layoutfirstcontact = (LinearLayout)view.findViewById(R.id.dlayoutfirstcontact);
        layoutmobilemoney = (LinearLayout)view.findViewById(R.id.dlayoutmobilemoney);
        layoutlocalite = (LinearLayout)view.findViewById(R.id.dlayoutlocalite);

        layoutoperator = (LinearLayout)view.findViewById(R.id.dlayoutoperator);

        b_modif = (Button)view.findViewById(R.id.modifier);


        String[] prospect = getPropect();
        /// getSingleCustomer();
        chargeViewDetailInLocal(prospect[1],prospect[0]);

        onClickLayoutLastname();
        onClickdlayoutfirstname();
        onClickLayoutNationality();
        onClickLayoutPhoneNumber();
        onClickLayoutProfession();
        onClickLayoutFirstContact();
        onClickLayoutMobileMoney();
        onClickLayoutOperatorMobile();
        onClickLayoutLocalite();

        onclickB_Modifier();



        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String nom,String prenom) {
        if (mDetailListener != null) {
            mDetailListener.PropectToViewInteraction(nom,prenom);
        }
    }

    private void onclickB_Modifier(){
        b_modif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] prospect = getPropect();
                //confirmation modification client sur l'alert dialog

                if (compareTwoCustomer(getCustomerForUpdate(prospect[1],prospect[0]),customerController.getCustomerLastnameFirstname(prospect[1],prospect[0]))){
                    onCreateDialogAlertNoModification();
                }else {
                    onCreateDialogConfirmModification();
                }


            }
        });
    }


    private  void onClickLayoutLastname(){
        layoutlastname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogLastname(dl_lastname);
            }
        });
    }

    private  void onClickdlayoutfirstname(){
        layoutfirstname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogFirstname(dl_firstname);
            }
        });
    }
    private  void onClickLayoutNationality(){
        layoutnationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogNationality(dl_nationality);
            }
        });
    }
    private  void onClickLayoutPhoneNumber(){
        layoutphonenumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogPhoneNumebers(dl_phonenumber);
            }
        });

    }
    private void onClickLayoutOperatorMobile(){
        layoutoperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogOperateurMobile(dl_operator);
            }
        });
    }
    private  void onClickLayoutProfession(){
        layoutprofession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogProfession(dl_profession);
            }
        });
    }
    private  void onClickLayoutFirstContact(){
        layoutfirstcontact.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onCreateDialogFirstContact(dl_firstcontact);





            }
        });
    }
    private  void onClickLayoutMobileMoney(){
        layoutmobilemoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogMobileMoney(dl_mobilemoney);
            }
        });
    }
    private  void onClickLayoutLocalite(){
        layoutlocalite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogLocalite(dl_localite);
            }
        });
    }


    private Dialog onCreateDialogLastname(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setText(dt_lastname.getText());

        builder.setView(editText);

        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonLastnameClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeLastnameButtonClickListener());

        return builder.show();
    }


    private class OnPositiveButtonLastnameClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            String valeur = editText.getText().toString();
            dt_lastname.setText(valeur);
            customerEntity.setLastname(valeur);


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeLastnameButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }







    private Dialog onCreateDialogFirstname(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setText(dt_firstname.getText());

        builder.setView(editText);

        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonFirstnameClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeFirstnameButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonFirstnameClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            final String valeur = editText.getText().toString();
            dt_firstname.setText(valeur);
            customerEntity.setFirstname(valeur);


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeFirstnameButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        dt_nationality.setText(country[i]);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }




    private Dialog onCreateDialogNationality(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        spinner = new Spinner(getActivity());
        spinner.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,country);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterType);



        builder.setView(spinner);

        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonNationalityClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeNationalityButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonNationalityClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeNationalityButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    private Dialog onCreateDialogOperateurMobile(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        LinearLayout layoutRadioGroup = new LinearLayout(getActivity());
        layoutRadioGroup.setOrientation(LinearLayout.HORIZONTAL);

        radioGroup = new RadioGroup(getActivity());
        radioOrange = new RadioButton(getActivity());
        radioOrange.setId(RadioButton.generateViewId());
        radioOrange.setText("Orange");
        radioMtn = new RadioButton(getActivity());
        radioMtn.setId(RadioButton.generateViewId());
        radioMtn.setText("Mtn");

        radioNexttel = new RadioButton(getActivity());
        radioNexttel.setId(RadioButton.generateViewId());
        radioNexttel.setText("Nexttel");



        radioGroup.addView(radioOrange);
        radioGroup.addView(radioMtn);
        radioGroup.addView(radioNexttel);
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        layoutRadioGroup.addView(radioGroup);


        builder.setView(layoutRadioGroup);

        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonOperatorMobileClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeOperatorButtonMobileClickListener());

        return builder.show();
    }

    private class OnPositiveButtonOperatorMobileClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            if (radioGroup.getCheckedRadioButtonId() == radioOrange.getId()){
                dt_operator.setText("ORANGE-CM");
            } else if (radioGroup.getCheckedRadioButtonId() == radioMtn.getId()) {
                dt_operator.setText("MTN-CM");
            } else if (radioGroup.getCheckedRadioButtonId() == radioNexttel.getId()) {
                dt_operator.setText("NEXTTEL-CM");
            }



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeOperatorButtonMobileClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }


    private Dialog onCreateDialogPhoneNumebers(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        textInputLayout = new TextInputLayout(getActivity());
        textInputLayout1 = new TextInputLayout(getActivity());
        textInputLayout2 = new TextInputLayout(getActivity());
        textInputLayout3 = new TextInputLayout(getActivity());
        LinearLayout  layout = new LinearLayout(getActivity());
        LinearLayout layoutRadioGroup = new LinearLayout(getActivity());
        ScrollView scrollView = new ScrollView(getActivity());

        TextView favorite = new TextView(getActivity());
        favorite.setText("Numéro preféré");
        favorite.setGravity(Gravity.CENTER);
        favorite.setTextSize(20);




        layoutRadioGroup.setOrientation(LinearLayout.HORIZONTAL);
        layout.setOrientation(LinearLayout.VERTICAL);

        radioGroup = new RadioGroup(getActivity());
        radioOrange = new RadioButton(getActivity());
        radioOrange.setId(RadioButton.generateViewId());
        radioOrange.setText("Orange");
        radioMtn = new RadioButton(getActivity());
        radioMtn.setId(RadioButton.generateViewId());
        radioMtn.setText("Mtn");

        radioNexttel = new RadioButton(getActivity());
        radioNexttel.setId(RadioButton.generateViewId());
        radioNexttel.setText("Nexttel");



        radioGroup.addView(radioOrange);
        radioGroup.addView(radioMtn);
        radioGroup.addView(radioNexttel);
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        layoutRadioGroup.addView(radioGroup);



        editText2 = new EditText(getActivity());
        editText2.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText2.setHint("Numéro Orange");
        editText2.setText(dt_phoneorange.getText().toString());

        editText3 = new EditText(getActivity());
        editText3.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText3.setHint("Numéro Mtn");
        editText3.setText(dt_phonemtn.getText().toString());


        editText4 = new EditText(getActivity());
        editText4.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText4.setHint("Numéro Nexttel");
        editText4.setText(dt_phonenexttel.getText().toString());



        textInputLayout1.addView(editText2);
        textInputLayout2.addView(editText3);
        textInputLayout3.addView(editText4);



        layout.addView(textInputLayout1);
        layout.addView(textInputLayout2);
        layout.addView(textInputLayout3);
        layout.addView(favorite);
        layout.addView(layoutRadioGroup);


        scrollView.addView(layout);

        builder.setView(scrollView);

        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonPhoneNumberClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativePhoneNumberButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonPhoneNumberClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            String orange = editText2.getText().toString();
            String mtn = editText3.getText().toString();
            String nexttel = editText4.getText().toString();
            if (radioGroup.getCheckedRadioButtonId() == radioOrange.getId()){
                customerEntity.setPhoneorange(orange);
                customerEntity.setPreferredphone(orange);
                if (mtn.equals("")){
                    customerEntity.setPhonemtn("non");
                }else
                    customerEntity.setPhonemtn(mtn);
                if (nexttel.equals(""))
                    customerEntity.setPhonenexttel("non");
                else
                    customerEntity.setPhonenexttel(nexttel);
                dt_phoneprefer.setText(customerEntity.getPreferredphone());
                dt_phoneorange.setText(customerEntity.getPhoneorange());
                dt_phonemtn.setText(customerEntity.getPhonemtn());
                dt_phonenexttel.setText(customerEntity.getPhonenexttel());


            }else if (radioGroup.getCheckedRadioButtonId()== radioMtn.getId()){
                customerEntity.setPreferredphone(mtn);
                customerEntity.setPhonemtn(mtn);

                if (orange.equals("")){
                    customerEntity.setPhoneorange("non");
                }else
                    customerEntity.setPhoneorange(orange);
                if (nexttel.equals(""))
                    customerEntity.setPhonenexttel("non");
                else
                    customerEntity.setPhonenexttel(nexttel);

                dt_phoneprefer.setText(customerEntity.getPreferredphone());
                dt_phoneorange.setText(customerEntity.getPhoneorange());
                dt_phonemtn.setText(customerEntity.getPhonemtn());
                dt_phonenexttel.setText(customerEntity.getPhonenexttel());



            }else if (radioGroup.getCheckedRadioButtonId()== radioNexttel.getId()){
                customerEntity.setPreferredphone(nexttel);
                customerEntity.setPhonenexttel(nexttel);

                if (orange.equals("")){
                    customerEntity.setPhoneorange("non");
                }else
                    customerEntity.setPhoneorange(orange);
                if (mtn.equals(""))
                    customerEntity.setPhonemtn("non");
                else
                    customerEntity.setPhonemtn(nexttel);

                dt_phoneprefer.setText(customerEntity.getPreferredphone());
                dt_phoneorange.setText(customerEntity.getPhoneorange());
                dt_phonemtn.setText(customerEntity.getPhonemtn());
                dt_phonenexttel.setText(customerEntity.getPhonenexttel());




            }






            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativePhoneNumberButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    private Dialog onCreateDialogProfession(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setText(dt_profession.getText().toString());

        builder.setView(editText);

        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonProfessionClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeProfessionButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonProfessionClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            String valeur = editText.getText().toString();
            dt_profession.setText(valeur);
            customerEntity.setSignedistinctif(valeur);


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeProfessionButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    private Dialog onCreateDialogFirstContact(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setHint("source info");
        editText.setText(dt_firstcontact.getText().toString());



        builder.setView(editText);




        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonFirstContactClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeFirstContactButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonFirstContactClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            String valeur = editText.getText().toString();
            dt_firstcontact.setText(valeur);
            customerEntity.setFirstcontact(valeur);


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeFirstContactButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }





    private Dialog onCreateDialogMobileMoney(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());

        radioGroup = new RadioGroup(getActivity());
        radioGroup.setId(RadioGroup.generateViewId());
        radioyes = new RadioButton(getActivity());
        radioyes.setId(RadioButton.generateViewId());
        radioyes.setText("Oui");
        radiono = new RadioButton(getActivity());
        radiono.setText("Non");
        radiono.setId(RadioButton.generateViewId());
        LinearLayout layout = new LinearLayout(getActivity());

        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        radioGroup.addView(radioyes);
        radioGroup.addView(radiono);
        layout.addView(radioGroup);
        layout.setOrientation(LinearLayout.HORIZONTAL);

        builder.setView(layout);



        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonMobilemoneyClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeMobileMoneyButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonMobilemoneyClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            if (radioGroup.getCheckedRadioButtonId()== radioyes.getId())
                dt_mobilemoney.setText("oui");
            else if (radioGroup.getCheckedRadioButtonId()== radiono.getId())
                dt_mobilemoney.setText("Non");


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeMobileMoneyButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }







    private Dialog onCreateDialogLocalite(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText2 = new EditText(getActivity());
        editText2.setInputType(InputType.TYPE_CLASS_TEXT);
        editText2.setHint("Village");
        editText2.setText(dt_village.getText().toString());
        textInputLayout = new TextInputLayout(getActivity());
        textInputLayout1 = new TextInputLayout(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setHint("Municipalité");
        editText.setText(dt_municipality.getText().toString());
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        textInputLayout.addView(editText);
        textInputLayout1.addView(editText2);
        layout.addView(textInputLayout);
        layout.addView(textInputLayout1);

        builder.setView(layout);




        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonLocaliteClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeLocaliteClickListener());

        return builder.show();
    }
    private class OnPositiveButtonLocaliteClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            String municipality = editText.getText().toString();
            String village = editText2.getText().toString();

            dt_village.setText(village);
            dt_municipality.setText(municipality);



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeLocaliteClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }





/*

    */
/**
     * charge lié au client selectionner dans la liste des clients
     *//*

    public void getSingleCustomer() {

        if (CheckNetworkConnection.isOnline(getActivity())) {
            AsyncHttpClient client = new AsyncHttpClient();


            client.get(Urls.getallcustomersurl, new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject content) {
                    super.onSuccess(statusCode, headers, content);


                    chargeViewDetailInServer(content);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);

                    if (statusCode == 404) {

                        Toast.makeText(getApplicationContext(), "Ressource non trouvée dans le serveur", Toast.LENGTH_LONG)
                                .show();
                        dialog.dismiss();

                    } else if (statusCode == 500) {

                        Toast.makeText(getApplicationContext(), "erreur 500, probleme avec la requete au serveur...", Toast.LENGTH_LONG).show();

                        dialog.dismiss();
                    } else {

                        Toast.makeText(getApplicationContext(), "le serveur n'est pas disponible pour", Toast.LENGTH_LONG).show();

                        dialog.dismiss();

                    }
                }


            });
        } else {

            // chargeViewDetailInLocal(getIntent().getStringExtra("position"));

        }


    }
*/



    /**
     * chargement de la vue en locale
     * plus besoin de faire une requete au serveur car
     * les donnees sont charges en locale
     * a partir du menu gestclient
     * @param lastname
     */
    public void  chargeViewDetailInLocal(String lastname,String firstname){
        customerEntity = customerController.getCustomerLastnameFirstname(lastname,firstname);
        String codeOperator = operatorController.getCodeOperatorById(customerEntity.getIdoperator());
        dt_lastname.setText(customerEntity.getLastname());
        dt_firstname.setText(customerEntity.getFirstname());
        dt_profession.setText(customerEntity.getSignedistinctif());
        dt_nationality.setText(customerEntity.getNationality());
        dt_firstcontact.setText(customerEntity.getFirstcontact());
        dt_phoneprefer.setText(customerEntity.getPreferredphone());
        dt_phoneorange.setText(customerEntity.getPhoneorange());
        dt_phonemtn.setText(customerEntity.getPhonemtn());
        dt_phonenexttel.setText(customerEntity.getPhonenexttel());
        dt_operator.setText(codeOperator);
        dt_mobilemoney.setText(customerEntity.getMobilemoney());
        dt_municipality.setText(customerEntity.getMunicipality());
        dt_village.setText( customerEntity.getVillage());
        dialog.dismiss();
        Toast.makeText(getActivity(), "extraction terminée!", Toast.LENGTH_LONG).show();

    }

    /**
     * permet de faire la mise a jour sur un client a la base de donnees
     * @param customer
     */
    public void upDateCustomerLocally(ProspectEntity customer){
        String[] name=getPropect();
        Long idclientLocal = Long.valueOf(customerController.getIdClientLocal(name[1],name[0]));

        if (customerController.upDateCustomer(customer,idclientLocal))
            Toast.makeText(getActivity(),"modification terminée avec succès ",Toast.LENGTH_LONG).show();
    }

    /**
     * permet de reccuperer le client modifier
     * @param lastname
     * @param firstname
     * @return
     */
    public ProspectEntity getCustomerForUpdate(String lastname,String firstname){



        // customerEntity.setIdclient(Long.valueOf(customerController.getIdClientLocal(lastname,firstname)));
        customerEntity.setIdoperator(Long.valueOf(operatorController.getIdoperator(dt_operator.getText().toString())));
        customerEntity.setLastname(dt_lastname.getText().toString());
        customerEntity.setFirstname(dt_firstname.getText().toString());
        customerEntity.setNationality(dt_nationality.getText().toString());
        customerEntity.setPreferredphone(dt_phoneprefer.getText().toString());
        customerEntity.setPhonemtn(dt_phonemtn.getText().toString());
        customerEntity.setPhoneorange(dt_phoneorange.getText().toString());
        customerEntity.setPhonenexttel(dt_phonenexttel.getText().toString());
        customerEntity.setMobilemoney(dt_mobilemoney.getText().toString());
        customerEntity.setMunicipality(dt_municipality.getText().toString());
        customerEntity.setVillage(dt_village.getText().toString());
        return customerEntity;
    }


    /**
     * compare deux clients
     * utiliser pour savoir si un client a été modifié ou pas!!
     * @param entity
     * @param entity1
     * @return
     */
    public boolean compareTwoCustomer(ProspectEntity entity,ProspectEntity entity1){
        if(entity.equals(entity1)){
            return true;
        }else return false;
    }

    /**
     * boite de dialogue permettant de confirmer la modification sur un client
     * @return
     */
    private Dialog onCreateDialogConfirmModification(){
        builder= new AlertDialog.Builder(getActivity());




        builder.setTitle("Modification");
        builder.setIcon(getResources().getDrawable(R.drawable.alert));
        builder.setMessage("Confirmez-vous ces modifications?");


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonConfirmationClickListener
                ());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeConfirmationButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonConfirmationClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            String[] prospect= getPropect();
            //application de la modification
            upDateCustomerLocally(getCustomerForUpdate(prospect[1],prospect[0]));



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeConfirmationButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }

    /**
     * boite de dialog affichant le message si aucune modification n'a ete apporter sur un client
     * @return
     */
    private Dialog onCreateDialogAlertNoModification(){
        builder= new AlertDialog.Builder(getActivity());
        builder.setTitle("Modification");
        builder.setIcon(getResources().getDrawable(R.drawable.alert));
        builder.setMessage("Aucune modification n'a été effectuée!!");
        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonConfirmClickListener());
        return builder.show();
    }
    private class OnPositiveButtonConfirmClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DetailsProspectInteractionListener) {
            mDetailListener = (DetailsProspectInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DetailsProspectInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mDetailListener = null;
    }

    /**
     * getActivity() interface must be implemented by activities that contain getActivity()
     * fragment to allow an interaction in getActivity() fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface DetailsProspectInteractionListener {
        // TODO: Update argument type and name
        void PropectToViewInteraction(String nom,String prenom);
    }
}
