package com.example.yvonzobo.uwipmob.utility;

import com.example.yvonzobo.uwipmob.models.entities.prospects.IdentiteProspectEntity;
import com.example.yvonzobo.uwipmob.models.entities.prospects.ProspectEntity;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by yvon on 17/10/16.
 */
public class ConstructJsonObject {

    public static String constructProspectJsonObject(ProspectEntity prospectEntity) {
        JSONObject object = new JSONObject();

        try {
            object.put("idclient",prospectEntity.getIdentiteProspectEntity().getIdClient());
            object.put("firstname",prospectEntity.getIdentiteProspectEntity().getFirstname());
            object.put("lastname",prospectEntity.getIdentiteProspectEntity().getLastname());
            object.put("age",prospectEntity.getIdentiteProspectEntity().getAge());
            object.put("sexe",prospectEntity.getIdentiteProspectEntity().getSexe());
            object.put("langue",prospectEntity.getIdentiteProspectEntity().getLangue());
            object.put("email",prospectEntity.getIdentiteProspectEntity().getEmail());
            object.put("nationality",prospectEntity.getIdentiteProspectEntity().getNationality());

            object.put("idvillage",prospectEntity.getCoordonnesProspectEntity().getIdvillage());
            object.put("preferredphone",prospectEntity.getCoordonnesProspectEntity().getPreferredphone());
            object.put("phoneorange",prospectEntity.getCoordonnesProspectEntity().getPhoneorange());
            object.put("phonemtn",prospectEntity.getCoordonnesProspectEntity().getPhonemtn());
            object.put("phonenexttel",prospectEntity.getCoordonnesProspectEntity().getPhonenexttel());
            object.put("stautindividu",prospectEntity.getCoordonnesProspectEntity().getStatutindividu());
            object.put("sourcing",prospectEntity.getCoordonnesProspectEntity().getSourcing());
            object.put("mobilemoney",prospectEntity.getCoordonnesProspectEntity().getMobilemoney());
            object.put("acquisitdate",prospectEntity.getCoordonnesProspectEntity().getAcquisitdate());
            object.put("clientuniqnumber",prospectEntity.getCoordonnesProspectEntity().getClientuniqnumber());
            object.put("typezone",prospectEntity.getCoordonnesProspectEntity().getTypezone());


            object.put("rsesatisfaiteneo",prospectEntity.getRessourcesProspectEntity().getRse_satisfaiteneo());
            object.put("rsefacturemensuelleeneo",prospectEntity.getRessourcesProspectEntity().getRse_facturemensuelleeneo());
            object.put("rsegenerator",prospectEntity.getRessourcesProspectEntity().getRse_generator());
            object.put("rseqtitehebdogasoil",prospectEntity.getRessourcesProspectEntity().getRse_qtitehebdogasoil());
            object.put("rseprixunitgasoil",prospectEntity.getRessourcesProspectEntity().getRse_prixunitgasoil());
            object.put("rseextrautilsgenerator",prospectEntity.getRessourcesProspectEntity().getRse_extrautilsgenerator());
            object.put("rselampepetrole",prospectEntity.getRessourcesProspectEntity().getRse_lampepetrole());
            object.put("rseqtitehebdopetrole",prospectEntity.getRessourcesProspectEntity().getRse_qtitehebdopetrole());
            object.put("rseprixunitpetrole",prospectEntity.getRessourcesProspectEntity().getRse_prixunitpetrole());
            object.put("rsebougie",prospectEntity.getRessourcesProspectEntity().getRse_bougie());
            object.put("rseqtitehebdobougie",prospectEntity.getRessourcesProspectEntity().getRse_qtitehebdobougie());
            object.put("rseprixunitbougie",prospectEntity.getRessourcesProspectEntity().getRse_prixunitbougie());
            object.put("rsepile",prospectEntity.getRessourcesProspectEntity().getRse_pile());
            object.put("rseqtitehebdopile",prospectEntity.getRessourcesProspectEntity().getRse_qtitehebdopile());
            object.put("rseprixunitpile",prospectEntity.getRessourcesProspectEntity().getRse_prixunitpile());
            object.put("rseqtitephonedispo",prospectEntity.getRessourcesProspectEntity().getRse_qtitephonedispo());
            object.put("moderecharge",prospectEntity.getRessourcesProspectEntity().getModerecharge());
            object.put("rsecoutrechargephone",prospectEntity.getRessourcesProspectEntity().getRse_coutrechargephone());
            object.put("rsetotalhebdorecharge",prospectEntity.getRessourcesProspectEntity().getRse_totalhebdorecharge());
            object.put("rsetransport",prospectEntity.getRessourcesProspectEntity().getRse_transport());



        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    public static String convertGSON(IdentiteProspectEntity entity) {
        Gson gson = new Gson();

        String prospect = gson.toJson(entity);
        return  prospect;
    }

    public static IdentiteProspectEntity extratGSON(String json) {
        Gson ob =new Gson();
        IdentiteProspectEntity entity = new IdentiteProspectEntity();

        entity = ob.fromJson(json,IdentiteProspectEntity.class);
        return entity;
    }
}
