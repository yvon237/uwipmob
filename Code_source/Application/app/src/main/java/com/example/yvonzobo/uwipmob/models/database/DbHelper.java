package com.example.yvonzobo.uwipmob.models.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.yvonzobo.uwipmob.controllers.AgentController;
import com.example.yvonzobo.uwipmob.controllers.OperatorController;
import com.example.yvonzobo.uwipmob.controllers.ProspectController;
import com.example.yvonzobo.uwipmob.controllers.UserController;


/**
 * Created by Yvon ZOBO on 20/07/2016.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "uwipmob.db";
    private static final int VERSION = 5;


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);

       SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(AgentController.CREATE_TABLE_AGENT);
       /* db.execSQL(OperatorController.CREATE_TABLE_OPERATEUR);
        db.execSQL(ProspectController.CREATE_TABLE_PROSPECT);
        */


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {


        db.execSQL("DROP TABLE IF EXISTS "+ContentDb.TABLE_AGENT);

        onCreate(db);

    }
}
