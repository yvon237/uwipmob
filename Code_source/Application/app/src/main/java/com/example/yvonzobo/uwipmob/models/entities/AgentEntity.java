package com.example.yvonzobo.uwipmob.models.entities;

/**
 * Created by Yvon ZOBO on 09/10/2016.
 */
public class AgentEntity {


    private Long idagent;
    private Long iduuser;

    private String nom;
    private String prenom;
    private String typeagent;
    private String login;
    private String mdp;


    public AgentEntity() {

    }



    public Long getIdagent() {
        return idagent;
    }

    public void setIdagent(Long idagent) {
        this.idagent = idagent;
    }

    public Long getIduuser() {
        return iduuser;
    }

    public void setIduuser(Long iduuser) {
        this.iduuser = iduuser;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


    public String getTypeagent() {
        return typeagent;
    }

    public void setTypeagent(String typeagent) {
        this.typeagent = typeagent;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }
}
