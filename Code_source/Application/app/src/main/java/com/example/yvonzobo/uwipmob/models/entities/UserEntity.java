package com.example.yvonzobo.uwipmob.models.entities;

/**
 * Created by Yvon ZOBO on 20/07/2016.
 */
public class UserEntity {

    private Long iduser;
    private Long idprofile;
    private Long idorganisation;
    private String firstname;
    private String lastname;
    private String datenaiss;
    private String login;
    private String password;


    public UserEntity(){

    }

    public Long getIduser() {
        return iduser;
    }

    public void setIduser(Long iduser) {
        this.iduser = iduser;
    }

    public Long getIdprofile() {
        return idprofile;
    }

    public void setIdprofile(Long idprofile) {
        this.idprofile = idprofile;
    }

    public Long getIdorganisation() {
        return idorganisation;
    }

    public void setIdorganisation(Long idorganisation) {
        this.idorganisation = idorganisation;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDatenaiss() {
        return datenaiss;
    }

    public void setDatenaiss(String datenaiss) {
        this.datenaiss = datenaiss;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
