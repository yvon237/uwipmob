package com.example.yvonzobo.uwipmob.models.entities;

import android.support.annotation.Size;

import java.util.Date;

/**
 * Created by yvon on 19/10/16.
 */
public class Client {


    public Client() {

    }


    private Long idclient;


    private String firstname;


    private String lastname;

    private Integer clientuniqnumber;

    private String nationality;

    private Integer totalbox;

    private String preferredphone;

    private String phoneorange;

    private String phonemtn;
    private String phonenexttel;

    private String cnipic;

    private Date acquisitdate;

    private String firstcontact;

    private String statusindividu;

    private String idsexe;

    private Integer idage;

    private String idlangue;

    private String mmdispo;

    private String idemail;

    private String coordtypezone;

    private String coordsourcing;

    private Integer besointotallampe;

    private Integer besointotalradio;

    private Integer besointotaltorche;

    private String rsesatisfaiteneo;

    private Integer rsefacturemensuelleeneo;

    private String rsegenerateur;

    private Integer rseqtitehebdogasoil;

    private Integer rseprixunitgasoil;

    private String rselampeapetrole;

    private String rseextrautilsgenerator;

    private Integer rseqtitehebdopetrole;

    private Integer rseprixunitpetrole;

    private String rsebougie;

    private Integer rseqtitehebdobougie;

    private Integer rseprixunitbougie;

    private String rsepiles;

    private Integer rseqtitehebdopile;
    private Integer rseprixunitpile;
    private Integer rsetransport;
    private Integer rseqtitephonedispo;
    private String rsemoderecharge;
    private Integer rsetotalhebdorecharge;
    private Integer rsecoutrechargephone;
    private String solvablematrimonialstatus;
    private Integer solvabletotalenfant;
    private String solvableactiviteprof1;
    private String solvableactiviteprof2;
    private String solvablebetail;
    private String solvableculture;
    private String solvabletypefonction;
    private String solvabletypecommerce;
    private String solvableautresource;
    private String solvableactiviteprofconjoint;
    private Date suividateacompteprevu;
    private String solvabledureeperiodecreuse;
    private String solvablesuretepaye;
    private String solvableepargneactu;
    private String solvableempruntactu;
    private Integer solvablenbmaisons;
    private Integer solvablenbhectares;

    private Integer solvablenbvoitures;
    private Integer solvablenbmotos;
    private String solvablepatrimoineautre;
    private String solvabiliteassociationactu;
    private String suivilieurencontre;
    private String solvableremarques;
    private Date suividatenextrelance;
    private Long idagent;
    private Long idvillage;


    public Long getIdclient() {
        return idclient;
    }

    public void setIdclient(Long idclient) {
        this.idclient = idclient;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Integer getClientuniqnumber() {
        return clientuniqnumber;
    }

    public void setClientuniqnumber(Integer clientuniqnumber) {
        this.clientuniqnumber = clientuniqnumber;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Integer getTotalbox() {
        return totalbox;
    }

    public void setTotalbox(Integer totalbox) {
        this.totalbox = totalbox;
    }

    public String getPreferredphone() {
        return preferredphone;
    }

    public void setPreferredphone(String preferredphone) {
        this.preferredphone = preferredphone;
    }

    public String getPhoneorange() {
        return phoneorange;
    }

    public void setPhoneorange(String phoneorange) {
        this.phoneorange = phoneorange;
    }

    public String getPhonemtn() {
        return phonemtn;
    }

    public void setPhonemtn(String phonemtn) {
        this.phonemtn = phonemtn;
    }

    public String getPhonenexttel() {
        return phonenexttel;
    }

    public void setPhonenexttel(String phonenexttel) {
        this.phonenexttel = phonenexttel;
    }

    public String getCnipic() {
        return cnipic;
    }

    public void setCnipic(String cnipic) {
        this.cnipic = cnipic;
    }

    public Date getAcquisitdate() {
        return acquisitdate;
    }

    public void setAcquisitdate(Date acquisitdate) {
        this.acquisitdate = acquisitdate;
    }

    public String getFirstcontact() {
        return firstcontact;
    }

    public void setFirstcontact(String firstcontact) {
        this.firstcontact = firstcontact;
    }

    public String getStatusindividu() {
        return statusindividu;
    }

    public void setStatusindividu(String statusindividu) {
        this.statusindividu = statusindividu;
    }

    public String getIdsexe() {
        return idsexe;
    }

    public void setIdsexe(String idsexe) {
        this.idsexe = idsexe;
    }

    public Integer getIdage() {
        return idage;
    }

    public void setIdage(Integer idage) {
        this.idage = idage;
    }

    public String getIdlangue() {
        return idlangue;
    }

    public void setIdlangue(String idlangue) {
        this.idlangue = idlangue;
    }

    public String getMmdispo() {
        return mmdispo;
    }

    public void setMmdispo(String mmdispo) {
        this.mmdispo = mmdispo;
    }

    public String getIdemail() {
        return idemail;
    }

    public void setIdemail(String idemail) {
        this.idemail = idemail;
    }

    public String getCoordtypezone() {
        return coordtypezone;
    }

    public void setCoordtypezone(String coordtypezone) {
        this.coordtypezone = coordtypezone;
    }

    public String getCoordsourcing() {
        return coordsourcing;
    }

    public void setCoordsourcing(String coordsourcing) {
        this.coordsourcing = coordsourcing;
    }

    public Integer getBesointotallampe() {
        return besointotallampe;
    }

    public void setBesointotallampe(Integer besointotallampe) {
        this.besointotallampe = besointotallampe;
    }

    public Integer getBesointotalradio() {
        return besointotalradio;
    }

    public void setBesointotalradio(Integer besointotalradio) {
        this.besointotalradio = besointotalradio;
    }

    public Integer getBesointotaltorche() {
        return besointotaltorche;
    }

    public void setBesointotaltorche(Integer besointotaltorche) {
        this.besointotaltorche = besointotaltorche;
    }

    public String getRsesatisfaiteneo() {
        return rsesatisfaiteneo;
    }

    public void setRsesatisfaiteneo(String rsesatisfaiteneo) {
        this.rsesatisfaiteneo = rsesatisfaiteneo;
    }

    public Integer getRsefacturemensuelleeneo() {
        return rsefacturemensuelleeneo;
    }

    public void setRsefacturemensuelleeneo(Integer rsefacturemensuelleeneo) {
        this.rsefacturemensuelleeneo = rsefacturemensuelleeneo;
    }

    public Integer getRseqtitehebdogasoil() {
        return rseqtitehebdogasoil;
    }

    public void setRseqtitehebdogasoil(Integer rseqtitehebdogasoil) {
        this.rseqtitehebdogasoil = rseqtitehebdogasoil;
    }

    public String getRsegenerateur() {
        return rsegenerateur;
    }

    public void setRsegenerateur(String rsegenerateur) {
        this.rsegenerateur = rsegenerateur;
    }

    public Integer getRseprixunitgasoil() {
        return rseprixunitgasoil;
    }

    public void setRseprixunitgasoil(Integer rseprixunitgasoil) {
        this.rseprixunitgasoil = rseprixunitgasoil;
    }

    public String getRselampeapetrole() {
        return rselampeapetrole;
    }

    public void setRselampeapetrole(String rselampeapetrole) {
        this.rselampeapetrole = rselampeapetrole;
    }

    public String getRseextrautilsgenerator() {
        return rseextrautilsgenerator;
    }

    public void setRseextrautilsgenerator(String rseextrautilsgenerator) {
        this.rseextrautilsgenerator = rseextrautilsgenerator;
    }

    public Integer getRseqtitehebdopetrole() {
        return rseqtitehebdopetrole;
    }

    public void setRseqtitehebdopetrole(Integer rseqtitehebdopetrole) {
        this.rseqtitehebdopetrole = rseqtitehebdopetrole;
    }

    public Integer getRseprixunitpetrole() {
        return rseprixunitpetrole;
    }

    public void setRseprixunitpetrole(Integer rseprixunitpetrole) {
        this.rseprixunitpetrole = rseprixunitpetrole;
    }

    public String getRsebougie() {
        return rsebougie;
    }

    public void setRsebougie(String rsebougie) {
        this.rsebougie = rsebougie;
    }

    public Integer getRseqtitehebdobougie() {
        return rseqtitehebdobougie;
    }

    public void setRseqtitehebdobougie(Integer rseqtitehebdobougie) {
        this.rseqtitehebdobougie = rseqtitehebdobougie;
    }

    public Integer getRseprixunitbougie() {
        return rseprixunitbougie;
    }

    public void setRseprixunitbougie(Integer rseprixunitbougie) {
        this.rseprixunitbougie = rseprixunitbougie;
    }

    public String getRsepiles() {
        return rsepiles;
    }

    public void setRsepiles(String rsepiles) {
        this.rsepiles = rsepiles;
    }

    public Integer getRseqtitehebdopile() {
        return rseqtitehebdopile;
    }

    public void setRseqtitehebdopile(Integer rseqtitehebdopile) {
        this.rseqtitehebdopile = rseqtitehebdopile;
    }

    public Integer getRseprixunitpile() {
        return rseprixunitpile;
    }

    public void setRseprixunitpile(Integer rseprixunitpile) {
        this.rseprixunitpile = rseprixunitpile;
    }

    public Integer getRsetransport() {
        return rsetransport;
    }

    public void setRsetransport(Integer rsetransport) {
        this.rsetransport = rsetransport;
    }

    public Integer getRseqtitephonedispo() {
        return rseqtitephonedispo;
    }

    public void setRseqtitephonedispo(Integer rseqtitephonedispo) {
        this.rseqtitephonedispo = rseqtitephonedispo;
    }

    public String getRsemoderecharge() {
        return rsemoderecharge;
    }

    public void setRsemoderecharge(String rsemoderecharge) {
        this.rsemoderecharge = rsemoderecharge;
    }

    public Integer getRsetotalhebdorecharge() {
        return rsetotalhebdorecharge;
    }

    public void setRsetotalhebdorecharge(Integer rsetotalhebdorecharge) {
        this.rsetotalhebdorecharge = rsetotalhebdorecharge;
    }

    public Integer getRsecoutrechargephone() {
        return rsecoutrechargephone;
    }

    public void setRsecoutrechargephone(Integer rsecoutrechargephone) {
        this.rsecoutrechargephone = rsecoutrechargephone;
    }

    public Integer getSolvabletotalenfant() {
        return solvabletotalenfant;
    }

    public void setSolvabletotalenfant(Integer solvabletotalenfant) {
        this.solvabletotalenfant = solvabletotalenfant;
    }

    public String getSolvablematrimonialstatus() {
        return solvablematrimonialstatus;
    }

    public void setSolvablematrimonialstatus(String solvablematrimonialstatus) {
        this.solvablematrimonialstatus = solvablematrimonialstatus;
    }

    public String getSolvableactiviteprof1() {
        return solvableactiviteprof1;
    }

    public void setSolvableactiviteprof1(String solvableactiviteprof1) {
        this.solvableactiviteprof1 = solvableactiviteprof1;
    }

    public String getSolvableactiviteprof2() {
        return solvableactiviteprof2;
    }

    public void setSolvableactiviteprof2(String solvableactiviteprof2) {
        this.solvableactiviteprof2 = solvableactiviteprof2;
    }

    public String getSolvablebetail() {
        return solvablebetail;
    }

    public void setSolvablebetail(String solvablebetail) {
        this.solvablebetail = solvablebetail;
    }

    public String getSolvableculture() {
        return solvableculture;
    }

    public void setSolvableculture(String solvableculture) {
        this.solvableculture = solvableculture;
    }

    public String getSolvabletypefonction() {
        return solvabletypefonction;
    }

    public void setSolvabletypefonction(String solvabletypefonction) {
        this.solvabletypefonction = solvabletypefonction;
    }

    public String getSolvabletypecommerce() {
        return solvabletypecommerce;
    }

    public void setSolvabletypecommerce(String solvabletypecommerce) {
        this.solvabletypecommerce = solvabletypecommerce;
    }

    public String getSolvableautresource() {
        return solvableautresource;
    }

    public void setSolvableautresource(String solvableautresource) {
        this.solvableautresource = solvableautresource;
    }

    public String getSolvableactiviteprofconjoint() {
        return solvableactiviteprofconjoint;
    }

    public void setSolvableactiviteprofconjoint(String solvableactiviteprofconjoint) {
        this.solvableactiviteprofconjoint = solvableactiviteprofconjoint;
    }

    public Date getSuividateacompteprevu() {
        return suividateacompteprevu;
    }

    public void setSuividateacompteprevu(Date suividateacompteprevu) {
        this.suividateacompteprevu = suividateacompteprevu;
    }

    public String getSolvabledureeperiodecreuse() {
        return solvabledureeperiodecreuse;
    }

    public void setSolvabledureeperiodecreuse(String solvabledureeperiodecreuse) {
        this.solvabledureeperiodecreuse = solvabledureeperiodecreuse;
    }

    public String getSolvablesuretepaye() {
        return solvablesuretepaye;
    }

    public void setSolvablesuretepaye(String solvablesuretepaye) {
        this.solvablesuretepaye = solvablesuretepaye;
    }

    public String getSolvableepargneactu() {
        return solvableepargneactu;
    }

    public void setSolvableepargneactu(String solvableepargneactu) {
        this.solvableepargneactu = solvableepargneactu;
    }

    public String getSolvableempruntactu() {
        return solvableempruntactu;
    }

    public void setSolvableempruntactu(String solvableempruntactu) {
        this.solvableempruntactu = solvableempruntactu;
    }

    public Integer getSolvablenbmaisons() {
        return solvablenbmaisons;
    }

    public void setSolvablenbmaisons(Integer solvablenbmaisons) {
        this.solvablenbmaisons = solvablenbmaisons;
    }

    public Integer getSolvablenbhectares() {
        return solvablenbhectares;
    }

    public void setSolvablenbhectares(Integer solvablenbhectares) {
        this.solvablenbhectares = solvablenbhectares;
    }

    public Integer getSolvablenbvoitures() {
        return solvablenbvoitures;
    }

    public void setSolvablenbvoitures(Integer solvablenbvoitures) {
        this.solvablenbvoitures = solvablenbvoitures;
    }

    public Integer getSolvablenbmotos() {
        return solvablenbmotos;
    }

    public void setSolvablenbmotos(Integer solvablenbmotos) {
        this.solvablenbmotos = solvablenbmotos;
    }

    public String getSolvablepatrimoineautre() {
        return solvablepatrimoineautre;
    }

    public void setSolvablepatrimoineautre(String solvablepatrimoineautre) {
        this.solvablepatrimoineautre = solvablepatrimoineautre;
    }

    public String getSolvabiliteassociationactu() {
        return solvabiliteassociationactu;
    }

    public void setSolvabiliteassociationactu(String solvabiliteassociationactu) {
        this.solvabiliteassociationactu = solvabiliteassociationactu;
    }

    public String getSuivilieurencontre() {
        return suivilieurencontre;
    }

    public void setSuivilieurencontre(String suivilieurencontre) {
        this.suivilieurencontre = suivilieurencontre;
    }

    public String getSolvableremarques() {
        return solvableremarques;
    }

    public void setSolvableremarques(String solvableremarques) {
        this.solvableremarques = solvableremarques;
    }

    public Date getSuividatenextrelance() {
        return suividatenextrelance;
    }

    public void setSuividatenextrelance(Date suividatenextrelance) {
        this.suividatenextrelance = suividatenextrelance;
    }

    public Long getIdagent() {
        return idagent;
    }

    public void setIdagent(Long idagent) {
        this.idagent = idagent;
    }

    public Long getIdvillage() {
        return idvillage;
    }

    public void setIdvillage(Long idvillage) {
        this.idvillage = idvillage;
    }
}
