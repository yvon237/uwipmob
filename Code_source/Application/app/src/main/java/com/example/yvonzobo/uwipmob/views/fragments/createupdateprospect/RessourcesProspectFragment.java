package com.example.yvonzobo.uwipmob.views.fragments.createupdateprospect;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.yvonzobo.uwipmob.R;
import com.example.yvonzobo.uwipmob.bundle.ConstructBundleProspect;
import com.example.yvonzobo.uwipmob.models.entities.prospects.RessourcesProspectEntity;


public class RessourcesProspectFragment extends Fragment {



    private OnResourcesFragmentInteractionListener mListener;

    private TextView t_rsesatisfaiteneo, t_rsefacturemensuelleeneo,t_rsegenerator,t_rseqtitehebdogasoil,
    t_rseprixunitgasoil,t_rseextrautilsgenerator,t_rselampepetrole,t_rseqtitehebdopetrole,t_rseprixunitpetrole,t_rsebougie,t_deplacement,
    t_rseqtitehebdobougie,t_rseprixunitbougie,t_rsepile,t_rseqtitehebdopile,t_rseprixunitpile,t_rseqtitephonedispo, t_moderecharge,t_coutrechargephone,t_totalhebdorecharge,t_transport, couttransphone;
    private LinearLayout layoutrsesatisfaiteneo, layoutrsefacturemensuelleeneo,layoutrsegenerator,
           layoutrselampepetrole,layoutrsebougie,layoutcoutrtansphone,layoutcoutrecharge,
            layoutrsepile,layoutrseqtitephonedispo,layouttransport, layoutqtitegasoil,layoutprixunitgasoil,layoutrseextrautilsgenerator,layoutqtitepetrole,layoutprixunitpetrole,layoutqtitebougie,layoutprixunitbougie,
            layoutprixpile,layoutqtitepile,layoutmoderecharge, layouthebdorecharge;

    private EditText editText,editText1,editText2,editText3;
    private RadioButton yeseneo,noeneo,yesgenerator,nogenerator,yesotherusage,nootherusage,yeslampe,nolampe,yesbougie,nobougie,yestorche,notorche;
    private RadioGroup radioGroup, radioGroup1;
    private ImageView nextpage,previouspage;

    private RessourcesProspectEntity ressourcesProspectEntity = new RessourcesProspectEntity();


    public RessourcesProspectFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static RessourcesProspectFragment newInstance(Bundle identite,Bundle coordonnees) {
        RessourcesProspectFragment fragment = new RessourcesProspectFragment();
        Bundle args = new Bundle();
       args.putBundle("identite",identite);
        args.putBundle("coordonnees",coordonnees);
        fragment.setArguments(args);
        return fragment;
    }
    public static RessourcesProspectFragment newInstance(Bundle ressources,String whatcall) {
        RessourcesProspectFragment fragment = new RessourcesProspectFragment();
        Bundle args = new Bundle();
        args.putBundle("ressources",ressources);
        args.putString("whatcall",whatcall);
        fragment.setArguments(args);

        return fragment;
    }

    public Bundle getBundleRessources() {
        return getArguments().getBundle("ressources");
    }

    public Bundle getBundleIdentite() {
        return getArguments().getBundle("identite");

    }

    public Bundle getBundleCoordonnees() {
        return getArguments().getBundle("coordonnees");
    }

    public void onRestoreInstanceState() {
        if (getBundleRessources() != null) {
            if (getArguments().getString("whatcall") == "precedent") {

            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_ressources_prospect, container, false);

        t_rsesatisfaiteneo = (TextView)view.findViewById(R.id.t_satisfaiteneo);
        t_rsefacturemensuelleeneo = (TextView)view.findViewById(R.id.t_facteneo);
        t_rsegenerator = (TextView)view.findViewById(R.id.t_generateur);
        t_rseqtitehebdogasoil = (TextView)view.findViewById(R.id.t_qtitegasoil);
        t_rseprixunitgasoil = (TextView)view.findViewById(R.id.t_prixgasoil);
        t_rseextrautilsgenerator = (TextView)view.findViewById(R.id.t_autreusage);
        t_rselampepetrole = (TextView)view.findViewById(R.id.t_lampepetrole);
        t_rseqtitehebdopetrole = (TextView)view.findViewById(R.id.t_qtitepetrole);
        t_rseprixunitpetrole = (TextView)view.findViewById(R.id.t_prixpetrole);
        t_rsebougie = (TextView)view.findViewById(R.id.t_bougie);
        t_rseqtitehebdobougie = (TextView)view.findViewById(R.id.t_qtitebougie);
        t_rseprixunitbougie = (TextView)view.findViewById(R.id.t_prixbougie);
        t_rsepile = (TextView)view.findViewById(R.id.t_rsetorche);
        t_rseqtitehebdopile = (TextView)view.findViewById(R.id.t_qtitepile);
        t_rseprixunitpile = (TextView)view.findViewById(R.id.t_prixpile);
        t_rseqtitephonedispo = (TextView)view.findViewById(R.id.t_qtitephone);
        t_moderecharge = (TextView)view.findViewById(R.id.t_moderecharge);
        t_coutrechargephone = (TextView)view.findViewById(R.id.t_coutrecharge);
        t_totalhebdorecharge = (TextView)view.findViewById(R.id.t_hebdorecharge);
        t_transport = (TextView)view.findViewById(R.id.t_deplacement);
        couttransphone = (TextView)view.findViewById(R.id.t_couttransphone);







        layoutrsesatisfaiteneo = (LinearLayout)view.findViewById(R.id.layoutsatisfaiteneo);
        layoutrsefacturemensuelleeneo = (LinearLayout)view.findViewById(R.id.layoutfacteneo);
        layoutrsegenerator = (LinearLayout)view.findViewById(R.id.layoutgenerateur);
        layoutrselampepetrole = (LinearLayout)view.findViewById(R.id.layoutlampepetrole);
        layoutrsebougie = (LinearLayout)view.findViewById(R.id.layoutbougie);
        layoutrsepile = (LinearLayout)view.findViewById(R.id.layoutrsetorche);
        layoutrseqtitephonedispo = (LinearLayout)view.findViewById(R.id.layoutqtitephone);
        layouttransport = (LinearLayout)view.findViewById(R.id.layoutcouttrans);

        layoutqtitegasoil = (LinearLayout)view.findViewById(R.id.layoutqtitegasoil);
        layoutprixunitgasoil = (LinearLayout)view.findViewById(R.id.layoutprixgasoil);
        layoutrseextrautilsgenerator = (LinearLayout)view.findViewById(R.id.layoutautreusage);
        layoutqtitepetrole = (LinearLayout)view.findViewById(R.id.layoutqtitepetrole);
        layoutprixunitpetrole  = (LinearLayout)view.findViewById(R.id.layoutprixpetrole);
        layoutqtitebougie = (LinearLayout)view.findViewById(R.id.layoutqtitebougie);
        layoutprixunitbougie = (LinearLayout)view.findViewById(R.id.layoutprixbougie);
        layoutprixpile = (LinearLayout)view.findViewById(R.id.layoutprixpile);
        layoutqtitepile = (LinearLayout)view.findViewById(R.id.layoutqtitepile);
        layoutcoutrtansphone = (LinearLayout)view.findViewById(R.id.layoutcoutrtansphone);
        layoutcoutrecharge = (LinearLayout)view.findViewById(R.id.layoutcoutrecharge);
        layoutmoderecharge = (LinearLayout)view.findViewById(R.id.layoutmoderecharge);
        layoutrseqtitephonedispo = (LinearLayout)view.findViewById(R.id.layoutnbphone);
        layouttransport =  (LinearLayout)view.findViewById(R.id.layoutdeplacement);
        layouthebdorecharge =  (LinearLayout)view.findViewById(R.id.layouthebdorecharge);



        nextpage = (ImageView)view.findViewById(R.id.nextpage);
        previouspage = (ImageView)view.findViewById(R.id.previouspage);


        onClickLayoutSatisfaiteneo();

        // gestion source energie generateur
        onClickLayoutrsegenerator();
        onClickrseQtiteprixhebdoGasoil();
        onClickrseExtrautilsGenerator();

        //gestion source energie petrole
        onClickLayoutrselampepetrole();
        onClickLayoutrseQtiteprixpetrole();

        //gestion source energei bougie
        onClickrseBougie();
        onClickLayoutrseQtiteprixbougie();

        //gestion source energie pile
        onClickLayoutrsePile();
        onClickLayoutrseQtitePrixPile();
        //gestion recharge telephone
        onClickLayoutModeRecharge();
        onClickLayoutCoutTrasportPhone();

        onClickLayoutphoneDispo();
        onClickLayoutNbRechargePhone();
        onClickLayoutTransport();




        return view;

    }


    private void onClickNextPage() {
        nextpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //passage des bundle à l'interface
                onButtonPressed(getBundleIdentite(),getBundleCoordonnees(),ConstructBundleProspect.constructBundleRessources(ressourcesProspectEntity));
            }
        });
    }

    private void onClickPreviousPage() {
        previouspage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
    public void onClickLayoutSatisfaiteneo() {
        layoutrsesatisfaiteneo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rseEneo("Abonné au réseau Eneo");
            }
        });
    }


    public void onClickLayoutrsegenerator () {
        layoutrsegenerator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrseGenerateur("Utilisation du générateur");
            }
        });
    }

    public void onClickrseQtiteprixhebdoGasoil() {
        layoutqtitegasoil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrseprixqunatite("Quantite/Prix","Prix d'un litre de gasoil","Quantite consommée par semaine",t_rseprixunitgasoil,t_rseqtitehebdogasoil);

            }
        });
    }



    public void onClickrseExtrautilsGenerator() {
        layoutrseextrautilsgenerator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrseExtrautilisGenerateur("autre usage du generateur");
            }
        });
    }
     public void onClickLayoutrselampepetrole () {
         layoutrselampepetrole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              dialogRessourceClient("Utilisation lampe a petrole",t_rselampepetrole,layoutprixunitpetrole,layoutqtitepetrole);
            }
        });
    }

    public void onClickLayoutrseQtiteprixpetrole() {
        layoutqtitepetrole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrseprixqunatite("Quantite/Prix","Prix d'un litre petrole","Quantite de litre par semaine",t_rseprixunitpetrole,t_rseqtitehebdopetrole);

            }
        });
    }

    public void onClickrseBougie() {
        layoutrsebougie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogRessourceClient("Utilisation bougie",t_rsebougie,layoutprixunitbougie,layoutqtitebougie);

            }
        });
    }


    public void onClickLayoutrseQtiteprixbougie() {
        layoutqtitebougie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrseprixqunatite("Quantite/Prix","Prix d'une bougie","Quantite par semaine",t_rseprixunitbougie,t_rseqtitehebdobougie);
            }
        });
    }

    public void onClickLayoutrsePile() {
        layoutrsepile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogRessourceClient("Utilisation torche",t_rsepile,layoutprixpile,layoutqtitepile);
            }
        });
    }

    public void onClickLayoutrseQtitePrixPile() {

        layoutqtitepile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrseprixqunatite("Quantite/Prix","Prix d'une pile","Quantite par semaine",t_rseprixunitpile,t_rseqtitehebdopile);

            }
        });

    }

    public void onClickLayoutModeRecharge() {
        layoutmoderecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrseLieuRecharge("lieu recharge telephone");
            }
        });
    }

    public void onClickLayoutCoutTrasportPhone() {
        layoutcoutrtansphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrseprixqunatite("depenses recharge telephone","cout deplacement","cout recharge",couttransphone,t_coutrechargephone);

            }
        });
    }

    public void onClickLayoutphoneDispo() {
        layoutrseqtitephonedispo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrseqteuniq("nombre Telephone","quantite telephone",t_rseqtitephonedispo);
            }
        });
    }

    public void onClickLayoutNbRechargePhone() {
        layouthebdorecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrseqteuniq("recharge hebdomadaire","nombre recharge par semaine",t_coutrechargephone);
            }
        });
    }

    public void onClickLayoutTransport() {
        layouttransport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogrseqteuniq("deplacement","montant deplacement par semaine",t_transport);

            }
        });
    }


    public void rseEneo(String titre){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        yeseneo = new RadioButton(getActivity());
        yeseneo.setText("oui");
        yeseneo.setId(RadioButton.generateViewId());


        noeneo = new RadioButton(getActivity());
        noeneo.setText("non");
        noeneo.setId(RadioButton.generateViewId());

        radioGroup = new RadioGroup(getActivity());
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        radioGroup.addView(yeseneo);
        radioGroup.addView(noeneo);

        TextInputLayout textInputLayout = new TextInputLayout(getActivity());
        editText = new EditText(getActivity());


        layout.addView(radioGroup);




        editText.setHint("prix facture eneo");
        textInputLayout.addView(editText);
        layout.addView(textInputLayout);

        alertDialog.setView(layout);






        // Setting Dialog Title
        alertDialog.setTitle(titre);


        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog,int which) {


                if (radioGroup.getCheckedRadioButtonId() == yeseneo.getId()) {
                    t_rsesatisfaiteneo.setText("oui");

                } else if (radioGroup.getCheckedRadioButtonId() == noeneo.getId()) {
                    t_rsesatisfaiteneo.setText("non");
                }

                t_rsefacturemensuelleeneo.setText(editText.getText().toString());


                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            }

        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void dialogrseGenerateur(String titre){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        final LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        alertDialog.setCancelable(false);

        yesgenerator = new RadioButton(getActivity());
        yesgenerator.setText("oui");
        yesgenerator.setId(RadioButton.generateViewId());


        nogenerator = new RadioButton(getActivity());
        nogenerator.setText("non");
        nogenerator.setId(RadioButton.generateViewId());



        radioGroup = new RadioGroup(getActivity());
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        radioGroup.addView(yesgenerator);
        radioGroup.addView(nogenerator);


        layout.addView(radioGroup);


            alertDialog.setView(layout);


        // Setting Dialog Title
        alertDialog.setTitle(titre);


        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog,int which) {


                if (radioGroup.getCheckedRadioButtonId() == yesgenerator.getId()) {

                    t_rsegenerator.setText("oui");
                    layoutqtitegasoil.setVisibility(LinearLayout.VISIBLE);
                    layoutprixunitgasoil.setVisibility(LinearLayout.VISIBLE);
                    layoutrseextrautilsgenerator.setVisibility(View.VISIBLE);
                   // t_rseqtitehebdogasoil.setVisibility(TextView.VISIBLE);

                } else if (radioGroup.getCheckedRadioButtonId() == nogenerator.getId()) {
                    t_rsegenerator.setText("non");
                }

                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            }

        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
    public void dialogrseExtrautilisGenerateur(String titre){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        final LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        alertDialog.setCancelable(false);


        yesotherusage = new RadioButton(getActivity());
        yesotherusage.setText("oui");
        yesotherusage.setId(RadioButton.generateViewId());

        nootherusage = new RadioButton(getActivity());
        nootherusage.setText("non");
        nootherusage.setId(RadioButton.generateViewId());



        radioGroup1 = new RadioGroup(getActivity());
        radioGroup1.setOrientation(RadioGroup.HORIZONTAL);
        radioGroup1.addView(yesotherusage);
        radioGroup1.addView(nootherusage);

        layout.addView(radioGroup1);







            alertDialog.setView(layout);


        // Setting Dialog Title
        alertDialog.setTitle(titre);


        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog,int which) {

                if (radioGroup1.getCheckedRadioButtonId() == yesotherusage.getId()) {
                    t_rseextrautilsgenerator.setText("oui");
                } else if (radioGroup1.getCheckedRadioButtonId() == nootherusage.getId()) {
                    t_rseextrautilsgenerator.setText("non");
                }

                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            }

        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public void dialogrseprixqunatite(String titre, String titleprix,String titleqtite, final TextView resultprix,final TextView resultatqtite){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        final LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        alertDialog.setCancelable(false);


        final TextInputLayout textInputLayout = new TextInputLayout(getActivity());
        final TextInputLayout textInputLayout1 = new TextInputLayout(getActivity());

        editText = new EditText(getActivity());
        editText1 = new EditText(getActivity());




            editText.setHint(titleprix);
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            textInputLayout.addView(editText);

            editText1.setHint(titleqtite);
            editText1.setInputType(InputType.TYPE_CLASS_NUMBER);
            textInputLayout1.addView(editText1);

            layout.addView(textInputLayout);
            layout.addView(textInputLayout1);



            alertDialog.setView(layout);


        // Setting Dialog Title
        alertDialog.setTitle(titre);


        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog,int which) {



                    resultprix.setText(editText.getText().toString());
                    resultatqtite.setText(editText1.getText().toString());

                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            }

        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
  public void dialogrseqteuniq(String titre, String titleeditext,final TextView resultat){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        final LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        alertDialog.setCancelable(false);


        final TextInputLayout textInputLayout = new TextInputLayout(getActivity());

        editText = new EditText(getActivity());





            editText.setHint(titleeditext);
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            textInputLayout.addView(editText);

            layout.addView(textInputLayout);




            alertDialog.setView(layout);


        // Setting Dialog Title
        alertDialog.setTitle(titre);


        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog,int which) {



                    resultat.setText(editText.getText().toString());


                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            }

        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void dialogRessourceClient(String titre, final TextView resultat, final LinearLayout prix, final LinearLayout quantite){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        final LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        alertDialog.setCancelable(false);

         final RadioButton   radioyes = new RadioButton(getActivity());
        radioyes.setText("oui");
        radioyes.setId(RadioButton.generateViewId());


        final RadioButton   radiono = new RadioButton(getActivity());
        radiono.setText("non");
        radiono.setId(RadioButton.generateViewId());



        radioGroup = new RadioGroup(getActivity());
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        radioGroup.addView(radioyes);
        radioGroup.addView(radiono);


        layout.addView(radioGroup);


        alertDialog.setView(layout);


        // Setting Dialog Title
        alertDialog.setTitle(titre);


        // On pressing Settings button

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog,int which) {


                if (radioGroup.getCheckedRadioButtonId() == radioyes.getId()) {

                    resultat.setText("oui");
                    quantite.setVisibility(LinearLayout.VISIBLE);
                    prix.setVisibility(LinearLayout.VISIBLE);

                } else if (radioGroup.getCheckedRadioButtonId() == radiono.getId()) {
                    resultat.setText("non");
                }

                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            }

        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public void dialogrseLieuRecharge(String titre){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        final LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        alertDialog.setCancelable(false);

        final RadioButton domicile = new RadioButton(getActivity());
        domicile.setText("à domicile");
        domicile.setId(RadioButton.generateViewId());


        final RadioButton exterieur = new RadioButton(getActivity());
        exterieur.setText("à l'extérieur");
        exterieur.setId(RadioButton.generateViewId());



        radioGroup = new RadioGroup(getActivity());
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        radioGroup.addView(yesbougie);
        radioGroup.addView(nobougie);


        layout.addView(radioGroup);


        alertDialog.setView(layout);


        // Setting Dialog Title
        alertDialog.setTitle(titre);


        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog,int which) {


                if (radioGroup.getCheckedRadioButtonId() == exterieur.getId()) {

                    t_moderecharge.setText("exterieur");
                    layoutcoutrtansphone.setVisibility(LinearLayout.VISIBLE);
                    layoutcoutrecharge.setVisibility(LinearLayout.VISIBLE);

                } else if (radioGroup.getCheckedRadioButtonId() == domicile.getId()) {
                    t_rsebougie.setText("domicile");
                }

                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            }

        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    /**
     * passage des trois bundle au fragment suivant
     * @param identite
     * @param coordonnees
     * @param ressources
     */
    public void onButtonPressed(Bundle identite,Bundle coordonnees,Bundle ressources ){
        Bundle datas = new Bundle();
        datas.putBundle("identite",identite);
        datas.putBundle("coordonnees",coordonnees);
        datas.putBundle("ressources",ressources);
        if (mListener != null) {
            mListener.onCommunicateDataInteraction(datas);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnResourcesFragmentInteractionListener) {
            mListener = (OnResourcesFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnResourcesFragmentInteractionListener {
         void onCommunicateDataInteraction(Bundle datas);
        void onPassDataCallBackRessourcesProspectInteraction(Bundle identiteprospect, String precedent);
    }
}
