package com.example.yvonzobo.uwipmob.models.entities;

/**
 * Created by Yvon ZOBO on 11/10/2016.
 */
public class VillageEntity {

    private Long idvillage;
    private String libelle_village;

    public VillageEntity() {

    }

    public Long getIdvillage() {
        return idvillage;
    }

    public void setIdvillage(Long idvillage) {
        this.idvillage = idvillage;
    }

    public String getLibelle_village() {
        return libelle_village;
    }

    public void setLibelle_village(String libelle_village) {
        this.libelle_village = libelle_village;
    }
}
