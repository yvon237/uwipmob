package com.example.yvonzobo.uwipmob.views.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.yvonzobo.uwipmob.R;
import com.example.yvonzobo.uwipmob.controllers.OperatorController;
import com.example.yvonzobo.uwipmob.controllers.ProspectController;
import com.example.yvonzobo.uwipmob.models.entities.OperatorEntity;
import com.example.yvonzobo.uwipmob.models.entities.ProspectEntity;
import com.example.yvonzobo.uwipmob.synchronisation.SyncCustomer;
import com.example.yvonzobo.uwipmob.utility.CheckNetworkConnection;
import com.example.yvonzobo.uwipmob.utility.ExtractJsonObject;
import com.example.yvonzobo.uwipmob.utility.Urls;
import com.example.yvonzobo.uwipmob.utility.Utility;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MenuPrincipalFragment.MenuPrincipalInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MenuPrincipalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuPrincipalFragment extends Fragment {


    private MenuPrincipalInteractionListener mListener;
    private LinearLayout layoutgestclient, layougestinstallation, layoutgestmaintenance,layoutsync;

    private String lienOperators = Utility.ADRESS_SERVER + "entities.mmoperator/GetAllmmOperator";
    private ProspectController controller;
    private OperatorController operatorController;
    private List<OperatorEntity> operatorEntities = new ArrayList<>();
    private List<ProspectEntity> entityList = new ArrayList<>();

    private ProgressDialog dialog;
    private AlertDialog.Builder builder;

    public MenuPrincipalFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static MenuPrincipalFragment newInstance() {
        MenuPrincipalFragment fragment = new MenuPrincipalFragment();

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_menu_principal, container, false);




        layoutgestclient = (LinearLayout)view.findViewById(R.id.layoutgestclient);
        layougestinstallation = (LinearLayout)view.findViewById(R.id.layoutinstallation);
        layoutgestmaintenance = (LinearLayout)view.findViewById(R.id.layoutmaintenance);
        layoutsync = (LinearLayout)view.findViewById(R.id.layoutsync);

        onClickGestClient();
        onClickGestInstallation();
        onClickGestMaintenance();
        onClickSync();

        controller = new ProspectController(getActivity());
        operatorController = new OperatorController(getActivity());

        saveMmoperator(lienOperators);
        saveCustomers(Urls.getallcustomersurl);



        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("Chargement liste en cours");
        dialog.setMessage("Patientez s'il vous plait...");
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);


        configPrivilege();
        return view;
    }

    private void configPrivilege() {
        if (getActivity().getIntent().getStringExtra("typeagent").equals("commercial")) {
                layougestinstallation.setVisibility(LinearLayout.GONE);
                layoutgestmaintenance.setVisibility(LinearLayout.GONE);
        } else if (getActivity().getIntent().getStringExtra("typeagent").equals("technicien")) {
            layoutgestclient.setVisibility(LinearLayout.GONE);
        } else {
            Toast.makeText(getActivity(),"Bonjour "+getActivity().getIntent().getStringExtra("typeagent"),Toast.LENGTH_LONG).show();
        }
    }

    public void onClickGestClient() {
        layoutgestclient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed("gestclient");
            }
        });
    }

    public void onClickGestInstallation() {
        layougestinstallation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed("gestinstallation");
            }
        });
    }

    public void onClickGestMaintenance() {
        layoutgestmaintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed("gestmaintenance");
            }
        });
    }

    public void onClickSync() {
        layoutsync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckNetworkConnection.isOnline(getActivity())) {

                    onCreateDialogSynchronisation();
                } else
                    onCreateDialogAlertNoSynchronisation();
            }
        });
    }





    /**
     * ajout des operateurs a la bd local
     *
     * @param lien
     */
    public void saveMmoperator(String lien) {
        AsyncHttpClient client = new AsyncHttpClient();

        if (CheckNetworkConnection.isOnline(getActivity())) {


            client.get(lien, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    // dialog.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    dialog.setMessage("Chargement des opérateurs en cours...");
                    operatorEntities = ExtractJsonObject.extractOperatorListJsonEntity(response);
                    boolean reponse = false;


                    for (int t = 0; t < operatorEntities.size(); t++) {
                        if (!operatorController.getOperatorByCode(operatorEntities.get(t).getCode()))
                            reponse = operatorController.createOperator(operatorEntities.get(t));
                    }
                    if (reponse) {
                        Toast.makeText(getActivity(), "ajout des operateurs terminés " + "avec succes!!", Toast
                                .LENGTH_LONG).show();
                    }

                    dialog.dismiss();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    dialog.setMessage("échec chargement des opérateurs!!");
                    if (statusCode == 404) {


                        Toast.makeText(getActivity(), "Ressource non trouvée dans le serveur", Toast.LENGTH_LONG).show();


                    } else if (statusCode == 500) {


                        Toast.makeText(getActivity(), "erreur 500, probleme avec la requete au serveur...", Toast.LENGTH_LONG).show();

                    } else {


                        Toast.makeText(getActivity(), "le serveur n'est pas disponible  ", Toast.LENGTH_LONG).show();

                    }
                    dialog.dismiss();
                }

            });
        }
    }

    /**
     * ajoout des clients a la bd locale
     *
     * @param url
     */
    public void saveCustomers(String url) {
        AsyncHttpClient client = new AsyncHttpClient();

        if (CheckNetworkConnection.isOnline(getActivity())) {
            client.get(url, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();

                    dialog.setMessage("chargement des clients en cours...");
                    dialog.show();
                    // Toast.makeText(getApplicationContext(),"chargement des clients en cours...",Toast.LENGTH_LONG).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);


                    entityList = ExtractJsonObject.extractProspectListJsonObject(response);


                    boolean reponse = false;
                    for (int j = 0; j < entityList.size(); j++) {
                        if (!controller.getCustomerById(entityList.get(j).getIdclient())) {

                            if (!controller.verifIfcustomerExist(entityList.get(j).getLastname(), entityList.get(j).getFirstname()))
                                reponse = controller.createCustomer(entityList.get(j));


                        } else
                            controller.upDateCustomerDistant_Locale(entityList.get(j), entityList.get(j).getIdclient());
                    }
                    if (reponse) {
                        dialog.setMessage("Ajout des clients terminés!!!");
                        Toast.makeText(getActivity(), "ajout des clients terminés!!", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);

                    dialog.setMessage("Echec ajout des clients!!!");

                    if (statusCode == 404) {


                        Toast.makeText(getActivity(), "Ressource non trouvée dans le serveur", Toast.LENGTH_LONG).show();


                    } else if (statusCode == 500) {


                        Toast.makeText(getActivity(), "erreur 500, probleme avec la requete au serveur...", Toast.LENGTH_LONG).show();

                    } else {


                        Toast.makeText(getActivity(), "le serveur n'est pas disponible pour ", Toast.LENGTH_LONG).show();

                    }

                    dialog.dismiss();
                }
            });
        }


    }


    private Dialog onCreateDialogSynchronisation() {
        builder = new AlertDialog.Builder(getActivity());


        builder.setTitle("Synchronisation");
        builder.setIcon(getResources().getDrawable(R.drawable.alert));
        builder.setMessage("Confirmez-vous l'envoi de données au serveur central?");


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonConfirmationClickListener
                ());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeConfirmationButtonClickListener());

        return builder.show();
    }


    private class OnPositiveButtonConfirmationClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialogInterface, int id) {

            //syncCustomer.synchronizeClient(getApplicationContext());
            synchronizeData();
            // synchronizeClient(getApplicationContext());



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }

    private class OnNegativeConfirmationButtonClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialogInterface, int id) {

            builder.setCancelable(true);
        }
    }

    private Dialog onCreateDialogAlertNoSynchronisation() {
        builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Synchronisation");
        builder.setIcon(getResources().getDrawable(R.drawable.alert));
        builder.setMessage("Désolé! pas de connexion internet.");
        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonConfirmClickListener
                ());
        return builder.show();
    }

    private class OnPositiveButtonConfirmClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialogInterface, int id) {

            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }


    public void synchronizeData() {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "Synchronisation en " + "cours", "Envoi données au serveur, patientez svp...", true);

        dialog.setCancelable(false);
        final android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
        new Thread(new Runnable() {
            @Override
            public void run() {
                SyncCustomer.synchronizeClient(getActivity());
                dialog.dismiss();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        //syncData(Urls.addclienturl,Urls.updatecustomerurl);

                        Toast.makeText(getActivity(), "Envoi de données au serveur terminé avec succès!!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();

    }


        // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String menuSelected) {
        if (mListener != null) {
            mListener.menuPrincipalEventListener(menuSelected);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MenuPrincipalInteractionListener) {
            mListener = (MenuPrincipalInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface MenuPrincipalInteractionListener {
        // TODO: Update argument type and name
        void menuPrincipalEventListener(String menuSelected);
    }
}
