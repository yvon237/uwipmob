package com.example.yvonzobo.uwipmob.utility;

/**
 * Created by Yvon ZOBO on 03/08/2016.
 */
public class Urls {



    public static final String loginAgenturl = Utility.ADRESS_SERVER+"entities.agent/dologin";
    public static final String updatepasswordAgenturl = Utility.ADRESS_SERVER+"entities.agent/updatepassword";


    public static final String addclienturl = Utility.ADRESS_SERVER+"entities.client/addclient";

    public static final String getallcustomersurl = Utility.ADRESS_SERVER+"entities" + ".client/getAllClient";
    public static final   String updatecustomerurl =Utility.ADRESS_SERVER+ "entities" + ".client/upDateClient";
    public static final String getcountcustomerurl = Utility.ADRESS_SERVER+ "entities" + ".client/countClient";














    public static final   String getallcontracturl =Utility.ADRESS_SERVER+ "entities.contract/getAllContract";

    public static final   String getallcountrylayerurl =Utility.ADRESS_SERVER+ "entities.countrylayer/getAllCountryLayer";

    public static final   String getallhardversionurl =Utility.ADRESS_SERVER+ "entities.hardvers/getAllHardVers";

    public static final   String getallboxurl =Utility.ADRESS_SERVER+ "entities.box/getAllBox";

    public static final   String getalltownlayerurl =Utility.ADRESS_SERVER+ "entities.townlayer/getAllTownLayer";

    public static final   String getallsoftversionurl =Utility.ADRESS_SERVER+ "entities.softvers/getAllSoftVers";

    public static final  String savemmoperatorurl =Utility.ADRESS_SERVER+"entities" + ".mmoperator/addmmOperator";
    public static final  String getallmmoperatorurl =Utility.ADRESS_SERVER+"entities" + "" + ".mmoperator/GetAllmmOperator";
    public static final  String updatemmoperatorurl =Utility.ADRESS_SERVER+"entities" + "" + "" + ".mmoperator/upDateOperator";
}
