package com.example.yvonzobo.uwipmob.utility;

/**
 * Created by Yvon ZOBO on 24/08/2016.
 */
public class PostCustomer {
    private String nom;
    private String prenom;
    private String localite;
    private String village;

    public PostCustomer() {
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getNom() {
        return nom;
    }


    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLocalite() {
        return localite;
    }

    public void setLocalite(String localite) {
        this.localite = localite;
    }
}
