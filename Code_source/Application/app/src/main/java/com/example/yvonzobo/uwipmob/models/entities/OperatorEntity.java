package com.example.yvonzobo.uwipmob.models.entities;

/**
 * Created by Yvon ZOBO on 29/07/2016.
 */
public class OperatorEntity {

    private Long idoperator;
    private String code;


    public OperatorEntity(){

    }

    public Long getIdoperator() {
        return idoperator;
    }

    public void setIdoperator(Long idoperator) {
        this.idoperator = idoperator;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
