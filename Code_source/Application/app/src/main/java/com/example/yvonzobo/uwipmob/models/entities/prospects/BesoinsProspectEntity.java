package com.example.yvonzobo.uwipmob.models.entities.prospects;

/**
 * Created by Yvon ZOBO on 30/09/2016.
 */
public class BesoinsProspectEntity {

    private int totallampe;
    private int totalradio;
    private int totaltorche;
    private String modepaiement; // comptant ou credit

    public BesoinsProspectEntity() {

    }

    public int getTotallampe() {
        return totallampe;
    }

    public void setTotallampe(int totallampe) {
        this.totallampe = totallampe;
    }

    public int getTotalradio() {
        return totalradio;
    }

    public void setTotalradio(int totalradio) {
        this.totalradio = totalradio;
    }

    public int getTotaltorche() {
        return totaltorche;
    }

    public void setTotaltorche(int totaltorche) {
        this.totaltorche = totaltorche;
    }

    public String getModepaiement() {
        return modepaiement;
    }

    public void setModepaiement(String modepaiement) {
        this.modepaiement = modepaiement;
    }
}
