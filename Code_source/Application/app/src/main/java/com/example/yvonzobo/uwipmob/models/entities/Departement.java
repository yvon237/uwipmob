package com.example.yvonzobo.uwipmob.models.entities;

/**
 * Created by Yvon ZOBO on 11/10/2016.
 */
public class Departement {

    private Long iddepartement;
    private String libelle_departement;


    public Departement() {

    }

    public Long getIddepartement() {
        return iddepartement;
    }

    public void setIddepartement(Long iddepartement) {
        this.iddepartement = iddepartement;
    }

    public String getLibelle_departement() {
        return libelle_departement;
    }

    public void setLibelle_departement(String libelle_departement) {
        this.libelle_departement = libelle_departement;
    }
}
