package com.example.yvonzobo.uwipmob.models.entities;

/**
 * Created by Yvon ZOBO on 24/08/2016.
 */
public class PostProspectList {
    private String nom;
    private String prenom;
    private String localite;
    private String village;

    public PostProspectList() {
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getNom() {
        return nom;
    }


    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLocalite() {
        return localite;
    }

    public void setLocalite(String localite) {
        this.localite = localite;
    }
}
