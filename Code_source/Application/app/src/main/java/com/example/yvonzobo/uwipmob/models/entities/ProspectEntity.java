package com.example.yvonzobo.uwipmob.models.entities;

/**
 * Created by Yvon ZOBO on 25/07/2016.
 */
public class ProspectEntity {

    private Long idclientlocal;
    private Long idclient;
    private Long idoperator;
    private String firstname;
    private String lastname;
    private int clientuniqnumber;
    private String nationality;
    private int totalbox;
    private String preferredphone;
    private String phoneorange;
    private String phonemtn;
    private String phonenexttel;
    private String acquisitdate;
    private String signedistinctif;
    private String firstcontact;
    private String mobilemoney;
    private String municipality;
    private String village;
    private String cnipic;


    public ProspectEntity() {
    }

    public Long getIdclientlocal() {
        return idclientlocal;
    }

    public void setIdclientlocal(Long idclientlocal) {
        this.idclientlocal = idclientlocal;
    }

    public Long getIdoperator() {
        return idoperator;
    }

    public void setIdoperator(Long idoperator) {
        this.idoperator = idoperator;
    }

    public String getMobilemoney() {
        return mobilemoney;
    }

    public void setMobilemoney(String mobilemoney) {
        this.mobilemoney = mobilemoney;
    }

    public Long getIdclient() {
        return idclient;
    }

    public void setIdclient(Long idclient) {
        this.idclient = idclient;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getClientuniqnumber() {
        return clientuniqnumber;
    }

    public void setClientuniqnumber(int clientuniqnumber) {
        this.clientuniqnumber = clientuniqnumber;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public int getTotalbox() {
        return totalbox;
    }

    public void setTotalbox(int totalbox) {
        this.totalbox = totalbox;
    }

    public String getPreferredphone() {
        return preferredphone;
    }

    public void setPreferredphone(String preferredphone) {
        this.preferredphone = preferredphone;
    }

    public String getPhoneorange() {
        return phoneorange;
    }

    public void setPhoneorange(String phoneorange) {
        this.phoneorange = phoneorange;
    }

    public String getPhonemtn() {
        return phonemtn;
    }

    public void setPhonemtn(String phonemtn) {
        this.phonemtn = phonemtn;
    }

    public String getPhonenexttel() {
        return phonenexttel;
    }

    public void setPhonenexttel(String phonenexttel) {
        this.phonenexttel = phonenexttel;
    }

    public String getCnipic() {
        return cnipic;
    }

    public void setCnipic(String cnipic) {
        this.cnipic = cnipic;
    }

    public String getAcquisitdate() {
        return acquisitdate;
    }

    public void setAcquisitdate(String acquisitdate) {
        this.acquisitdate = acquisitdate;
    }

    public String getSignedistinctif() {
        return signedistinctif;
    }

    public void setSignedistinctif(String signedistinctif) {
        this.signedistinctif = signedistinctif;
    }

    public String getFirstcontact() {
        return firstcontact;
    }

    public void setFirstcontact(String firstcontact) {
        this.firstcontact = firstcontact;
    }


    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }
}
