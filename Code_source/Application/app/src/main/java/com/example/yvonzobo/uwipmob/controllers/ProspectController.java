package com.example.yvonzobo.uwipmob.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.example.yvonzobo.uwipmob.models.database.ContentDb;
import com.example.yvonzobo.uwipmob.models.database.DbHelper;
import com.example.yvonzobo.uwipmob.models.entities.PostProspectList;
import com.example.yvonzobo.uwipmob.models.entities.ProspectEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yvon ZOBO on 29/07/2016.
 */
public class ProspectController {

    public static final String CREATE_TABLE_PROSPECT = "CREATE TABLE "+
            ContentDb.TABLE_PROSPECT+" ("+
            ContentDb.COLUMM_ID_CLIENT_LOCALE+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            ContentDb.COLUMM_ID_CLIENT+" INTEGER, "+
            ContentDb.COLUMM_ID_OPERATOR+" INTEGER,"+
            ContentDb.COLUMM_FIRSTNAME+" TEXT, "+
            ContentDb.COLUMM_LASTNAME+" TEXT, "+
            ContentDb.COLUMM_CLIENTUNIQNUMBER+" INTEGER,"+
            ContentDb.COLUMM_NATIONALITY+" TEXT, "+
            ContentDb.COLUMM_TOTAL_BOX+" INTEGER, "+
            ContentDb.COLUMM_PREFERRED_PHONE+" TEXT,"+
            ContentDb.COLUMM_PHONE_ORANGE+" TEXT,"+
            ContentDb.COLUMM_PHONE_MTN+" TEXT,"+
            ContentDb.COLUMM_PHONE_NEXTTEL+" TEXT,"+
            ContentDb.COLUMM_CNIPIC+" TEXT,"+
            ContentDb.COLUMM_ACQUISITDATE+" TEXT,"+
            ContentDb.COLUMM_SIGNEDISTINCTIF+" TEXT,"+
            ContentDb.COLUMM_FIRST_CONTACT+" TEXT,"+
            ContentDb.COLUMM_MOBILE_MONEY+" TEXT,"+
            ContentDb.COLUMM_VILLAGE+" TEXT,"+
            "FOREIGN KEY("+ContentDb.COLUMM_ID_OPERATOR+") REFERENCES "+ContentDb.TABLE_OPERATOR+"("+ContentDb.COLUMM_ID_OPERATOR+"))";
        private Context context;
        private DbHelper db;
        private SQLiteDatabase mydb;

    public ProspectController(Context context){
        db = new DbHelper(context);
    }
    public SQLiteDatabase open(){

        mydb = db.getWritableDatabase();
        return mydb;
    }

    /**
     * ferme la base de données
     */
    public void close(){
        mydb.close();
    }

    public SQLiteDatabase getMydb(){
        return mydb;
    }



    public boolean createCustomer(ProspectEntity customerEntityList){

        long result;

      
            ContentValues values = new ContentValues();
            values.put(ContentDb.COLUMM_ID_CLIENT_LOCALE,customerEntityList.getIdclientlocal());
            values.put(ContentDb.COLUMM_ID_CLIENT,customerEntityList.getIdclient());
            values.put(ContentDb.COLUMM_ID_OPERATOR,customerEntityList.getIdoperator());
            values.put(ContentDb.COLUMM_FIRSTNAME,customerEntityList.getFirstname());
            values.put(ContentDb.COLUMM_LASTNAME,customerEntityList.getLastname());
            values.put(ContentDb.COLUMM_CLIENTUNIQNUMBER,customerEntityList.getClientuniqnumber());
            values.put(ContentDb.COLUMM_NATIONALITY,customerEntityList.getNationality());
            values.put(ContentDb.COLUMM_TOTAL_BOX,customerEntityList.getTotalbox());
            values.put(ContentDb.COLUMM_PREFERRED_PHONE,customerEntityList.getPreferredphone());
            values.put(ContentDb.COLUMM_PHONE_ORANGE,customerEntityList.getPhoneorange());
            values.put(ContentDb.COLUMM_PHONE_MTN,customerEntityList.getPhonemtn());
            values.put(ContentDb.COLUMM_PHONE_NEXTTEL,customerEntityList.getPhonenexttel());
            values.put(ContentDb.COLUMM_CNIPIC,customerEntityList.getCnipic());
            values.put(ContentDb.COLUMM_SIGNEDISTINCTIF,customerEntityList.getSignedistinctif());
            values.put(ContentDb.COLUMM_FIRST_CONTACT,customerEntityList.getFirstcontact());
            values.put(ContentDb.COLUMM_MOBILE_MONEY,customerEntityList.getMobilemoney());

            values.put(ContentDb.COLUMM_VILLAGE,customerEntityList.getVillage());
            result = open().insert(ContentDb.TABLE_PROSPECT,null,values);

      
        if (result==-1)
            return false;
        else return true;
    }

    public List<ProspectEntity> getAllCustomers(){

        Cursor cursor = open().rawQuery("select * from prospect",null);
        if (cursor.getCount()==0)
            return new ArrayList<>(0);
        else
            return cursorToCustomers(cursor);


    }

    public List<PostProspectList> getPostCustomer(){
        Cursor cursor = open().query(ContentDb.TABLE_PROSPECT,new String[]{ContentDb
                .COLUMM_LASTNAME,ContentDb.COLUMM_FIRSTNAME,
                ContentDb.COLUMM_VILLAGE},null,
                null,null,null,
                ContentDb
                .COLUMM_LASTNAME);

        if (cursor.getCount()==0)
            return  new ArrayList<>(0);
        else
            return  cursorToPostCustomer(cursor);


    }
    public List<PostProspectList> cursorToPostCustomer(Cursor cursor){
        List<PostProspectList> list = new ArrayList<>(cursor.getCount());
        cursor.moveToFirst();
        do {
            PostProspectList entity = new PostProspectList();
            entity.setNom(cursor.getString(0));
            entity.setPrenom(cursor.getString(1));
            entity.setLocalite(cursor.getString(2));
            entity.setVillage(cursor.getString(3));

            list.add(entity);

        }while (cursor.moveToNext());

        cursor.close();

        return list;
    }



    public List<ProspectEntity> cursorToCustomers(Cursor cursor){

        List<ProspectEntity> entityList = new ArrayList<>();
        cursor.moveToFirst();

        do {
            ProspectEntity entity = new ProspectEntity();
            entity.setIdclientlocal(cursor.getLong(0));
            entity.setIdclient(cursor.getLong(1));
            entity.setIdoperator(cursor.getLong(2));
            entity.setFirstname(cursor.getString(3));
            entity.setLastname(cursor.getString(4));
            entity.setClientuniqnumber(cursor.getInt(5));
            entity.setNationality(cursor.getString(6));
            entity.setTotalbox(cursor.getInt(7));
            entity.setPreferredphone(cursor.getString(8));
            entity.setPhoneorange(cursor.getString(9));
            entity.setPhonemtn(cursor.getString(10));
            entity.setPhonenexttel(cursor.getString(11));
            entity.setCnipic(cursor.getString(12));
            entity.setAcquisitdate(cursor.getString(13));
            entity.setSignedistinctif(cursor.getString(14));
            entity.setFirstcontact(cursor.getString(15));
            entity.setMobilemoney(cursor.getString(16));
            entity.setMunicipality(cursor.getString(17));
            entity.setVillage(cursor.getString(18));

            entityList.add(entity);


        }while (cursor.moveToNext());
        cursor.close();

        return entityList;
    }

    public  int getCount(){

        try {
            Cursor cursor = open().rawQuery("select count(idclient) from prospect",null);
            cursor.moveToFirst();
            int total = cursor.getInt(0);




            return total;

        }catch (Exception e){
            e.getMessage();
            return 0;
        }



    }
    public boolean getCustomerById(Long id){
        Cursor cursor = open().rawQuery("select idclient from prospect where idclient = ?", new
                String[]{String.valueOf(id)});
        if (cursor.getCount()==0)
            return false;
        else return true;
    }
    public boolean getCustomerByLastname(String lastname){
        Cursor cursor = open().rawQuery("select lastname from prospect where lastname = ?",new
                String[]{lastname});

        if (cursor.getCount()==0)
            return false;
        else
            return true;

    }



    public int getIdclient(String lastname, String firstname){
        Cursor cursor = open().rawQuery("select idclient from prospect where lastname = ? and " + "firstname = ?",new String[]{lastname,firstname});

      //  Cursor cursor = open().query(ContentDb.TABLE_CLIENT,new String[]{ContentDb.COLUMM_ID_CLIENT},null,new String[]{lastname,firstname},null,null,null,null);
        cursor.moveToFirst();


        return cursor.getInt(0);
    }
    public int getIdClientLocal(String lastname,String firstname) {
        Cursor cursor = open().rawQuery("select idclientlocal from prospect where lastname = ? and " + "firstname = ?",new String[]{lastname,firstname});

        //  Cursor cursor = open().query(ContentDb.TABLE_CLIENT,new String[]{ContentDb.COLUMM_ID_CLIENT},null,new String[]{lastname,firstname},null,null,null,null);
        cursor.moveToFirst();


        return cursor.getInt(0);
    }

    public ProspectEntity getCustomerLastnameFirstname(String lastname,String firstname){
        Cursor cursor = open().rawQuery("select * from prospect where lastname = ? and " + "firstname= ?",new String[]{lastname,firstname});

        return  cursorToCustomer(cursor);

    }

    public boolean upDateCustomer(ProspectEntity customer,Long id){

        ContentValues values = new ContentValues();


            values.put(ContentDb.COLUMM_ID_OPERATOR,customer.getIdoperator());
            values.put(ContentDb.COLUMM_FIRSTNAME,customer.getFirstname());
            values.put(ContentDb.COLUMM_LASTNAME,customer.getLastname());
            values.put(ContentDb.COLUMM_CLIENTUNIQNUMBER,customer.getClientuniqnumber());
            values.put(ContentDb.COLUMM_NATIONALITY,customer.getNationality());
            values.put(ContentDb.COLUMM_TOTAL_BOX,customer.getTotalbox());
            values.put(ContentDb.COLUMM_PREFERRED_PHONE,customer.getPreferredphone());
            values.put(ContentDb.COLUMM_PHONE_ORANGE,customer.getPhoneorange());
            values.put(ContentDb.COLUMM_PHONE_MTN,customer.getPhonemtn());
            values.put(ContentDb.COLUMM_PHONE_NEXTTEL,customer.getPhonenexttel());
            values.put(ContentDb.COLUMM_CNIPIC,customer.getCnipic());
            values.put(ContentDb.COLUMM_ACQUISITDATE,customer.getAcquisitdate());
            values.put(ContentDb.COLUMM_SIGNEDISTINCTIF,customer.getSignedistinctif());
            values.put(ContentDb.COLUMM_FIRST_CONTACT,customer.getFirstcontact());
            values.put(ContentDb.COLUMM_MOBILE_MONEY,customer.getMobilemoney());

            values.put(ContentDb.COLUMM_VILLAGE,customer.getVillage());


            return  open().update(ContentDb.TABLE_PROSPECT,values,ContentDb.COLUMM_ID_CLIENT_LOCALE+" = " + ""+id,null)>0;

    }

    public boolean upDateCustomerDistant_Locale(ProspectEntity customer,Long id){

        ContentValues values = new ContentValues();


        values.put(ContentDb.COLUMM_ID_OPERATOR,customer.getIdoperator());
        values.put(ContentDb.COLUMM_FIRSTNAME,customer.getFirstname());
        values.put(ContentDb.COLUMM_LASTNAME,customer.getLastname());
        values.put(ContentDb.COLUMM_CLIENTUNIQNUMBER,customer.getClientuniqnumber());
        values.put(ContentDb.COLUMM_NATIONALITY,customer.getNationality());
        values.put(ContentDb.COLUMM_TOTAL_BOX,customer.getTotalbox());
        values.put(ContentDb.COLUMM_PREFERRED_PHONE,customer.getPreferredphone());
        values.put(ContentDb.COLUMM_PHONE_ORANGE,customer.getPhoneorange());
        values.put(ContentDb.COLUMM_PHONE_MTN,customer.getPhonemtn());
        values.put(ContentDb.COLUMM_PHONE_NEXTTEL,customer.getPhonenexttel());
        values.put(ContentDb.COLUMM_CNIPIC,customer.getCnipic());
        values.put(ContentDb.COLUMM_ACQUISITDATE,customer.getAcquisitdate());
        values.put(ContentDb.COLUMM_SIGNEDISTINCTIF,customer.getSignedistinctif());
        values.put(ContentDb.COLUMM_FIRST_CONTACT,customer.getFirstcontact());
        values.put(ContentDb.COLUMM_MOBILE_MONEY,customer.getMobilemoney());

        values.put(ContentDb.COLUMM_VILLAGE,customer.getVillage());


        return  open().update(ContentDb.TABLE_PROSPECT,values,ContentDb.COLUMM_ID_CLIENT+" = " + ""+id,null)>0;

    }

    public boolean upDateCustomer(ProspectEntity customer,String lastname,String firstname){

        ContentValues values = new ContentValues();


        values.put(ContentDb.COLUMM_ID_OPERATOR,customer.getIdoperator());
        values.put(ContentDb.COLUMM_ID_CLIENT, customer.getIdclient());
        values.put(ContentDb.COLUMM_FIRSTNAME,customer.getFirstname());
        values.put(ContentDb.COLUMM_LASTNAME,customer.getLastname());
        values.put(ContentDb.COLUMM_CLIENTUNIQNUMBER,customer.getClientuniqnumber());
        values.put(ContentDb.COLUMM_NATIONALITY,customer.getNationality());
        values.put(ContentDb.COLUMM_TOTAL_BOX,customer.getTotalbox());
        values.put(ContentDb.COLUMM_PREFERRED_PHONE,customer.getPreferredphone());
        values.put(ContentDb.COLUMM_PHONE_ORANGE,customer.getPhoneorange());
        values.put(ContentDb.COLUMM_PHONE_MTN,customer.getPhonemtn());
        values.put(ContentDb.COLUMM_PHONE_NEXTTEL,customer.getPhonenexttel());
        values.put(ContentDb.COLUMM_CNIPIC,customer.getCnipic());
        values.put(ContentDb.COLUMM_ACQUISITDATE,customer.getAcquisitdate());
        values.put(ContentDb.COLUMM_SIGNEDISTINCTIF,customer.getSignedistinctif());
        values.put(ContentDb.COLUMM_FIRST_CONTACT,customer.getFirstcontact());
        values.put(ContentDb.COLUMM_MOBILE_MONEY,customer.getMobilemoney());

        values.put(ContentDb.COLUMM_VILLAGE,customer.getVillage());


        return  open().update(ContentDb.TABLE_PROSPECT,values,ContentDb.COLUMM_LASTNAME+" =? " +" " + "AND "+ContentDb.COLUMM_FIRSTNAME+" =? ", new String[]{lastname,firstname})>0;

    }

    private ProspectEntity cursorToCustomer(Cursor cursor){
        ProspectEntity entity = new ProspectEntity();

        cursor.moveToFirst();

        entity.setIdclientlocal(cursor.getLong(0));
        entity.setIdclient(cursor.getLong(1));
        entity.setIdoperator(cursor.getLong(2));
        entity.setFirstname(cursor.getString(3));
        entity.setLastname(cursor.getString(4));
        entity.setClientuniqnumber(cursor.getInt(5));
        entity.setNationality(cursor.getString(6));
        entity.setTotalbox(cursor.getInt(7));
        entity.setPreferredphone(cursor.getString(8));
        entity.setPhoneorange(cursor.getString(9));
        entity.setPhonemtn(cursor.getString(10));
        entity.setPhonenexttel(cursor.getString(11));
        entity.setCnipic(cursor.getString(12));
        entity.setAcquisitdate(cursor.getString(13));
        entity.setSignedistinctif(cursor.getString(14));
        entity.setFirstcontact(cursor.getString(15));
        entity.setMobilemoney(cursor.getString(16));
        entity.setMunicipality(cursor.getString(17));
        entity.setVillage(cursor.getString(18));

        cursor.close();

        return entity;

    }



    public boolean verifIfcustomerExist(String nom,String prenom){
        Cursor cursor = open().rawQuery("select lastname,firstname from prospect where lastname=? " +
                "and firstname =?",new String[]{nom,prenom});


        if (cursor.getCount()==0)
            return false;
        else return  true;
    }
}
