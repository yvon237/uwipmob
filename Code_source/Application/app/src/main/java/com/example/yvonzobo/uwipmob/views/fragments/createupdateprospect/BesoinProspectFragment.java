package com.example.yvonzobo.uwipmob.views.fragments.createupdateprospect;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.yvonzobo.uwipmob.R;


public class BesoinProspectFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private TextView l_lampe,l_radio,l_torche,l_modepaiement;
    private TextView t_lampe,t_radio,t_torche,t_modepaiement;
    private LinearLayout layoutlampe,layoutradio,layouttorche,layoutmodepaiement;
    private RadioButton radio3lampe, radio4lampe, comptant,credit;
    private RadioGroup radioGroup;
    private  AlertDialog.Builder builder;
    private  EditText editText;
    private ImageView b_next;
    public BesoinProspectFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static BesoinProspectFragment newInstance(String param1, String param2) {
        BesoinProspectFragment fragment = new BesoinProspectFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_besoin_prospect, container, false);



                 l_lampe = (TextView)view.findViewById(R.id.l_lampe);
                l_radio  = (TextView)view.findViewById(R.id.l_radio);
                l_torche  = (TextView)view.findViewById(R.id.l_torche);
                l_modepaiement  = (TextView)view.findViewById(R.id.l_modepaiement);



                  t_lampe  = (TextView)view.findViewById(R.id.t_lampe);
                  t_radio  = (TextView)view.findViewById(R.id.t_radio);
                  t_torche  = (TextView)view.findViewById(R.id.t_torche);
                  t_modepaiement  = (TextView)view.findViewById(R.id.t_modepaiement);


                  layoutlampe  = (LinearLayout) view.findViewById(R.id.layoutlampe);
                  layoutradio  = (LinearLayout) view.findViewById(R.id.layoutradio);
                  layouttorche  = (LinearLayout) view.findViewById(R.id.layouttorche);
                  layoutmodepaiement  = (LinearLayout) view.findViewById(R.id.layoutmodepaiement);


                b_next = (ImageView)view.findViewById(R.id.b_next);
        return view;
    }

    public void onClickBNext() {
        b_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void onClickLayoutLampe() {
        layoutlampe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogLampe(l_lampe);
            }
        });
    }

    public void onClickLayoutRadio() {
        layoutradio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogRadio(l_radio);
            }
        });
    }

    public void onClickLayoutTorche() {
        layouttorche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogTorche(l_torche);
            }
        });
    }
    public void onClickLayoutModePaiement() {
        onCreateDialogModePaiment(l_modepaiement);
    }

    private Dialog onCreateDialogRadio(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText.setHint("nombre radio souhaitez");

        builder.setView(editText);


        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonRadioClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeRadioButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonRadioClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            final String valeur = editText.getText().toString();
            t_radio.setText(valeur);



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeRadioButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }




    private Dialog onCreateDialogTorche(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText.setHint("nombre torche souhaitez");

        builder.setView(editText);


        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonTorcheClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeTorcheButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonTorcheClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            final String valeur = editText.getText().toString();
            t_torche.setText(valeur);



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeTorcheButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    private Dialog onCreateDialogModePaiment(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());

        LinearLayout layoutRadioGroup = new LinearLayout(getActivity());
        layoutRadioGroup.setOrientation(LinearLayout.HORIZONTAL);

        radioGroup = new RadioGroup(getActivity());
        comptant = new RadioButton(getActivity());
        comptant.setId(RadioButton.generateViewId());
        comptant.setText("Comptant");
        credit = new RadioButton(getActivity());
        credit.setId(RadioButton.generateViewId());
        credit.setText("Credit");





        radioGroup.addView(credit);
        radioGroup.addView(comptant);

        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        layoutRadioGroup.addView(radioGroup);


        builder.setView(layoutRadioGroup);


        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonModePaiementClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeModePaiementButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonModePaiementClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            if (radioGroup.getCheckedRadioButtonId() == comptant.getId()) {

                t_modepaiement.setText("Comptant");
            } else if (radioGroup.getCheckedRadioButtonId() == credit.getId()) {
                t_modepaiement.setText("Credit");
            }

            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeModePaiementButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }




    private Dialog onCreateDialogLampe(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());

        LinearLayout layoutRadioGroup = new LinearLayout(getActivity());
        layoutRadioGroup.setOrientation(LinearLayout.HORIZONTAL);

        radioGroup = new RadioGroup(getActivity());
        radio3lampe = new RadioButton(getActivity());
        radio3lampe.setId(RadioButton.generateViewId());
        radio3lampe.setText("3 Lampes");
        radio4lampe = new RadioButton(getActivity());
        radio4lampe.setId(RadioButton.generateViewId());
        radio4lampe.setText("4 Lampes");





        radioGroup.addView(radio3lampe);
        radioGroup.addView(radio4lampe);

        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        layoutRadioGroup.addView(radioGroup);


        builder.setView(layoutRadioGroup);


        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonLampeClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeLampeButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonLampeClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            int val = 4;
            if (radioGroup.getCheckedRadioButtonId() == radio3lampe.getId()) {
                val=3;
                t_lampe.setText(String.valueOf(val));
            } else if (radioGroup.getCheckedRadioButtonId() == radio4lampe.getId()) {
                val=4;
                t_lampe.setText(String.valueOf(val));
            }

            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeLampeButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
