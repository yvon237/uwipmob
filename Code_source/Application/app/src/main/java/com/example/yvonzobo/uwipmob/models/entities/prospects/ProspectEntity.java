package com.example.yvonzobo.uwipmob.models.entities.prospects;

import org.w3c.dom.Text;

/**
 * Created by Yvon ZOBO on 01/10/2016.
 */
public class ProspectEntity {

    private IdentiteProspectEntity identiteProspectEntity;
    private CoordonnesProspectEntity coordonnesProspectEntity;

    private BesoinsProspectEntity besoinsProspectEntity;
    private RessourcesProspectEntity ressourcesProspectEntity;
    private SolvabiliteProspectEntity solvabiliteProspectEntity;
    private SuiviProspectEntity suiviProspectEntity;




    public ProspectEntity() {

    }

    public IdentiteProspectEntity getIdentiteProspectEntity() {
        return identiteProspectEntity;
    }

    public void setIdentiteProspectEntity(IdentiteProspectEntity identiteProspectEntity) {
        this.identiteProspectEntity = identiteProspectEntity;
    }

    public CoordonnesProspectEntity getCoordonnesProspectEntity() {
        return coordonnesProspectEntity;
    }

    public void setCoordonnesProspectEntity(CoordonnesProspectEntity coordonnesProspectEntity) {
        this.coordonnesProspectEntity = coordonnesProspectEntity;
    }


    public BesoinsProspectEntity getBesoinsProspectEntity() {
        return besoinsProspectEntity;
    }

    public void setBesoinsProspectEntity(BesoinsProspectEntity besoinsProspectEntity) {
        this.besoinsProspectEntity = besoinsProspectEntity;
    }

    public RessourcesProspectEntity getRessourcesProspectEntity() {
        return ressourcesProspectEntity;
    }

    public void setRessourcesProspectEntity(RessourcesProspectEntity ressourcesProspectEntity) {
        this.ressourcesProspectEntity = ressourcesProspectEntity;
    }

    public SuiviProspectEntity getSuiviProspectEntity() {
        return suiviProspectEntity;
    }

    public void setSuiviProspectEntity(SuiviProspectEntity suiviProspectEntity) {
        this.suiviProspectEntity = suiviProspectEntity;
    }

    public SolvabiliteProspectEntity getSolvabiliteProspectEntity() {
        return solvabiliteProspectEntity;
    }

    public void setSolvabiliteProspectEntity(SolvabiliteProspectEntity solvabiliteProspectEntity) {
        this.solvabiliteProspectEntity = solvabiliteProspectEntity;
    }
}
