package com.example.yvonzobo.uwipmob.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


/**
 * Created by Yvon ZOBO on 20/07/2016.
 */
public abstract class CheckNetworkConnection {




    public static boolean  isOnline(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
           return  true;
        }else
            return false;

    }





}
