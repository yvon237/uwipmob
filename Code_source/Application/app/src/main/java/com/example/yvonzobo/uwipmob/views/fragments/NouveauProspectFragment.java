package com.example.yvonzobo.uwipmob.views.fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yvonzobo.uwipmob.R;
import com.example.yvonzobo.uwipmob.controllers.OperatorController;
import com.example.yvonzobo.uwipmob.controllers.ProspectController;
import com.example.yvonzobo.uwipmob.models.entities.ProspectEntity;
import com.example.yvonzobo.uwipmob.utility.Utility;

import java.util.Calendar;


public class NouveauProspectFragment extends Fragment implements AdapterView.OnItemSelectedListener{


    private NouveauProspectInteractionListener nouveauProspectInteractionListener;
    // textview pour les valeurs des differents champs de types text des dialog
    private TextView t_lastname,t_firstname,t_nationality,t_profession,t_firstcontact,
            t_mobilemoney,t_localite,t_phoneprefer,t_phoneorange,t_phonemtn,t_phonenexttel,t_operator,
            t_municipality,t_village,t_age,t_langue,t_zone,t_sexe,t_situation,t_region;
    // les textview des differents libelle
    private TextView l_lastname,l_firstname,l_nationality,l_phonenumber,l_profession,
            l_firstcontact,l_mobilemoney,l_localite,l_operator,l_age,l_langue,l_zone,l_sexe,l_situation;
    // les layout de chaque bloc on affecte l'evenement onclicklistener
    private LinearLayout layoutlastname,layoutfirstname,layoutnationality,layoutphonenumber,
            layoutprofession,layoutoperator,layoutfirstcontact,layoutmobilemoney,layoutlocalite,layoutage,layoutsexe,layoutlangue,layoutsituation,layoutzone;
    //la boite de dialog
    private  AlertDialog.Builder builder;
    // les editText se trouvant dans les differents boites de dialog
    private EditText editText,editText2,editText3,editText4;
    //textInputLayout utilisé pour le design
    private TextInputLayout textInputLayout,textInputLayout1,textInputLayout2,textInputLayout3;
    //button enregister au serveur et en local et button choix de l'image dans la gallery
    private TextView register;
    // spinner pour les selections des pays
    private Spinner spinner, spinnerzone,spinnerlangue;
    private String[] country ={"Cameroun","Congo","Gabon","Guinnée Equatoriale","Mali"};
    private String[] langue ={"Français","Anglais","Bilingue","Autres"};
    private String[] zone ={"Urbain","Péri-urbain","Rural"};

    // utiliser pour connaitre le numero preferer du client
    private RadioGroup radioGroup;
    private RadioButton radioyes,radiono,radioOrange,radioMtn,radioNexttel;
    // le bean client
    private ProspectEntity customerEntity = new ProspectEntity();
    // pour l'image de la cni
    private ImageView cnipic,cniview;
    private int mYear,mMonth,mDay;
    private ProspectController customerController;
    private ProgressDialog progressDialog;
    private OperatorController operatorController;
    private int contraleSpinner =0;

    public NouveauProspectFragment() {
        // Required empty public constructor
    }

    public static NouveauProspectFragment newInstance() {
        NouveauProspectFragment prospectFragment = new NouveauProspectFragment();

        return prospectFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view= inflater.inflate(R.layout.fragment_nouveau_prospect, container, false);


        t_lastname = (TextView)view.findViewById(R.id.t_lastname);
        t_firstname = (TextView)view.findViewById(R.id.t_firstname);
        t_nationality = (TextView)view.findViewById(R.id.t_nationality);


        t_profession = (TextView)view.findViewById(R.id.t_profession);
        t_firstcontact = (TextView)view.findViewById(R.id.t_firstcontact);
        t_mobilemoney = (TextView)view.findViewById(R.id.t_mobilemoney);

        t_phoneprefer  = (TextView)view.findViewById(R.id.t_phoneprefer);
        t_phoneorange = (TextView)view.findViewById(R.id.t_phoneorange);
        t_phonemtn = (TextView)view.findViewById(R.id.t_phonemtn);
        t_phonenexttel = (TextView)view.findViewById(R.id.t_phonenexttel);
        t_municipality = (TextView)view.findViewById(R.id.t_municipality);
        t_village= (TextView)view.findViewById(R.id.t_village);
        t_operator = (TextView)view.findViewById(R.id.t_operator);

        t_age = (TextView)view.findViewById(R.id.t_age);
        t_langue = (TextView)view.findViewById(R.id.t_langue);
        t_zone = (TextView)view.findViewById(R.id.t_zone);
        t_sexe = (TextView)view.findViewById(R.id.t_sexe);
        t_situation = (TextView)view.findViewById(R.id.t_situation);
        t_region = (TextView)view.findViewById(R.id.t_region);



        l_lastname = (TextView)view.findViewById(R.id.l_lastname);
        l_firstname = (TextView)view.findViewById(R.id.l_firstname);
        l_nationality = (TextView)view.findViewById(R.id.l_nationality);
        l_phonenumber = (TextView)view.findViewById(R.id.l_phonenumber);

        l_profession = (TextView)view.findViewById(R.id.l_profession);
        l_firstcontact = (TextView)view.findViewById(R.id.l_firstcontact);
        l_mobilemoney = (TextView)view.findViewById(R.id.l_mobilemoney);
        l_localite = (TextView)view.findViewById(R.id.l_localite);

        l_operator = (TextView)view.findViewById(R.id.l_operator);

        l_age = (TextView)view.findViewById(R.id.l_age);
        l_langue = (TextView)view.findViewById(R.id.l_langue);
        l_zone = (TextView)view.findViewById(R.id.l_zone);
        l_sexe = (TextView)view.findViewById(R.id.l_sexe);
        l_situation = (TextView)view.findViewById(R.id.l_situation);





        progressDialog = new ProgressDialog(getActivity());

        layoutlastname = (LinearLayout)view.findViewById(R.id.layoutnom);
        layoutfirstname = (LinearLayout)view.findViewById(R.id.layoutprenom);
        layoutnationality = (LinearLayout)view.findViewById(R.id.layoutnationality);
        layoutphonenumber = (LinearLayout)view.findViewById(R.id.layoutphonenumberr);

        layoutprofession = (LinearLayout)view.findViewById(R.id.layoutprofession);
        layoutfirstcontact = (LinearLayout)view.findViewById(R.id.layoutfirstcontact);
        layoutmobilemoney = (LinearLayout)view.findViewById(R.id.layoutmobilemoney);
        layoutlocalite = (LinearLayout)view.findViewById(R.id.layoutlocalite);

        layoutoperator =(LinearLayout)view.findViewById(R.id.layoutoperator);

        layoutage =(LinearLayout)view.findViewById(R.id.layoutage);
        layoutsexe =(LinearLayout)view.findViewById(R.id.layoutsexe);
        layoutlangue =(LinearLayout)view.findViewById(R.id.layoutlangue);
        layoutsituation =(LinearLayout)view.findViewById(R.id.layoutsituation);
        layoutzone =(LinearLayout)view.findViewById(R.id.layoutzone);

        register = (TextView)view.findViewById(R.id.register);

        customerController = new ProspectController(getActivity());
        operatorController = new OperatorController(getActivity());

        progressDialog = new ProgressDialog(getActivity());

        Calendar calendar= Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);

        onClickLayoutLastname();
        onClickLayoutFirstname();
        onClickLayoutNationality();
        onClickLayoutPhoneNumber();
        onClickLayoutProfession();
        onClickLayoutFirstContact();
        onClickLayoutMobileMoney();
        onClickLayoutLocalite();

        onClickLayoutOperator();

        onClickLayoutZone();
        onClickLayoutLangue();


        onClickRegister();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(boolean status) {
        if (nouveauProspectInteractionListener != null) {
            nouveauProspectInteractionListener.notifysuivifragrament(status);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NouveauProspectInteractionListener) {
            nouveauProspectInteractionListener = (NouveauProspectInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        nouveauProspectInteractionListener = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void onClickRegister(){
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // encodeImageToString();//envoi de l'iamge cni au serveur
                controllInputValue(getInputValueForm());


            }
        });
    }

    private  void onClickLayoutLastname(){
        layoutlastname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogLastname(l_lastname);
            }
        });
    }

    private  void onClickLayoutFirstname(){
        layoutfirstname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogFirstname(l_firstname);
            }
        });
    }
    private  void onClickLayoutNationality(){
        layoutnationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogNationality(l_nationality);
            }
        });
    }
    private  void onClickLayoutPhoneNumber(){
        layoutphonenumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogPhoneNumebers(l_phonenumber);
            }
        });
    }
    private void onClickLayoutOperator(){
        layoutoperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogOperateurMobile(l_operator);
            }
        });
    }
    private  void onClickLayoutProfession(){
        layoutprofession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogProfession(l_profession);
            }
        });
    }
    private  void onClickLayoutFirstContact(){
        layoutfirstcontact.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onCreateDialogFirstContact(l_firstcontact);





            }
        });
    }
    private  void onClickLayoutMobileMoney(){
        layoutmobilemoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogMobileMoney(l_mobilemoney);
            }
        });
    }
    private  void onClickLayoutLocalite(){
        layoutlocalite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogLocalite(l_localite);
            }
        });
    }

    private void onClickLayoutAge() {
        layoutage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void onClickLayoutSituationMatrimoniale() {
        layoutsituation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void onClickLayoutLangue() {
        layoutlangue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialogLangue(l_langue);
            }
        });
    }

    private void onClickLayoutSexe() {
        layoutsexe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void onClickLayoutZone() {
        layoutzone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                l_zone.setText("Zone du prospect");
                onCreateDialogZone(l_zone);
            }
        });
    }

    private Dialog onCreateDialogLastname(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);

        builder.setView(editText);

        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonLastnameClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeLastnameButtonClickListener());

        return builder.show();
    }




    private class OnPositiveButtonLastnameClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            String valeur = editText.getText().toString();
            t_lastname.setText(valeur);
            customerEntity.setLastname(valeur);


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeLastnameButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }







    private Dialog onCreateDialogFirstname(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);

        builder.setView(editText);

        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonFirstnameClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeFirstnameButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonFirstnameClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            final String valeur = editText.getText().toString();
            t_firstname.setText(valeur);
            customerEntity.setFirstname(valeur);


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeFirstnameButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }





    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (contraleSpinner == 1) {
            t_nationality.setText(country[i]);
        } else if (contraleSpinner == 2) {
            t_zone.setText(zone[i]);
        } else if (contraleSpinner == 3) {
            t_langue.setText(langue[i]);
        }




    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private Dialog onCreateDialogNationality(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        spinner = new Spinner(getActivity());
        spinner.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,country);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterType);

        contraleSpinner=1;



        builder.setView(spinner);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonNationalityClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeNationalityButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonNationalityClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeNationalityButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    private Dialog onCreateDialogZone(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        spinnerzone = new Spinner(getActivity());
        spinnerzone.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,zone);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerzone.setAdapter(adapterType);

        contraleSpinner =2;

        builder.setView(spinnerzone);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonZoneClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativezoneButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonZoneClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativezoneButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }


    private Dialog onCreateDialogLangue(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        spinnerlangue = new Spinner(getActivity());
        spinnerlangue.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,langue);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerlangue.setAdapter(adapterType);

        contraleSpinner =3;

        builder.setView(spinnerlangue);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonLangueClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeLangueButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonLangueClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeLangueButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }












    private Dialog onCreateDialogPhoneNumebers(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        textInputLayout = new TextInputLayout(getActivity());
        textInputLayout1 = new TextInputLayout(getActivity());
        textInputLayout2 = new TextInputLayout(getActivity());
        textInputLayout3 = new TextInputLayout(getActivity());
        LinearLayout  layout = new LinearLayout(getActivity());
        LinearLayout layoutRadioGroup = new LinearLayout(getActivity());
        ScrollView scrollView = new ScrollView(getActivity());

        TextView favorite = new TextView(getActivity());
        favorite.setText("Numéro preféré");
        favorite.setGravity(Gravity.CENTER);
        favorite.setTextSize(20);




        layoutRadioGroup.setOrientation(LinearLayout.HORIZONTAL);
        layout.setOrientation(LinearLayout.VERTICAL);

        radioGroup = new RadioGroup(getActivity());
        radioOrange = new RadioButton(getActivity());
        radioOrange.setId(RadioButton.generateViewId());
        radioOrange.setText("Orange");
        radioMtn = new RadioButton(getActivity());
        radioMtn.setId(RadioButton.generateViewId());
        radioMtn.setText("Mtn");

        radioNexttel = new RadioButton(getActivity());
        radioNexttel.setId(RadioButton.generateViewId());
        radioNexttel.setText("Nexttel");



        radioGroup.addView(radioOrange);
        radioGroup.addView(radioMtn);
        radioGroup.addView(radioNexttel);
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        layoutRadioGroup.addView(radioGroup);



        editText2 = new EditText(getActivity());
        editText2.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText2.setHint("Numéro Orange");

        editText3 = new EditText(getActivity());
        editText3.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText3.setHint("Numéro Mtn");


        editText4 = new EditText(getActivity());
        editText4.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText4.setHint("Numéro Nexttel");



        textInputLayout1.addView(editText2);
        textInputLayout2.addView(editText3);
        textInputLayout3.addView(editText4);



        layout.addView(textInputLayout1);
        layout.addView(textInputLayout2);
        layout.addView(textInputLayout3);
        layout.addView(favorite);
        layout.addView(layoutRadioGroup);


        scrollView.addView(layout);

        builder.setView(scrollView);

        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonPhoneNumberClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativePhoneNumberButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonPhoneNumberClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            String orange = editText2.getText().toString();
            String mtn = editText3.getText().toString();
            String nexttel = editText4.getText().toString();
            if (radioGroup.getCheckedRadioButtonId() == radioOrange.getId()){
                customerEntity.setPhoneorange(orange);
                customerEntity.setPreferredphone(orange);
                if (mtn.equals("")){
                    customerEntity.setPhonemtn("non");
                }else
                    customerEntity.setPhonemtn(mtn);
                if (nexttel.equals(""))
                    customerEntity.setPhonenexttel("non");
                else
                    customerEntity.setPhonenexttel(nexttel);

                t_phoneprefer.setText(customerEntity.getPreferredphone());
                t_phoneorange.setText(customerEntity.getPhoneorange());
                t_phonemtn.setText(customerEntity.getPhonemtn());
                t_phonenexttel.setText(customerEntity.getPhonenexttel());
            }else if (radioGroup.getCheckedRadioButtonId()== radioMtn.getId()){
                customerEntity.setPreferredphone(mtn);
                customerEntity.setPhonemtn(mtn);

                if (orange.equals("")){
                    customerEntity.setPhoneorange("non");
                }else
                    customerEntity.setPhoneorange(orange);
                if (nexttel.equals(""))
                    customerEntity.setPhonenexttel("non");
                else
                    customerEntity.setPhonenexttel(nexttel);

                t_phoneprefer.setText(customerEntity.getPreferredphone());
                t_phoneorange.setText(customerEntity.getPhoneorange());
                t_phonemtn.setText(customerEntity.getPhonemtn());
                t_phonenexttel.setText(customerEntity.getPhonenexttel());

            }else if (radioGroup.getCheckedRadioButtonId()== radioNexttel.getId()){
                customerEntity.setPreferredphone(nexttel);
                customerEntity.setPhonenexttel(nexttel);

                if (orange.equals("")){
                    customerEntity.setPhoneorange("non");
                }else
                    customerEntity.setPhoneorange(orange);
                if (mtn.equals(""))
                    customerEntity.setPhonemtn("non");
                else
                    customerEntity.setPhonemtn(nexttel);


                t_phoneprefer.setText(customerEntity.getPreferredphone());
                t_phoneorange.setText(customerEntity.getPhoneorange());
                t_phonemtn.setText(customerEntity.getPhonemtn());
                t_phonenexttel.setText(customerEntity.getPhonenexttel());

            }






            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativePhoneNumberButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }


    private Dialog onCreateDialogOperateurMobile(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        LinearLayout layoutRadioGroup = new LinearLayout(getActivity());
        layoutRadioGroup.setOrientation(LinearLayout.HORIZONTAL);

        radioGroup = new RadioGroup(getActivity());
        radioOrange = new RadioButton(getActivity());
        radioOrange.setId(RadioButton.generateViewId());
        radioOrange.setText("Orange");
        radioMtn = new RadioButton(getActivity());
        radioMtn.setId(RadioButton.generateViewId());
        radioMtn.setText("Mtn");

        radioNexttel = new RadioButton(getActivity());
        radioNexttel.setId(RadioButton.generateViewId());
        radioNexttel.setText("Nexttel");



        radioGroup.addView(radioOrange);
        radioGroup.addView(radioMtn);
        radioGroup.addView(radioNexttel);
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        layoutRadioGroup.addView(radioGroup);


        builder.setView(layoutRadioGroup);

        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonOperatorMobileClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeOperatorButtonMobileClickListener());

        return builder.show();
    }

    private class OnPositiveButtonOperatorMobileClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            if (radioGroup.getCheckedRadioButtonId() == radioOrange.getId()){
                t_operator.setText("ORANGE-CM");
            } else if (radioGroup.getCheckedRadioButtonId() == radioMtn.getId()) {
                t_operator.setText("MTN-CM");
            } else if (radioGroup.getCheckedRadioButtonId() == radioNexttel.getId()) {
                t_operator.setText("NEXTTEL-CM");
            }



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeOperatorButtonMobileClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }

    private Dialog onCreateDialogProfession(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);

        builder.setView(editText);

        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonProfessionClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeProfessionButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonProfessionClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            String valeur = editText.getText().toString();
            t_profession.setText(valeur);
            customerEntity.setSignedistinctif(valeur);


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeProfessionButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    private Dialog onCreateDialogFirstContact(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);




        builder.setView(editText);




        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonFirstContactClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeFirstContactButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonFirstContactClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            String valeur = editText.getText().toString();
            t_firstcontact.setText(valeur);
            customerEntity.setFirstcontact(valeur);


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeFirstContactButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }





    private Dialog onCreateDialogMobileMoney(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());

        radioGroup = new RadioGroup(getActivity());
        radioGroup.setId(RadioGroup.generateViewId());
        radioyes = new RadioButton(getActivity());
        radioyes.setId(RadioButton.generateViewId());
        radioyes.setText("oui");
        radiono = new RadioButton(getActivity());
        radiono.setText("Non");
        radiono.setId(RadioButton.generateViewId());
        LinearLayout layout = new LinearLayout(getActivity());

        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        radioGroup.addView(radioyes);
        radioGroup.addView(radiono);
        layout.addView(radioGroup);
        layout.setOrientation(LinearLayout.HORIZONTAL);

        builder.setView(layout);



        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonMobilemoneyClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeMobileMoneyButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonMobilemoneyClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            if (radioGroup.getCheckedRadioButtonId()== radioyes.getId())
                t_mobilemoney.setText("Oui");
            else if (radioGroup.getCheckedRadioButtonId()== radiono.getId())
                t_mobilemoney.setText("Non");


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeMobileMoneyButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    private Dialog onCreateDialogLocalite(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText2 = new EditText(getActivity());
        editText3 = new EditText(getActivity());
        editText3.setInputType(InputType.TYPE_CLASS_TEXT);
        editText3.setHint("Région");
        editText2.setInputType(InputType.TYPE_CLASS_TEXT);
        editText2.setHint("Ville ou Village");
        textInputLayout = new TextInputLayout(getActivity());
        textInputLayout1 = new TextInputLayout(getActivity());
        textInputLayout3 = new TextInputLayout(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setHint("Département");
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        textInputLayout3.addView(editText3);
        textInputLayout.addView(editText);
        textInputLayout1.addView(editText2);

        layout.addView(textInputLayout3);
        layout.addView(textInputLayout);
        layout.addView(textInputLayout1);

        builder.setView(layout);

        builder.setCancelable(false);




        builder.setTitle(labelle.getText());


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonLocaliteClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeLocaliteClickListener());

        return builder.show();
    }
    private class OnPositiveButtonLocaliteClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            t_municipality.setText(editText.getText().toString());
            t_village.setText(editText2.getText().toString());




            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeLocaliteClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    public void controllInputValue(ProspectEntity customerEntity){
        if (t_lastname.getText().equals("valeur") || !Utility.isNotNull(t_lastname.getText().toString())){
            Toast.makeText(getActivity(),"le nom est incorrect, 3 caractères au moins!",Toast
                    .LENGTH_LONG).show();
        }else if (t_firstname.getText().equals("valeur") || !Utility.isNotNull(t_firstname.getText().toString()))
            Toast.makeText(getActivity(),"prenom incorrect, 3 caractères au moins!",Toast
                    .LENGTH_LONG).show();
        else if (t_nationality.getText().equals("valeur"))
            Toast.makeText(getActivity(),"choisir une nationalité!",Toast.LENGTH_LONG).show();

        else if (t_phoneprefer.getText().equals("valeur") || t_phoneprefer.getText().equals(" ")
                || !Utility.isNotNull(t_phoneprefer.getText().toString())){
            Toast.makeText(getActivity(),"numéro permanent est obligatoire!!",Toast.LENGTH_LONG).show();

        }else if (t_operator.getText().equals("valeur") || !Utility.isNotNull(t_operator.getText()
                .toString())) {
            Toast.makeText(getActivity(),"choisir un operateur, obligatoire!",Toast.LENGTH_LONG).show();
        } else if (t_firstcontact.getText().equals("valeur") || !Utility.isNotNull(t_firstcontact.getText().toString())) {
            Toast.makeText(getActivity(),"préciser le premier contact!",Toast.LENGTH_LONG).show();
        } else if (t_municipality.getText().equals("valeur") || !Utility.isNotNull(t_municipality.getText().toString())) {
            Toast.makeText(getActivity(),"préciser la localité!",Toast.LENGTH_LONG).show();
        }else if (!customerController.verifIfcustomerExist(customerEntity.getLastname(),customerEntity.getFirstname())){
            onCreateDialogConfirmRegistration();
        }else
            onCreateDialogAlertDuplication();
    }

    /**
     * methode permettant d'enregistrer localement un client
     * @param customerEntity
     */
    public void addCustomerInLocalBD(ProspectEntity customerEntity){
        boolean result;

        result = customerController.createCustomer(customerEntity);

        if (result){
            Toast.makeText(getActivity(),"client ajouter avec succès",Toast.LENGTH_SHORT).show();
        }
        nouveauProspectInteractionListener.registerCustomer(true);

    }



    /**
     * extraction des valeurs entrees et formatage en un client
     * @return client
     */
    public ProspectEntity getInputValueForm(){

        customerEntity.setLastname(t_lastname.getText().toString());
        customerEntity.setFirstname(t_firstname.getText().toString());
        customerEntity.setNationality(t_nationality.getText().toString());
        customerEntity.setPreferredphone(t_phoneprefer.getText().toString());
        customerEntity.setPhoneorange(t_phoneorange.getText().toString());
        customerEntity.setPhonemtn(t_phonemtn.getText().toString());
        customerEntity.setPhonenexttel(t_phonenexttel.getText().toString());
        customerEntity.setFirstcontact(t_firstcontact.getText().toString());
        customerEntity.setSignedistinctif(t_profession.getText().toString());
        customerEntity.setMobilemoney(t_mobilemoney.getText().toString());
        customerEntity.setMunicipality(t_municipality.getText().toString());
        customerEntity.setVillage(t_village.getText().toString());
        // customerEntity.setIdclient(Long.valueOf(genereId(customerController.getCount())));
        customerEntity.setIdoperator(Long.valueOf(operatorController.getIdoperator(t_operator.getText().toString())));
        Log.i("total id", customerController.getCount()+"");

        return customerEntity;

    }

    private int genereId(int count){
        if (count<100){
            return   count+=54;
        }else
            return count+=1;
    }

    private Dialog onCreateDialogConfirmRegistration(){
        builder= new AlertDialog.Builder(getActivity());




        builder.setTitle("Nouveau client");
        builder.setIcon(getResources().getDrawable(R.drawable.alert));
        builder.setMessage("Confirmez-vous ces informations?");


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonConfirmationClickListener
                ());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeConfirmationButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonConfirmationClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            addCustomerInLocalBD(customerEntity);


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeConfirmationButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }


    private Dialog onCreateDialogAlertDuplication(){
        builder= new AlertDialog.Builder(getActivity());
        builder.setTitle("Duplication");
        builder.setIcon(getResources().getDrawable(R.drawable.alert));
        builder.setMessage("ce client est déjà enrégistré!!");
        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonConfirmClickListener
                ());
        return builder.show();
    }
    private class OnPositiveButtonConfirmClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }




    public interface NouveauProspectInteractionListener {
        void notifysuivifragrament(boolean status);
        void registerCustomer(boolean status);
    }

}
