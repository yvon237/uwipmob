package com.example.yvonzobo.uwipmob.views.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yvonzobo.uwipmob.R;
import com.example.yvonzobo.uwipmob.controllers.AgentController;
import com.example.yvonzobo.uwipmob.models.entities.AgentEntity;
import com.example.yvonzobo.uwipmob.utility.CheckNetworkConnection;
import com.example.yvonzobo.uwipmob.utility.ExtractJsonObject;
import com.example.yvonzobo.uwipmob.utility.ShaHash;
import com.example.yvonzobo.uwipmob.utility.Urls;
import com.example.yvonzobo.uwipmob.utility.Utility;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;


public class ChangePassFragment extends Fragment {

    private OnChangePassFragmentInteractionListener mListener;
    private String mlogin;
    private String mpassword;

    private EditText ancienpassword,newpassword,newlogin;
    private TextView b_appliquer;
    private AgentEntity agentEntity = new AgentEntity();
    private AgentController agentController;
    private ProgressDialog dialog;
    public ChangePassFragment() {
        // Required empty public constructor
    }


    public static ChangePassFragment newInstance(String login, String password) {
        ChangePassFragment fragment = new ChangePassFragment();
        Bundle args = new Bundle();
        args.putString("login",login);
        args.putString("password",password);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mlogin = getArguments().getString("login");
            mpassword = getArguments().getString("password");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_change_pass, container, false);


        ancienpassword = (EditText)view.findViewById(R.id.e_ancienpassword);
        newpassword = (EditText)view.findViewById(R.id.e_newpassword);
        newlogin = (EditText)view.findViewById(R.id.e_newlogin);

        b_appliquer = (TextView)view.findViewById(R.id.b_appliquer);

        agentController = new AgentController(getActivity());


        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Modification en cours patientez svp!");
        dialog.setCancelable(false);
        dialog.setIndeterminate(true);

        onclickBAppliquer();

        return view;
    }

    private void onclickBAppliquer() {
        b_appliquer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifierChampSaisie();
                if (!comparePassword(mpassword, newpassword.getText().toString())) {
                    Toast.makeText(getActivity(), "Aucun Changement effectué!", Toast.LENGTH_LONG).show();

                } else {
                    if (CheckNetworkConnection.isOnline(getActivity())) {
                        showConfirmationAlert();
                    }else
                        showCancelAlert();
                }
            }
        });
    }

    private boolean comparePassword(String ancienpassword,String newpassword) {
        if (ancienpassword.equals(new ShaHash().hash(newpassword)))
            return false;
        else
            return true;
    }


    private void verifierChampSaisie() {
        if (!Utility.isNotNull(newlogin.getText().toString())) {
            newlogin.setError("Mauvais login!");
        } else if (Utility.isNotNull(ancienpassword.getText().toString())) {
            if (!ancienpassword.getText().toString().equals(mpassword)) {
                ancienpassword.setError("mauvais mot de passe actuel!");
            }
        } else if (!Utility.isNotNull(ancienpassword.getText().toString())) {
            ancienpassword.setError("Merci renseigner le mot de passe actuel!");
        }
    }

    private void changePass(String url, RequestParams params) {
        AsyncHttpClient client = new AsyncHttpClient();

        client.put(getActivity(),url,params,new JsonHttpResponseHandler(){
            @Override
            public void onStart() {
                dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    if (response.getBoolean("status")) {
                        if (agentController.updatePassword(newlogin.getText().toString(), new ShaHash().hash(newpassword.getText().toString()), mpassword)) {

                            dialog.dismiss();
                            Toast.makeText(getActivity(),"Modification terminée avec succès!",Toast.LENGTH_LONG).show();
                        }

                    }else
                        Toast.makeText(getActivity(),response.getString("err_msg"),Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void showCancelAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("Synchronisation");

        // Setting Dialog Message
        alertDialog.setMessage("Désolé pas de connexion internet disponible!");
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton("Fermer", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.dismiss();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void showConfirmationAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("Synchronisation");

        // Setting Dialog Message
        alertDialog.setMessage("Confirmez-vous ces informations?");
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton("Oui", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog,int which) {

                RequestParams params = new RequestParams();
                params.put("newlogin",newlogin.getText().toString());
                params.put("newpassword",new ShaHash().hash(newpassword.getText().toString()));

                changePass(Urls.updatepasswordAgenturl,params);
                dialog.dismiss();
            }
        });

        alertDialog.setNegativeButton("Non", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void onButtonPressed(boolean nothing){
        if (mListener != null) {
            mListener.onChangePassFragmentCallInteraction(nothing);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChangePassFragmentInteractionListener) {
            mListener = (OnChangePassFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnChangePassFragmentInteractionListener {
        // TODO: Update argument type and name
        void onChangePassFragmentCallInteraction(boolean nothing);
    }
}
