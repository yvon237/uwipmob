package com.example.yvonzobo.uwipmob.models.database;

/**
 * Created by Yvon ZOBO on 20/07/2016.
 */
public abstract class ContentDb {



    public static  final  String TABLE_USER = "uuser";
    public static final String TABLE_PROFILE = "profile";
    public static final String TABLE_ORGANIZATION = "organization";
    public static final String TABLE_PROSPECT ="prospect";
    public static final String TABLE_OPERATOR = "operator";
    public static final String TABLE_CONTRACT = "contract";
    public static final String TABLE_AGENT = "agent";
    public static final String TABLE_CLIENT = "client";
    public static final String TABLE_REGION = "region";
    public static final String TABLE_DEPARTEMENT = "departement";
    public static final String TABLE_ARRONDISSEMENT="arrondissement";
    public static final String TABLE_VILLAGE = "village";



    public static final String  COLUMM_ID_USER = "iduuser";
    public static final String COLUMM_ID_PROFILE = "idprofile";
    public static final String COLUMM_ID_ORGANISATION = "idorganization";
    public static final String COLUMM_LASTNAME = "lastname";
    public static final String COLUMM_FIRSTNAME = "firstname";
    public static final String COLUMM_BIRTH = "birth";
    public static final String COLUMM_LOGIN = "login";
    public static final String COLUMM_PASSWORD = "mdp";

    public static final String COLUMM_TOTAL_BOX = "totalbox";

    public static final String COLUMM_ID_CLIENT_LOCALE = "idclientlocal";
    public static final String COLUMM_ID_CLIENT = "idclient";
    public static final String COLUMM_PHONE_ORANGE = "phoneorange";
    public static final String COLUMM_PHONE_MTN = "phonemtn";
    public static final String COLUMM_PHONE_NEXTTEL = "phonenexttel";
    public static final String COLUMM_FIRST_CONTACT = "firstcontact";
    public static final String COLUMM_PREFERRED_PHONE = "preferredphone";
    public static final String COLUMM_ID_OPERATOR = "idoperator";
    public static final String COLUMM_CODE ="code";
    public static final String COLUMM_SIGNEDISTINCTIF ="signedistinctif";
    public static final String COLUMM_CLIENTUNIQNUMBER = "clientuniqnumber";
    public static final String COLUMM_CNIPIC = "cnipic";
    public static final String COLUMM_ACQUISITDATE = "acquisitdate";
    public static final String COLUMM_TOTALBOX = "totalbox";
    public static final String COLUMM_MOBILE_MONEY = "mmdispo";
    public static final String COLUMM_STAUSINDIVIDU = "statusindividu";
    public static final String COLUMM_IDSEXE = "idsexe";
    public static final String COLUMM_IDAGE = "idage";
    public static final String COLUMM_IDLANGUE= "idlangue";
    public static final String COLUMM_COORDTYPEZONE= "coordtypezone";
    public static final String COLUMM_COORDSOURCING= "coordsourcing";
    public static final String COLUMM_BESOIN_TOTALLAMPE = "besointotallampe";
    public static final String COLUMM_BESOIN_TOTALRADIO= "besointotalradio";
    public static final String COLUMM_RSE_SATISFAIT_ENEO= "rsesatisfaiteneo";
    public static final String COLUMM_RSE_FACTUREMENSUELLE_ENEO = "rsefacturemensuelleeneo";
    public static final String COLUMM_ID_DEPARTEMENT ="iddepartement";
    public static final String COLUMM_DEPARTEMENT = "nomdepartement";
    public static final String COLUMM_ID_VILLAGE = "idvillage";
    public static final String COLUMM_VILLAGE = "nomvillage";

    public static final String COLUMM_ID_ARRONDISSEMENT = "idarrondissement";
    public static final String COLUMM_ARRONDISSEMENT ="nomarrondissement";

    public static final String COLUMM_ID_REGION = "idregion";
    public static final String COLUMM_REGION = "nomregion";

   /*les proprietes de la tablle agent*/
    public static final String COLUMM_ID_AGENT = "idagent";
   public static final String COLUMM_NOM = "nom";
    public static final String COLUMM_PRENOM = "prenom";
    public static final String COLUMM_typeagent = "typeagent";



/*les proprietes de la table prospect*/


/*besoins du prospect*/

    public static final String COLUMM_TOTALLAMPE = "totallampe";
    public static final String COLUMM_TOTALRADIO= "totalradio";
    public static final String COLUMM_TOTALTORCHE= "totaltorche";
    public static final String COLUMM_MODEPAIEMENT= "modepaiement";

/*coordonnees prospect*/


    public static final String COLUMM_NATIONALITY = "nationality";
    public static final String COLUMM_STATUT_INDIVIDU = "statutindividu";
    public static final String COLUMM_SOURCING = "sourcing";
    public static final String COLUMM_ACQUISIT_DATE = "acquisitdate";
    public static final String COLUMM_CLIENT_UNIQ_NUMBER = "clientuniqnumber";
    public static final String COLUMM_TYPE_ZONE = "typezone";

    /*resources prospect*/

    public static final String COLUMM_RSE_SATISFAITENEO = "rsesatisfaiteneo";
    public static final String COLUMM_RSE_FACTURE_MENSUELLE_ENEO= "rsefacturemensuelleeneo";
    public static final String COLUMM_RSE_GENERATEUR= "rsegenerateur";
    public static final String COLUMM_RSE_QTITE_HEBDO_GASOIL= "rseqtitehebdogasoil";
    public static final String COLUMM_RSE_PRIX_UNIT_GASOIL= "rseprixunitgasoil";
    public static final String COLUMM_RSE_EXTRAUTILS_GENERATOR ="rseextrautilsgenerator";
    public static final String COLUMM_RSE_LAMPE_PETROLE ="rselampepetrole";
    public static final String COLUMM_RSE_QTITE_HEBDO_PETROLE = "rseqtitehebdopetrole";
    public static final String COLUMM_RSE_PRIXUNIT_PETROLE = "rseprixunitpetrole";
    public static final String COLUMM_RSE_BOUGIE = "rsebougie";
    public static final String COLUMM_RSE_QTITEHEBDO_BOUGIE = "rseqtitehebdobougie";
    public static final String COLUMM_RSE_PRIXUNIT_BOUGIE = "rseprixunitbougie";
    public static final String COLUMM_RSE_PILE = "rsepile";
    public static final String COLUMM_RSE_QTITEHEBDO_PILE = "rseqtitehebdopile";
    public static final String COLUMM_RSE_PRIXUNIT_PILE = "rseprixunitpile";
    public static final String COLUMM_RSE_QTITE_PHONE_DISPO = "rseqtitephonedispo";
    public static final String COLUMM_RSE_MODE_RECHARGE = "rsemoderecharge";
    public static final String COLUMM_RSE_COUTRECHARGE_PHONE = "rsecoutrechargephone";
    public static final String COLUMM_RSE_TOTAL_HEBDO_RECHARGE = "rsetotalhebdorecharge";
    public static final String COLUMM_RSE_TRANSPORT= "rsetransport";




}
