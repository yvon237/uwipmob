package com.example.yvonzobo.uwipmob.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.yvonzobo.uwipmob.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MenuInstallationFragment.OnMenuInstallationFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MenuInstallationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuInstallationFragment extends Fragment {




    private OnMenuInstallationFragmentInteractionListener mListener;
    private LinearLayout layoutitineraire, layoutgps,layoutdonnees,layoutphoto;

    public MenuInstallationFragment() {
        // Required empty public constructor
    }


    public static MenuInstallationFragment newInstance() {
        MenuInstallationFragment fragment = new MenuInstallationFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_menu_installation, container, false);

                layoutitineraire = (LinearLayout)view.findViewById(R.id.layoutitineraire);
                layoutgps = (LinearLayout)view.findViewById(R.id.layoutgps);
                layoutdonnees = (LinearLayout)view.findViewById(R.id.layoutdonnees);
                layoutphoto = (LinearLayout)view.findViewById(R.id.layoutphoto);

        onClickLayoutGps();
        return view;
    }

    public void onClickLayoutDonnees() {
        layoutdonnees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void onClickLayoutGps() {
        layoutgps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed("gps");
            }
        });
    }

    public void onClickLayoutItineraire() {
        layoutitineraire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void onClickLayoutPhoto() {
        layoutphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String itemSelected) {
        if (mListener != null) {
            mListener.onGestionInstallationFragmentInteraction(itemSelected);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMenuInstallationFragmentInteractionListener) {
            mListener = (OnMenuInstallationFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMenuInstallationFragmentInteractionListener {
        // TODO: Update argument type and name
        void onGestionInstallationFragmentInteraction(String itemSelected);
    }
}
