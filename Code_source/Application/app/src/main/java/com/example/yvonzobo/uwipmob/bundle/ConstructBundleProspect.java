package com.example.yvonzobo.uwipmob.bundle;

import android.os.Bundle;

import com.example.yvonzobo.uwipmob.models.entities.prospects.CoordonnesProspectEntity;
import com.example.yvonzobo.uwipmob.models.entities.prospects.IdentiteProspectEntity;
import com.example.yvonzobo.uwipmob.models.entities.prospects.RessourcesProspectEntity;

/**
 * Created by Yvon ZOBO on 02/10/2016.
 */
public class ConstructBundleProspect {

    public static Bundle constructBundleIdentity(IdentiteProspectEntity identiteProspectEntity) {
        Bundle identite = new Bundle();
        identite.putString("firstname",identiteProspectEntity.getFirstname());
        identite.putString("lastname",identiteProspectEntity.getLastname());
        identite.putString("age",String.valueOf(identiteProspectEntity.getAge()));
        identite.putString("sexe",identiteProspectEntity.getSexe());
        identite.putString("langue",identiteProspectEntity.getLangue());
        identite.putString("nationality",identiteProspectEntity.getNationality());
        identite.putString("email",identiteProspectEntity.getEmail());
        return identite;
    }
    public static Bundle constructBundleCoordonnees(CoordonnesProspectEntity coordonnesProspectEntity) {
        Bundle coordonnees = new Bundle();

        coordonnees.putString("preferredphone",coordonnesProspectEntity.getPreferredphone());
        coordonnees.putString("phoneorange",coordonnesProspectEntity.getPhoneorange());
        coordonnees.putString("phonemtn",coordonnesProspectEntity.getPhonemtn());
        coordonnees.putString("phonenexttel",coordonnesProspectEntity.getPhonenexttel());
        coordonnees.putString("sourcing",coordonnesProspectEntity.getSourcing());
        coordonnees.putString("mobilemoney", coordonnesProspectEntity.getMobilemoney());
        coordonnees.putString("typezone",coordonnesProspectEntity.getTypezone());


        return coordonnees;
    }

    public static Bundle constructBundleRessources(RessourcesProspectEntity entity) {
        Bundle ressources = new Bundle();

        ressources.putString("rsesatisfaiteneo", entity.getRse_satisfaiteneo());
        ressources.putInt("rsefacturemensuelleeneo",entity.getRse_facturemensuelleeneo());
        ressources.putString("rsegenerateur",entity.getRse_generator());
        ressources.putInt("rseqtitehebdogasoil",entity.getRse_qtitehebdogasoil());
        ressources.putInt("rseprixunitgasoil",entity.getRse_prixunitgasoil());
        ressources.putString("rselampepetrole",entity.getRse_lampepetrole());
        ressources.putInt("rseqtitehebdopetrole",entity.getRse_qtitehebdopetrole());
        ressources.putInt("rseprixunitpetrole",entity.getRse_prixunitpetrole());
        ressources.putString("rsebougie",entity.getRse_bougie());
        ressources.putInt("rseqtitehebdobougie",entity.getRse_qtitehebdobougie());
        ressources.putInt("rseprixunitbougie",entity.getRse_prixunitbougie());
        ressources.putString("rsepile",entity.getRse_pile());
        ressources.putInt("rseqtitehebdopile",entity.getRse_qtitehebdopile());
        ressources.putInt("rseprixunitpile",entity.getRse_prixunitpile());
        ressources.putString("rseextrautilsgenerateur",entity.getRse_extrautilsgenerator());
        ressources.putString("rsemoderecharge",entity.getModerecharge());
        ressources.putInt("rseqtitephonedispo",entity.getRse_qtitephonedispo());
        ressources.putInt("rsecoutrechargephone",entity.getRse_coutrechargephone());
        ressources.putInt("rsetotalhebdorecharge",entity.getRse_totalhebdorecharge());
        ressources.putInt("rsetransport",entity.getRse_transport());

        return ressources;
    }
}
