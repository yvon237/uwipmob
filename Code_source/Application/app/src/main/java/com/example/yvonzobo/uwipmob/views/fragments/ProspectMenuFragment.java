package com.example.yvonzobo.uwipmob.views.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.yvonzobo.uwipmob.R;
import com.example.yvonzobo.uwipmob.adapters.MenuClientAdapter;
import com.example.yvonzobo.uwipmob.models.menu.MenuClient;
import com.example.yvonzobo.uwipmob.views.activities.ProspectActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ProspectMenuFragment extends Fragment {

    private ProspectMenuInteractionListener menuInteractionListener;
   private LinearLayout layoutnouveau, layoutsuivi, layoutcontrat, layoutlistclient;



    public ProspectMenuFragment() {
    }

    public static ProspectMenuFragment newInstance() {
        ProspectMenuFragment prospectMenuFragment = new ProspectMenuFragment();

        return prospectMenuFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_menu_prospect, container, false);


        layoutnouveau = (LinearLayout)view.findViewById(R.id.layoutnouveau);
        layoutsuivi = (LinearLayout)view.findViewById(R.id.layoutsuivi);
        layoutcontrat = (LinearLayout)view.findViewById(R.id.layoutcontrat);
        layoutlistclient = (LinearLayout)view.findViewById(R.id.layoutlistclient);



        onClickContrat();
        onClickListClient();
        onClickNouveau();
        onClickSuivi();
        return view;
    }


    public void onClickNouveau() {
        layoutnouveau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nouveau = new Intent(getActivity(), ProspectActivity.class);
                nouveau.putExtra("login",getActivity().getIntent().getStringExtra("login"));
                nouveau.putExtra("mdp",getActivity().getIntent().getStringExtra("mdp"));
                nouveau.putExtra("typeagent",getActivity().getIntent().getStringExtra("typeagent"));
                startActivity(nouveau);
                getActivity().finish();
            }
        });
    }

    public void onClickSuivi() {
        layoutsuivi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed("suivi");
            }
        });
    }

    public void onClickContrat() {
        layoutcontrat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed("contrat");
            }
        });
    }

    public void onClickListClient() {
        layoutlistclient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed("listeclient");
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProspectMenuInteractionListener) {
            menuInteractionListener = (ProspectMenuInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ProspectMenuInteractionListener");
        }
    }

    public void onButtonPressed(String b_selected) {

        if (menuInteractionListener != null) {
            menuInteractionListener.actionmenuevent(b_selected);
        }
    }


    public interface ProspectMenuInteractionListener {

        void actionmenuevent(String itemselected);
    }

}
