package com.example.yvonzobo.uwipmob.synchronisation;

import android.content.Context;
import android.widget.Toast;


import com.example.yvonzobo.uwipmob.controllers.ProspectController;
import com.example.yvonzobo.uwipmob.models.entities.ProspectEntity;
import com.example.yvonzobo.uwipmob.utility.ConstructParams;
import com.example.yvonzobo.uwipmob.utility.ExtractJsonObject;
import com.example.yvonzobo.uwipmob.utility.Urls;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;


/**
 * Created by Yvon ZOBO on 10/08/2016.
 */
public class SyncCustomer {
    private static ProspectController customerController;
    private static List<ProspectEntity> customerEntityListbd;







       public static void getUpdateCustomer(String url, ProspectEntity entity) {

    SyncHttpClient client = new SyncHttpClient();

        client.get(url, ConstructParams.constructParamsCustomer(entity, true), new JsonHttpResponseHandler());

    }

    public static void getCreateCustomer(String url,ProspectEntity customerEntity) {

        SyncHttpClient httpClient1 = new SyncHttpClient();

        httpClient1.get(url, ConstructParams.constructParamsCustomer(customerEntity, false), new JsonHttpResponseHandler());


    }

    /**
     * Synchronisation des clients au serveur
     * update un client si il existe
     * et crée un client si il n'existe pas au nveau du serveur
     * @param context
     */
    public static void synchronizeClient(final Context context){

        customerController = new ProspectController(context);
        SyncHttpClient httpClient =new SyncHttpClient();
        customerEntityListbd = customerController.getAllCustomers();
        int compteur;

        compteur = customerEntityListbd.size();


        for (int t=0;t<compteur;t++) {

            final int finalT = t;

            httpClient.get(Urls.getcountcustomerurl, ConstructParams.constructParamsCustomerCount(customerEntityListbd.get(t).getIdclient()), new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    // renvoi 1 si le client se trouve au niveau du serveur et o sinon
                    int  countsize = ExtractJsonObject.getCountCustomer(response);

                    if (countsize!=0) {
                        getUpdateCustomer(Urls.updatecustomerurl, customerEntityListbd.get(finalT));
                        //Toast.makeText(context, confirmMessage ,Toast.LENGTH_SHORT).show();

                    } else {
                         getCreateCustomer(Urls.addclienturl,customerEntityListbd.get(finalT));
                        //Toast.makeText(context, confirmMessage ,Toast.LENGTH_SHORT).show();

                    }
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    if (statusCode == 404) {
                        Toast.makeText(context,  "Ressource non trouvée dans le serveur",Toast.LENGTH_SHORT).show();

                    } else if (statusCode == 500) {
                        Toast.makeText(context,  "erreur 500, probleme avec la requete au serveur...",Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context,  "le serveur n'est pas disponible pour " + "l'instant...", Toast.LENGTH_SHORT).show();

                    }
                }


            });
        }


    }


}
