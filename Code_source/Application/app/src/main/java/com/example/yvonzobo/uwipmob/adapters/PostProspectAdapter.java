package com.example.yvonzobo.uwipmob.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.yvonzobo.uwipmob.R;

import com.example.yvonzobo.uwipmob.models.entities.PostProspectList;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Yvon ZOBO on 24/08/2016.
 */
public class PostProspectAdapter extends BaseAdapter{

    public class ViewHolder {
        TextView nom, localite;
    }
    public List<PostProspectList> postCustomerList;
    public Context context;
    ArrayList<PostProspectList> arrayList;



    public PostProspectAdapter(List<PostProspectList> list, Context context) {
        postCustomerList = list;
        context = context;
        arrayList = new ArrayList<>();
        arrayList.addAll(postCustomerList);

    }


    @Override
    public int getCount() {
        return postCustomerList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, final View convertView, ViewGroup parent) {

        View rowView = convertView;

        final ViewHolder viewHolder;

        if (rowView == null) {
            rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_prospect_list, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.nom = (TextView) rowView.findViewById(R.id.title);
            viewHolder.localite = (TextView) rowView.findViewById(R.id.subtitle);
            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        try {
            viewHolder.nom.setText(postCustomerList.get(i).getNom().toLowerCase() + " " + postCustomerList.get(i).getPrenom().toLowerCase());
            viewHolder.localite.setText(postCustomerList.get(i).getLocalite().toLowerCase() + " -> " + postCustomerList.get(i).getVillage().toLowerCase());

        } catch (Exception e) {
            e.getCause();
            e.getMessage();
        }

        return rowView;
    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());

        postCustomerList.clear();
        try {
            if (charText.length() == 0) {
                postCustomerList.addAll(arrayList);

            } else {
                for (PostProspectList postCustomer : arrayList) {

                    if (charText.length() != 0 && postCustomer.getNom().toLowerCase(Locale.getDefault()).contains(charText)) {
                        postCustomerList.add(postCustomer);
                    } /*else if (charText.length() != 0 && postCustomer.getLocalite().toLowerCase(Locale.getDefault()).contains(charText)) {
                    postCustomerList.add(postCustomer);
                }*/ else if (charText.length() != 0 && postCustomer.getPrenom().toLowerCase(Locale
                            .getDefault()).contains(charText)) {
                        postCustomerList.add(postCustomer);
                    } else if (charText.length() != 0 && postCustomer.getVillage().toLowerCase(Locale
                            .getDefault()).contains(charText)) {
                        postCustomerList.add(postCustomer);
                    }
                }
            }
        } catch (Exception e) {
            e.getMessage();
            e.getCause();
        }
        notifyDataSetChanged();
    }


}
