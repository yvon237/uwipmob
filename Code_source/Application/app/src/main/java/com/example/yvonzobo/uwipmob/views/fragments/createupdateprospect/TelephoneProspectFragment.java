package com.example.yvonzobo.uwipmob.views.fragments.createupdateprospect;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.yvonzobo.uwipmob.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TelephoneProspectFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TelephoneProspectFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TelephoneProspectFragment extends Fragment implements AdapterView.OnItemSelectedListener {


    private OnFragmentInteractionListener mListener;

    private TextView t_couttrans, t_qtitephone,t_moderecharge,t_couttransphone,t_hebdorecharge,t_coutrecharge;
    private TextView l_couttrans, l_qtitephone,l_moderecharge,l_couttransphone,l_hebdorecharge,l_coutrecharge;
    private LinearLayout layoutcouttrans,  layoutqtitephone,layoutmoderecharge,layoutcouttransphone,layouthebdorecharge,layoutcoutrecharge;

    private  AlertDialog.Builder builder;
    private  EditText editText;
    private Spinner spinnerqtitephone, spinnernombrerecharge;
    private String[] nombrekm = {"1km", "entre 1 à 5km", "plus de 5km"};
    private RadioGroup radioGroup;
    private RadioButton radiodomicle, radioexterieur;
    private String[] nombrerecharge = {"1 fois", "2 fois", "3 fois", "4 fois", "5 fois","6 fois", "7 fois", "8 fois"};
    private int controleSpinner=0;
    private String valeurmodeRechargechoisi ="";

    public TelephoneProspectFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TelephoneProspectFragment newInstance(String param1, String param2) {
        TelephoneProspectFragment fragment = new TelephoneProspectFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_telephone_prospect, container, false);

        t_couttrans = (TextView)view.findViewById(R.id.t_couttrans);
        t_qtitephone = (TextView)view.findViewById(R.id.t_qtitephone);
        t_moderecharge = (TextView)view.findViewById(R.id.t_moderecharge);
        t_couttransphone = (TextView)view.findViewById(R.id.t_couttransphone);
        t_hebdorecharge = (TextView)view.findViewById(R.id.t_hebdorecharge);
        t_coutrecharge = (TextView)view.findViewById(R.id.t_coutrecharge);


         l_couttrans = (TextView)view.findViewById(R.id.l_couttrans);
        l_qtitephone = (TextView)view.findViewById(R.id.l_qtitephone);
        l_moderecharge = (TextView)view.findViewById(R.id.l_moderecharge);
        l_couttransphone = (TextView)view.findViewById(R.id.l_couttransphone);
        l_hebdorecharge = (TextView)view.findViewById(R.id.l_hebdorecharge);
        l_coutrecharge = (TextView)view.findViewById(R.id.l_coutrecharge);





        layoutcouttrans = (LinearLayout)view.findViewById(R.id.layoutcouttrans);
        layoutqtitephone = (LinearLayout)view.findViewById(R.id.layoutqtitephone);
        layoutmoderecharge = (LinearLayout)view.findViewById(R.id.layoutmoderecharge);
        layoutcouttransphone = (LinearLayout)view.findViewById(R.id.layoutcouttransphone);
        layouthebdorecharge = (LinearLayout)view.findViewById(R.id.layouthebdorecharge);
        layoutcoutrecharge = (LinearLayout)view.findViewById(R.id.layoutcoutrecharge);

                 return view;
    }

    public void onClickLayoutCoutTrans() {
        layoutcouttrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void onClickLayoutqtitephone() {
        layoutqtitephone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void onClickLayoutModeRecharge() {
        layoutmoderecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
    public void onClicklayoutcouttransphone() {
        layoutcouttransphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void onClicklayouthebdorecharge() {
        layouthebdorecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void onClicklayoutcoutrecharge() {
        layoutcoutrecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private Dialog onCreateDialogCoutTransport(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_TEXT);

        builder.setView(editText);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonCoutTransClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeCoutTransButtonClickListener());

        return builder.show();
    }



    private class OnPositiveButtonCoutTransClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            final String valeur = editText.getText().toString();
            t_couttrans.setText(valeur);



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeCoutTransButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }




    private Dialog onCreateDialogQtitephone(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);

        builder.setView(editText);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonQtitephoneClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeQtitephoneButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonQtitephoneClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            final String valeur = editText.getText().toString();
            t_qtitephone.setText(valeur);



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeQtitephoneButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }



    private Dialog onCreateDialogModeRecharge(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        spinnerqtitephone = new Spinner(getActivity());
        spinnerqtitephone.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,nombrekm);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerqtitephone.setAdapter(adapterType);

        controleSpinner=1;

        LinearLayout layoutRadioGroup = new LinearLayout(getActivity());
        layoutRadioGroup.setOrientation(LinearLayout.HORIZONTAL);

        radioGroup = new RadioGroup(getActivity());
        radiodomicle = new RadioButton(getActivity());
        radiodomicle.setId(RadioButton.generateViewId());
        radiodomicle.setText("à domicile");
        radioexterieur = new RadioButton(getActivity());
        radioexterieur.setId(RadioButton.generateViewId());
        radioexterieur.setText("à l'exterieur");





        radioGroup.addView(radiodomicle);
        radioGroup.addView(radioexterieur);

        radioGroup.setOrientation(RadioGroup.HORIZONTAL);
        layoutRadioGroup.addView(radioGroup);


        builder.setView(layoutRadioGroup);


        if (radioGroup.getCheckedRadioButtonId() == radioexterieur.getId()) {
            builder.setView(spinnerqtitephone);
        }



        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonModeRechargeClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeModeRechargeButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonModeRechargeClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            if (radioGroup.getCheckedRadioButtonId() == radiodomicle.getId()) {
                t_moderecharge.setText("a domicile");
            } else if (radioGroup.getCheckedRadioButtonId() == radioexterieur.getId()) {
                valeurmodeRechargechoisi = "a l'exterieur";
            }

            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeModeRechargeButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }

    private Dialog onCreateDialogHebdoRecharge(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        spinnernombrerecharge = new Spinner(getActivity());
        spinnernombrerecharge.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,nombrerecharge);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnernombrerecharge.setAdapter(adapterType);

        controleSpinner=2;



        builder.setView(spinnernombrerecharge);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonHebdoRechargeClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeHebdoRechargeButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonHebdoRechargeClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){


            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeHebdoRechargeButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (controleSpinner == 1) {
            t_moderecharge.setText(valeurmodeRechargechoisi+" ("+nombrekm[i]+")");
        } else if (controleSpinner == 2) {
            t_hebdorecharge.setText(nombrerecharge[i]);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private Dialog onCreateDialogCoutTransPhone(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);

        builder.setView(editText);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonCoutTransPhoneClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeCoutTransPhoneButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonCoutTransPhoneClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            final String valeur = editText.getText().toString();
            t_couttransphone.setText(valeur);



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeCoutTransPhoneButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }

    private Dialog onCreateDialogCoutRecharge(TextView labelle){
        builder= new AlertDialog.Builder(getActivity());
        editText = new EditText(getActivity());
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);

        builder.setView(editText);

        builder.setTitle(labelle.getText());
        builder.setCancelable(false);


        builder.setPositiveButton(android.R.string.ok, new OnPositiveButtonCoutRechargeClickListener());
        builder.setNegativeButton(android.R.string.cancel, new OnNegativeCoutRechargeButtonClickListener());

        return builder.show();
    }
    private class OnPositiveButtonCoutRechargeClickListener implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int id){
            final String valeur = editText.getText().toString();
            t_coutrecharge.setText(valeur);



            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    dialog.dismiss();
                }
            });

        }
    }
    private class OnNegativeCoutRechargeButtonClickListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int id){

            builder.setCancelable(true);
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
