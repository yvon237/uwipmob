package com.example.yvonzobo.uwipmob.models.entities.prospects;

/**
 * Created by Yvon ZOBO on 11/10/2016.
 */
public class SolvabiliteProspectEntity {

    private String solvable_matrimonialstatus; // situation matrimoniale
    private int solvable_totalenfant; // nombre d'enfant en charge
    private String solvable_activiteprof1; // activité professionnel 1 du client
    private String solvable_activiteprof2; // activité professionnel 2 du client
    private String solvable_typefonction; // champ active si activite1 ou activite2 = fonctionnaire
    private String solvable_betail; // si activite 1 ou activite 2 = eleveur
    private String solvable_culture; // si activite 1 ou activité2= agriculteur
    private String solvable_typecommerce; // si activite1 ou activite2= commerçant
    private String solvable_activiteconjoint; // activite du conjoint ou de la conjointe
    private String solvable_dureeperiodecreuse; // la periode creuse que connait le client
    private String solvable_suretepaye; // sur de payer ses mensualités ( oui /non)
    private String solvable_epargneactu; // disposez-vous d'un compte d'epargne
    private String solvable_empruntactu; // avez vous des empruntes en cours
    private int solvable_nbmaisons; // nombre de maisons
    private int solvable_nbhectares; // nombre d'hectares de terrain
    private int solvable_nbvoitures; // nombre de voitures
    private int solvable_nbmotos; // nombre de moto
    private String solvable_patrimooineautre; // autres patrimoines
    private String solvable_associationactu; // etes vous inscrit dans association
    private String solvable_remarques; // remaques du commercial


    public SolvabiliteProspectEntity() {

    }

    public String getSolvable_matrimonialstatus() {
        return solvable_matrimonialstatus;
    }

    public void setSolvable_matrimonialstatus(String solvable_matrimonialstatus) {
        this.solvable_matrimonialstatus = solvable_matrimonialstatus;
    }

    public int getSolvable_totalenfant() {
        return solvable_totalenfant;
    }

    public void setSolvable_totalenfant(int solvable_totalenfant) {
        this.solvable_totalenfant = solvable_totalenfant;
    }

    public String getSolvable_activiteprof1() {
        return solvable_activiteprof1;
    }

    public void setSolvable_activiteprof1(String solvable_activiteprof1) {
        this.solvable_activiteprof1 = solvable_activiteprof1;
    }

    public String getSolvable_activiteprof2() {
        return solvable_activiteprof2;
    }

    public void setSolvable_activiteprof2(String solvable_activiteprof2) {
        this.solvable_activiteprof2 = solvable_activiteprof2;
    }

    public String getSolvable_typefonction() {
        return solvable_typefonction;
    }

    public void setSolvable_typefonction(String solvable_typefonction) {
        this.solvable_typefonction = solvable_typefonction;
    }

    public String getSolvable_betail() {
        return solvable_betail;
    }

    public void setSolvable_betail(String solvable_betail) {
        this.solvable_betail = solvable_betail;
    }

    public String getSolvable_culture() {
        return solvable_culture;
    }

    public void setSolvable_culture(String solvable_culture) {
        this.solvable_culture = solvable_culture;
    }

    public String getSolvable_typecommerce() {
        return solvable_typecommerce;
    }

    public void setSolvable_typecommerce(String solvable_typecommerce) {
        this.solvable_typecommerce = solvable_typecommerce;
    }

    public String getSolvable_activiteconjoint() {
        return solvable_activiteconjoint;
    }

    public void setSolvable_activiteconjoint(String solvable_activiteconjoint) {
        this.solvable_activiteconjoint = solvable_activiteconjoint;
    }

    public String getSolvable_dureeperiodecreuse() {
        return solvable_dureeperiodecreuse;
    }

    public void setSolvable_dureeperiodecreuse(String solvable_dureeperiodecreuse) {
        this.solvable_dureeperiodecreuse = solvable_dureeperiodecreuse;
    }

    public String getSolvable_epargneactu() {
        return solvable_epargneactu;
    }

    public void setSolvable_epargneactu(String solvable_epargneactu) {
        this.solvable_epargneactu = solvable_epargneactu;
    }

    public String getSolvable_suretepaye() {
        return solvable_suretepaye;
    }

    public void setSolvable_suretepaye(String solvable_suretepaye) {
        this.solvable_suretepaye = solvable_suretepaye;
    }

    public String getSolvable_empruntactu() {
        return solvable_empruntactu;
    }

    public void setSolvable_empruntactu(String solvable_empruntactu) {
        this.solvable_empruntactu = solvable_empruntactu;
    }

    public int getSolvable_nbmaisons() {
        return solvable_nbmaisons;
    }

    public void setSolvable_nbmaisons(int solvable_nbmaisons) {
        this.solvable_nbmaisons = solvable_nbmaisons;
    }

    public int getSolvable_nbhectares() {
        return solvable_nbhectares;
    }

    public void setSolvable_nbhectares(int solvable_nbhectares) {
        this.solvable_nbhectares = solvable_nbhectares;
    }

    public int getSolvable_nbvoitures() {
        return solvable_nbvoitures;
    }

    public void setSolvable_nbvoitures(int solvable_nbvoitures) {
        this.solvable_nbvoitures = solvable_nbvoitures;
    }

    public int getSolvable_nbmotos() {
        return solvable_nbmotos;
    }

    public void setSolvable_nbmotos(int solvable_nbmotos) {
        this.solvable_nbmotos = solvable_nbmotos;
    }

    public String getSolvable_patrimooineautre() {
        return solvable_patrimooineautre;
    }

    public void setSolvable_patrimooineautre(String solvable_patrimooineautre) {
        this.solvable_patrimooineautre = solvable_patrimooineautre;
    }

    public String getSolvable_associationactu() {
        return solvable_associationactu;
    }

    public void setSolvable_associationactu(String solvable_associationactu) {
        this.solvable_associationactu = solvable_associationactu;
    }

    public String getSolvable_remarques() {
        return solvable_remarques;
    }

    public void setSolvable_remarques(String solvable_remarques) {
        this.solvable_remarques = solvable_remarques;
    }
}
