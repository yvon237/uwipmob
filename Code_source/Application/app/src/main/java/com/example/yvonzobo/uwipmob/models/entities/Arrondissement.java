package com.example.yvonzobo.uwipmob.models.entities;

/**
 * Created by Yvon ZOBO on 11/10/2016.
 */
public class Arrondissement {

    private Long idarrondissement;
    private String libelle_arrondissement;


    public Arrondissement() {

    }

    public Long getIdarrondissement() {
        return idarrondissement;
    }

    public void setIdarrondissement(Long idarrondissement) {
        this.idarrondissement = idarrondissement;
    }

    public String getLibelle_arrondissement() {
        return libelle_arrondissement;
    }

    public void setLibelle_arrondissement(String libelle_arrondissement) {
        this.libelle_arrondissement = libelle_arrondissement;
    }
}
