package com.example.yvonzobo.uwipmob.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Yvon ZOBO on 28/09/2016.
 */
public class Dates {

    // choix de la langue

    static Locale locale = Locale.FRANCE.getDefault();
    static Date now = new Date();


    // definition du format
    static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");



    public static String date() {
        String date = dateFormat.format(now);

        return date;

    }
}
