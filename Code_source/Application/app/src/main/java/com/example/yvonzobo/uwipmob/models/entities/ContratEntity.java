package com.example.yvonzobo.uwipmob.models.entities;


/**
 * Created by Yvon ZOBO on 04/08/2016.
 */
public class ContratEntity {

    private Long idcontract;
    private String datecontra; // champ date
    private Double totalmoneypaid;
    private Double totlmoneydue;
    private Double moneybalance;
    private String contractpic;
    private String homepic;
    private String village;
    private String municipality;
    private String estimnextrtansact; // champ date
    private String typeinstall;
    private String natconnect;
    private String installstatus;
    private String userofsystem;
    private String installdate; // champ date
    private String desinstalldate; // champ date
    private Integer phoneofusersystem;
    private Integer numusersystem;
    private String typecontrat;
    private Long idboxe;
    private Long idclient;


    public ContratEntity() {
    }

    public Long getIdcontract() {
        return idcontract;
    }

    public void setIdcontract(Long idcontract) {
        this.idcontract = idcontract;
    }

    public String getDatecontra() {
        return datecontra;
    }

    public void setDatecontra(String datecontra) {
        this.datecontra = datecontra;
    }

    public Double getTotalmoneypaid() {
        return totalmoneypaid;
    }

    public void setTotalmoneypaid(Double totalmoneypaid) {
        this.totalmoneypaid = totalmoneypaid;
    }

    public Double getTotlmoneydue() {
        return totlmoneydue;
    }

    public void setTotlmoneydue(Double totlmoneydue) {
        this.totlmoneydue = totlmoneydue;
    }

    public Double getMoneybalance() {
        return moneybalance;
    }

    public void setMoneybalance(Double moneybalance) {
        this.moneybalance = moneybalance;
    }

    public String getContractpic() {
        return contractpic;
    }

    public void setContractpic(String contractpic) {
        this.contractpic = contractpic;
    }

    public String getHomepic() {
        return homepic;
    }

    public void setHomepic(String homepic) {
        this.homepic = homepic;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getEstimnextrtansact() {
        return estimnextrtansact;
    }

    public void setEstimnextrtansact(String estimnextrtansact) {
        this.estimnextrtansact = estimnextrtansact;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getTypeinstall() {
        return typeinstall;
    }

    public void setTypeinstall(String typeinstall) {
        this.typeinstall = typeinstall;
    }

    public String getNatconnect() {
        return natconnect;
    }

    public void setNatconnect(String natconnect) {
        this.natconnect = natconnect;
    }

    public String getInstallstatus() {
        return installstatus;
    }

    public void setInstallstatus(String installstatus) {
        this.installstatus = installstatus;
    }

    public String getUserofsystem() {
        return userofsystem;
    }

    public void setUserofsystem(String userofsystem) {
        this.userofsystem = userofsystem;
    }

    public String getInstalldate() {
        return installdate;
    }

    public void setInstalldate(String installdate) {
        this.installdate = installdate;
    }

    public String getDesinstalldate() {
        return desinstalldate;
    }

    public void setDesinstalldate(String desinstalldate) {
        this.desinstalldate = desinstalldate;
    }

    public Integer getPhoneofusersystem() {
        return phoneofusersystem;
    }

    public void setPhoneofusersystem(Integer phoneofusersystem) {
        this.phoneofusersystem = phoneofusersystem;
    }

    public Integer getNumusersystem() {
        return numusersystem;
    }

    public void setNumusersystem(Integer numusersystem) {
        this.numusersystem = numusersystem;
    }

    public String getTypecontrat() {
        return typecontrat;
    }

    public void setTypecontrat(String typecontrat) {
        this.typecontrat = typecontrat;
    }

    public Long getIdboxe() {
        return idboxe;
    }

    public void setIdboxe(Long idboxe) {
        this.idboxe = idboxe;
    }

    public Long getIdclient() {
        return idclient;
    }

    public void setIdclient(Long idclient) {
        this.idclient = idclient;
    }
}
