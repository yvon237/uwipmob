package com.example.yvonzobo.uwipmob.views.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yvonzobo.uwipmob.R;
import com.example.yvonzobo.uwipmob.controllers.AgentController;
import com.example.yvonzobo.uwipmob.controllers.UserController;
import com.example.yvonzobo.uwipmob.models.entities.AgentEntity;
import com.example.yvonzobo.uwipmob.utility.CheckNetworkConnection;
import com.example.yvonzobo.uwipmob.utility.ExtractJsonObject;
import com.example.yvonzobo.uwipmob.utility.ShaHash;
import com.example.yvonzobo.uwipmob.utility.Urls;
import com.example.yvonzobo.uwipmob.utility.Utility;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.concurrent.TimeoutException;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends AppCompatActivity {

    private EditText login, pass;
    private TextView b_connexion;
    private AgentController agentController;

    private AgentEntity agentEntity = new AgentEntity();




    static LoginActivity loginActivity;


    private UserController userController;

    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginActivity = this;
        dialog = new ProgressDialog(this);
        dialog.setMessage("patientez s'il vous plait!!");
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);



        agentController = new AgentController(this);

        login = (EditText) findViewById(R.id.e_login);
        pass = (EditText) findViewById(R.id.e_password);
        b_connexion = (TextView) findViewById(R.id.b_connexion);




        onClickB_connexion();


    }

    public static LoginActivity getInstance(){
        return loginActivity;
    }

    public void onClickB_connexion() {
        b_connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();

            }
        });
    }


    /**
     * method de connexion online
     */
    public void doLoginOnline() {

        String username = login.getText().toString();
        String password = pass.getText().toString();
        RequestParams params = new RequestParams();

        params.put("login", username);
        params.put("mdp", new ShaHash().hash(password));
        // invocation du web service par le mobile
        invokeWS(getApplicationContext(), params,Urls.loginAgenturl);


    }



    /**
     * Method de connexion;
     */
    public void doLogin() {


        if (!Utility.isNotNull(login.getText().toString())){
            login.setError("at least 4 characters!!");
        }

        else if (!Utility.isNotNull(pass.getText().toString())){
            pass.setError("at least 4 characters!!");
        }

        if (Utility.isNotNull(login.getText().toString()) && Utility.isNotNull(pass.getText().toString())) {
            if (agentController.checkAgentBylogin_Password(login.getText().toString(), new ShaHash().hash(pass.getText().toString()))) {

                Toast.makeText(LoginActivity.this, "authentification terminée avec succès...", Toast.LENGTH_LONG).show();
                navigationAgentConenctActivity();
            }  else {
                if (CheckNetworkConnection.isOnline(getApplicationContext())) {

                    doLoginOnline();
                } else {
                    Toast.makeText(this, "Pas de connexion internet disponible...", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(this, "verifiez votre login et password ou  contactez l'administrateur...", Toast.LENGTH_LONG).show();
        }



    }




    public void navigationAgentConenctActivity() {
        String typeagent= agentController.getTypeAgent(login.getText().toString(), new ShaHash().hash(pass.getText().toString()));
        Intent mainAcitivity = new Intent(LoginActivity.this, AccueilActivity.class);
        mainAcitivity.putExtra("login", login.getText().toString());
        mainAcitivity.putExtra("mdp", pass.getText().toString());
       mainAcitivity.putExtra("typeagent",typeagent);

        startActivity(mainAcitivity);

    }

    /**
     * appel du web service pour authentifier l'utilisateur en ligne
     * connexion asynchronne
     *
     * @param context
     * @param params
     * @param url
     */

    public void invokeWS(final Context context, final RequestParams params, final String url) {
        final AsyncHttpClient client = new AsyncHttpClient();
        client.setResponseTimeout(20000);
        client.setMaxRetriesAndTimeout(2,20000);
        client.get(url, params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                dialog.dismiss();


                try {
                    agentEntity = ExtractJsonObject.extractAgentJsonObject(response);
                        // verification si l'objet est vide
                    if (agentEntity.getIdagent().equals("")) {
                        Toast.makeText(getApplicationContext(), "Merci de contacter l'administrateur!", Toast.LENGTH_LONG).show();
                    } else {
                            //on vérifie si la table agent n'est pas vide, si oui
                        if (agentController.getLoginAgentFromdb()) {
                            //on supprime l'agent à partir de l'id utilisateur
                            if (agentController.deleteAgent(agentController.getIdUuser())) {
                                if (agentController.insertAgent(agentEntity)) {
                                    Toast.makeText(getApplicationContext(), "Agent ajouté avec succès!", Toast.LENGTH_LONG).show();
                                }
                            }
                        } else {
                            if (agentController.insertAgent(agentEntity)) {
                                Toast.makeText(getApplicationContext(), "Agent ajouté avec succès!", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.getCause();
                    e.getMessage();
                }
                Toast.makeText(LoginActivity.this, "authentification terminée avec succès...", Toast.LENGTH_LONG).show();
                navigationAgentConenctActivity();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                dialog.dismiss();


                if (statusCode == 404) {

                    Toast.makeText(context, "Ressource non trouvée dans le serveur", Toast.LENGTH_LONG);

                } else if (statusCode == 500) {

                    Toast.makeText(context, "erreur 500, probleme avec la requete au serveur...", Toast.LENGTH_LONG).show();
                } else {

                    Toast.makeText(context, statusCode + "le serveur n'est pas disponible pour l'instant...", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }

        });
    }





}
