package com.example.yvonzobo.uwipmob.views.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.yvonzobo.uwipmob.R;


public class MenuSuiviProspectFragment extends Fragment {

    private MenuSuiviInteractionListener mMesuSuiviListener;
    private LinearLayout planifierrelance, relancerprospect, modifierprospect,statrelance;

    public MenuSuiviProspectFragment() {
        // Required empty public constructor
    }


    public static MenuSuiviProspectFragment newInstance() {
        MenuSuiviProspectFragment fragment = new MenuSuiviProspectFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_menu_suivi_prospect, container, false);


                planifierrelance = (LinearLayout) view.findViewById(R.id.layoutplanifier);
                relancerprospect = (LinearLayout) view.findViewById(R.id.layoutrelancer);
                modifierprospect = (LinearLayout) view.findViewById(R.id.layoutmodifierprospect);
                statrelance = (LinearLayout) view.findViewById(R.id.layoutstat);


        clickBModifierProspect();
        return view;
    }

    public void clickBPlanifierRelance() {
        planifierrelance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void clickBelancerProspect() {
        relancerprospect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void clickBModifierProspect() {
        modifierprospect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed("modifier");
            }
        });
    }

    public void clickBStatRelance() {
        statrelance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String val) {
        if (mMesuSuiviListener != null) {
            mMesuSuiviListener.MenuSuiviInteraction(val);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MenuSuiviInteractionListener) {
            mMesuSuiviListener = (MenuSuiviInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mMesuSuiviListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface MenuSuiviInteractionListener {
        // TODO: Update argument type and name
        void MenuSuiviInteraction(String val);
    }
}
