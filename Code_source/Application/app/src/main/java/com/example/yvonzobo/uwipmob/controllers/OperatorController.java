package com.example.yvonzobo.uwipmob.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.example.yvonzobo.uwipmob.models.database.ContentDb;
import com.example.yvonzobo.uwipmob.models.database.DbHelper;
import com.example.yvonzobo.uwipmob.models.entities.OperatorEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yvon ZOBO on 29/07/2016.
 */
public class OperatorController {

    public static final String CREATE_TABLE_OPERATEUR = "create table "+
            ContentDb.TABLE_OPERATOR+" ("+
            ContentDb.COLUMM_ID_OPERATOR+" integer primary key ,"+
            ContentDb.COLUMM_CODE+" Text);";



    private DbHelper db;
    private SQLiteDatabase mydb;


    public OperatorController(Context context){
        db = new  DbHelper(context);
    }


    /**
     * ouvre la base de données en écriture
     * @return
     */
    public SQLiteDatabase open(){

        mydb = db.getWritableDatabase();
        return mydb;
    }

    /**
     * ferme la base de données
     */
    public void close(){
        mydb.close();
    }

    public SQLiteDatabase getMydb(){
        return mydb;
    }


    public  boolean createOperator(List<OperatorEntity> operatorEntityList){

        long  result = 0;
        for (int taille=0;taille<operatorEntityList.size();taille++){
            ContentValues values = new ContentValues();
            values.put(ContentDb.COLUMM_ID_OPERATOR,operatorEntityList.get(taille).getIdoperator());
            values.put(ContentDb.COLUMM_CODE,operatorEntityList.get(taille).getCode());
            result = open().insert(ContentDb.TABLE_OPERATOR,null,values);
        }

        if (result==-1)
            return false;
        else return true;
    }


    public boolean compareTwoOperator(OperatorEntity entity1,OperatorEntity entity2){
        if (entity1.equals(entity2))
            return true;
        else return false;
    }
    public boolean getOperatorByCode(String code){
        Cursor cursor = open().rawQuery("select code from operator where code = ?",new
                String[]{code});

        if (cursor.getCount()==0)
            return false;
        else
            return true;
    }

    public boolean createOperator(OperatorEntity entity){
        ContentValues values = new ContentValues();

        values.put(ContentDb.COLUMM_ID_OPERATOR,entity.getIdoperator());
        values.put(ContentDb.COLUMM_CODE,entity.getCode());

        long result = open().insert(ContentDb.TABLE_OPERATOR,null,values);


        if (result==-1)
            return false;
        else return true;
    }

    private ArrayList<OperatorEntity> cursorToOperators(Cursor cursor){

        if (cursor.getCount()==0){
            return new ArrayList<>(0);
        }

        ArrayList<OperatorEntity>  operatorEntities = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()){
            do {
                OperatorEntity entity = new OperatorEntity();

                entity.setIdoperator(cursor.getLong(0));
                entity.setCode(cursor.getString(1));
                operatorEntities.add(entity);

            }while (cursor.moveToNext());
        }


        cursor.close();
        return operatorEntities;
    }

    public int getIdoperator(String code){
        Cursor cursor = open().rawQuery("select idoperator from operator where code = ?",new String[]{code});

        if (cursor.getCount()==0){
            int tidoperator = getCount()+1;
            OperatorEntity entity = new OperatorEntity();
            entity.setIdoperator(Long.valueOf(tidoperator));
            entity.setCode(code);
            if(createOperator(entity))
                return tidoperator;
        }
        cursor.moveToFirst();
        return  cursor.getInt(0);
    }
    public  int getCount(){

        try {
            Cursor cursor = open().rawQuery("select count(idoperator) from operator",null);
            cursor.moveToFirst();
            int total = cursor.getInt(0);

            return total;

        }catch (Exception e){
            e.getMessage();
            return 0;
        }



    }


    public List<OperatorEntity> getAllOperator(){
        Cursor cursor = open().rawQuery("select * from operator",null);
        if (cursor.getCount()==0)
            return new ArrayList<>(0);

        return cursorToOperators(cursor);

    }

    public String getCodeOperatorById(Long id){

        Cursor cursor = open().rawQuery("select code from operator where idoperator = ?", new String[]{String.valueOf(id)});

        if (cursor.getCount()==0)
            return  null;

        cursor.moveToFirst();

        return  cursor.getString(0);
    }

    }
