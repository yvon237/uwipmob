package com.example.yvonzobo.uwipmob.views.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yvonzobo.uwipmob.R;
import com.example.yvonzobo.uwipmob.views.fragments.ChangePassFragment;
import com.example.yvonzobo.uwipmob.views.fragments.DetailsProspectFragment;
import com.example.yvonzobo.uwipmob.views.fragments.GPSFragment;
import com.example.yvonzobo.uwipmob.views.fragments.ListeProspectFragment;
import com.example.yvonzobo.uwipmob.views.fragments.MenuInstallationFragment;
import com.example.yvonzobo.uwipmob.views.fragments.MenuMaintenanceFragment;
import com.example.yvonzobo.uwipmob.views.fragments.MenuPrincipalFragment;
import com.example.yvonzobo.uwipmob.views.fragments.MenuSuiviProspectFragment;
import com.example.yvonzobo.uwipmob.views.fragments.ProspectMenuFragment;
import com.example.yvonzobo.uwipmob.views.fragments.createupdateprospect.IdentityProspectFragment;

public class AccueilActivity extends AppCompatActivity implements MenuPrincipalFragment.MenuPrincipalInteractionListener, MenuSuiviProspectFragment.MenuSuiviInteractionListener ,
MenuInstallationFragment.OnMenuInstallationFragmentInteractionListener,ProspectMenuFragment.ProspectMenuInteractionListener,DetailsProspectFragment.DetailsProspectInteractionListener,ListeProspectFragment.ListeProspectInteractionListener ,
MenuMaintenanceFragment.OnMenuMaintenanceFragmentInteractionListener, GPSFragment.OnGPSFragmentFragmentInteractionListener,ChangePassFragment.OnChangePassFragmentInteractionListener{


    Fragment mfragment, parentfragment; // instance du fragment courant et instance du fragment de depart
    private TextView title; // sous titre de la page
    Toolbar toolbar;
    private int count=0; // recupere le nombre de fois que l'utilisateur clique backpressed
    private static AccueilActivity accueilActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        accueilActivity = this;
        setContentView(R.layout.activity_accueil);
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackground(getResources().getDrawable(R.drawable.black_color));
        toolbar.setTitle(getResources().getString(R.string.titre));
        setSupportActionBar(toolbar);

        LoginActivity.getInstance().finish();

        title = (TextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.app_name));

        setFragment(MenuPrincipalFragment.newInstance());
        parentfragment = MenuPrincipalFragment.newInstance();

    }



    public static AccueilActivity getInstance() {
        return accueilActivity;
    }

    @Override
    public void onBackPressed() {

        if (mfragment == null) {

                onBackPressed();

        } else if (mfragment instanceof MenuInstallationFragment) {
            setFragment(parentfragment);
            toolbar.setTitle(getResources().getString(R.string.titre));
            title.setText(getResources().getString(R.string.app_name));

        } else if (mfragment instanceof MenuSuiviProspectFragment) {
            setFragment(parentfragment);
            title.setText(getResources().getString(R.string.app_name));
            toolbar.setTitle(getResources().getString(R.string.titre));

        } else if (mfragment instanceof ProspectMenuFragment) {
            setFragment(parentfragment);
            title.setText(getResources().getString(R.string.app_name));
            toolbar.setTitle(getResources().getString(R.string.titre));

        } else if (mfragment instanceof MenuMaintenanceFragment) {
            setFragment(parentfragment);
            title.setText(getResources().getString(R.string.app_name));
            toolbar.setTitle(getResources().getString(R.string.titre));

        } else if (mfragment instanceof GPSFragment) {
            setFragment(MenuInstallationFragment.newInstance());
            title.setText(getResources().getString(R.string.app_name));
            toolbar.setTitle(getResources().getString(R.string.titre));
            mfragment=MenuInstallationFragment.newInstance();
        }else if (mfragment instanceof ChangePassFragment) {
            setFragment(parentfragment);
            title.setText(getResources().getString(R.string.app_name));
            toolbar.setTitle(getResources().getString(R.string.titre));

        }else {
            mfragment = null;
            Toast.makeText(getApplicationContext(), "Appuyer de nouveau pour quitter", Toast.LENGTH_LONG).show();

        }

    }

    public void setFragment(final Fragment fragment){
        if(fragment==null) return;

        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right).replace(R.id.container,fragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.changecolor) {
            return true;
        } else if (id == R.id.changepass) {
            setFragment(ChangePassFragment.newInstance(getIntent().getStringExtra("login"),getIntent().getStringExtra("mdp")));
            mfragment = ChangePassFragment.newInstance(getIntent().getStringExtra("login"),getIntent().getStringExtra("mdp"));
            return true;
        } else if (id == R.id.aide) {
            return true;
        } else if (id == R.id.apropos) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * methode interface menu principal fragment
     * @param menuSelected
     */

    @Override
    public void menuPrincipalEventListener(String menuSelected) {
        switch (menuSelected) {
            case "gestclient":
                setFragment(ProspectMenuFragment.newInstance());
                mfragment = ProspectMenuFragment.newInstance();
                title.setText(getResources().getString(R.string.menuprospect));
                toolbar.setTitle(getResources().getString(R.string.gestclient));
                break;

            case "gestinstallation":
                setFragment(MenuInstallationFragment.newInstance());
                title.setText(getResources().getString(R.string.menuinstallation));
                toolbar.setTitle(getResources().getString(R.string.gestinstallation));
                mfragment = MenuInstallationFragment.newInstance();

                break;

            case "gestmaintenance":
                setFragment(MenuMaintenanceFragment.newInstance());
                title.setText(getResources().getString(R.string.menumaintenance));
                toolbar.setTitle(getResources().getString(R.string.gestmaintenance));
                mfragment = MenuMaintenanceFragment.newInstance();
                break;
        }
    }

    /**
     * methode interface menu prospect
     * @param itemselected
     */

    @Override
    public void actionmenuevent(String itemselected) {

        switch (itemselected) {

            case "suivi":
                setFragment(MenuSuiviProspectFragment.newInstance());
                title.setText(getResources().getString(R.string.suivi));
                mfragment = MenuSuiviProspectFragment.newInstance();

                break;
            case "contrat":

                break;
            case "listclient":

                break;
            default:
                setFragment(ProspectMenuFragment.newInstance());
                break;
        }

    }

    /**
     * methode interface menu suivi fragment
     * @param val
     */

    @Override
    public void MenuSuiviInteraction(String val) {
        switch (val) {
            case "modifier":
                setFragment(ListeProspectFragment.newInstance());
                title.setText(getResources().getString(R.string.listeprospect));
                mfragment= ListeProspectFragment.newInstance();
                parentfragment = MenuSuiviProspectFragment.newInstance();
                break;
        }
    }
    /**
     * methode interface menu installation fragment
     * @param itemSelected
     */
    @Override
    public void onGestionInstallationFragmentInteraction(String itemSelected) {
        switch (itemSelected) {
            case "gps":
                setFragment(GPSFragment.newInstance());
                mfragment = GPSFragment.newInstance();
                title.setText(getResources().getString(R.string.gps_title));
                toolbar.setTitle(getResources().getString(R.string.gestmaintenance));
        }

    }

    /**
     * methode interface details fragment
     * @param nom
     * @param prenom
     */

    @Override
    public void PropectToViewInteraction(String nom, String prenom) {

    }

    /**
     * methode interface Liste prospect fragment
     * @param uri
     */
    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * methode interface liste prospect fragment
     * permet de reccuperer le prospect à modifier
     * @param nom
     * @param prenom
     */

    @Override
    public void passProspectToView(String nom, String prenom) {
        if (nom != null && prenom != null) {
            setFragment(DetailsProspectFragment.newInstance(nom,prenom));
            title.setText(getResources().getString(R.string.detailprospect));
            mfragment = DetailsProspectFragment.newInstance(nom,prenom);


        }
    }

    /**
     * methode interface menu maintenance fragment
     * renvoi un string selon le buton choisi par l'utilisateur
     * appel le fragment ou l'activité concerner par la tache
     * @param itemSelected
     */

    @Override
    public void onMenuMaintenanceFragmentInteraction(String itemSelected) {

    }

    /**
     * methode de l'interface GPS fragment
     * @param itemSelected
     */
    @Override
    public void onGPSFragmentFragmentInteraction(String itemSelected) {

    }

    /**
     * methode de l'interface changepass fragment
     * @param nothing
     */
    @Override
    public void onChangePassFragmentCallInteraction(boolean nothing) {

    }
}
